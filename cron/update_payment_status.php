<?php

//require_once(__DIR__."/../lib/functions.php");

$model = new \Libraries\Model();
$date = new DateTime('now');
$date->modify('first day of this month');
$currentMonthStartDate = $date->format('yy-m-d');
$date = new DateTime('now');
$date->modify('last day of this month');
$currentMonthEndDate = $date->format('yy-m-d');
$payments = $model->getPendingPaymentTermsByExpiry($currentMonthStartDate, $currentMonthEndDate);
foreach($payments as $payment){
    $update = $model->updatePaymentTermsStatus([
        'id' => $payment->CPT_ID, 
        'payment_status' => 'Due',
    ]);
}


?>