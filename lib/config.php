<?php

$GLOBALS['configurations'] = [
    'dirpath' => 'http://localhost:8888/contractors',
    'enviroment' => 'development',
    'host' => 'localhost',
    'app' => [
        'name' => "Managment",
        'version' => '1.0',
        'beta' => 'TRUE',
        'description' => 'A Contractors Management System'
    ],
    'dev' => [
        'name' => 'Ali Hassan',
        'company' => 'Stack Technologies',
        'contact' => 'ali@thestacktech.com',
        'designation' => 'Web & Backend Developer'
    ],
    'git' => [
        'host' => 'BitBucket',
        'repository' => 'https://alihfaryad@bitbucket.org/stack-technologies/contractors-management-system.git'
    ]
];

if($_SERVER['SERVER_NAME'] == 'alidevs.com'){
    $GLOBALS['configurations']['enviroment'] = 'production';
    if($GLOBALS['configurations']['enviroment'] == 'production'){
        $GLOBALS['configurations']['dirpath'] = 'https://alidevs.com/contractors';
        $GLOBALS['configurations']['host'] = 'localhost';
    }
}

?>