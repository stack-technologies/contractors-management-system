<?php

require_once(__DIR__.'/config.php');
require_once(__DIR__.'/model.php');

function redirectTo(string $location){
    header("Location: ".$GLOBALS['configurations']['dirpath'].$location."");
    die();
}

function dirPath(){
    echo $GLOBALS['configurations']['dirpath'];
}

function killActiveSessions(){
    unset($_SESSION);
    session_destroy();
}

function auth(){
    $actual_link = explode('/', $_SERVER[REQUEST_URI]);
    if($GLOBALS['configurations']['enviroment'] == 'production'){
        $dashboard_link_index = 2;
        $login_link_index = 3;
    }
    else{
        $dashboard_link_index = 4;
        $login_link_index = 5;
    }
    if($actual_link[$dashboard_link_index] == "dashboard" && $actual_link[$login_link_index] != "login"){
        if(!isset($_SESSION['user']['auth']) || $_SESSION['user']['auth'] == FALSE){
            killActiveSessions();
            redirectTo('/dashboard/login');
        }
    }
}

function alert($type, $message){
    echo '
        <div class="alert alert-'.$type.'" role="alert" id="alert-'.$type.'">'.$message.'</div>
    ';
}

?>