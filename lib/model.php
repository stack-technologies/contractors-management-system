<?php

namespace Libraries;
use \PDO as PDO;
use \PDOExceptions as Exceptions;

class Model{

    private static $hostname = "localhost";
    private static $username;
    private static $password;
    private static $db_name;

    private function setHost(){
        if($GLOBALS['configurations']['enviroment'] == "production"){
            self::$username = "u406893430_alidevs";
            self::$password = "alidevsDB";
            self::$db_name = "u406893430_alidevs";
        }
        elseif ($GLOBALS['configurations']['enviroment'] == "development") {
            self::$username = "alihfaryad";
            self::$password = "1234qwer";
            self::$db_name = "contractorsDB";
        }
    }

    public function __construct(){
        self::setHost();
        $dns = "mysql:host=" . self::$hostname . ";dbname=" . self::$db_name;
        try{
            $this->conn = new PDO($dns, self::$username, self::$password);
            $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        }
        catch(Exceptions $e){
            die();
            return handleException($e);
        }
    }

    public function migrate(){
        try{
            $query = "CREATE TABLE IF NOT EXISTS users (
                id INT(11) AUTO_INCREMENT NOT NULL,
                name VARCHAR(100) NOT NULL,
                email VARCHAR(100) NOT NULL,
                password VARCHAR(1000) NOT NULL,
                timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY  (id)
            )";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $query = "CREATE TABLE IF NOT EXISTS Contracts (
                Contract_ID INT(11) AUTO_INCREMENT NOT NULL,
                Contract_Title VARCHAR(225) NOT NULL,
                Contract_Description TEXT NOT NULL,
                Contract_Total_Amount DOUBLE NOT NULL,
                Contractor_ID INT(11) NOT NULL,
                Beneficiary VARCHAR(255) NOT NULL,
                Assignee VARCHAR(255) NOT NULL,
                Budget_Group_ID INT(11) NOT NULL,
                Payment_Terms INT(11) DEFAULT NULL,
                Contract_Status_ID INT(11) DEFAULT NULL,
                Planned_Start_Date DATE NOT NULL,
                Expected_End_Date DATE NOT NULL,
                Actual_Start_Date DATE NOT NULL,
                Actual_End_Date DATE NOT NULL,
                Progress DECIMAL NOT NULL,
                Notes TEXT DEFAULT NULL,
                Files VARCHAR(255) DEFAULT NULL,
                CreatedBy_User_ID INT(11) NOT NULL,
                EditedBy_User_ID INT(11) DEFAULT NULL,
                Tags TEXT DEFAULT NULL,
                Timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY  (Contract_ID)
            )";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $query = "CREATE TABLE IF NOT EXISTS Contractors (
                Contractor_ID INT(11) AUTO_INCREMENT NOT NULL,
                Company_Name VARCHAR(225) NOT NULL,
                Notes TEXT NOT NULL,
                DateTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY  (Contractor_ID)
            )";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $query = "CREATE TABLE IF NOT EXISTS Contract_Payment_Terms (
                CPT_ID INT(11) AUTO_INCREMENT NOT NULL,
                Contract_ID INT(11) NOT NULL,
                Payment_Due_Date DATE NOT NULL,
                Payment_Amount DOUBLE NOT NULL,
                Payment_Status VARCHAR(255) NOT NULL,
                PRIMARY KEY  (CPT_ID)
            )";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $query = "CREATE TABLE IF NOT EXISTS Contract_Status (
                Contract_Status_ID INT(11) AUTO_INCREMENT NOT NULL,
                Contract_ID INT(11) NOT NULL,
                Status ENUM('Under Review', 'Approved', 'In-Progress', 'On Hold', 'Completed', 'Postponed', 'Cancelled'),
                Status_DateTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY  (Contract_Status_ID)
            )";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $query = "CREATE TABLE IF NOT EXISTS Contract_Files (
                File_ID INT(11) AUTO_INCREMENT NOT NULL,
                Contract_ID INT(11) NOT NULL,
                File_Name VARCHAR(225) NOT NULL,
                File_Description TEXT NOT NULL,
                File_Class VARCHAR(225) NOT NULL,
                File_Link VARCHAR(225) NOT NULL,
                Upload_DateTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                File_ViewCount INT(11) DEFAULT NULL,
                User_ID INT(11) NOT NULL,
                PRIMARY KEY  (File_ID)
            )";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $query = "CREATE TABLE IF NOT EXISTS Beneficiary (
                Beneficiary_ID INT(11) AUTO_INCREMENT NOT NULL,
                Beneficiary_Name VARCHAR(255) NOT NULL,
                Timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY  (Beneficiary_ID)
            )";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $query = "CREATE TABLE IF NOT EXISTS Assignee (
                Assignee_ID INT(11) AUTO_INCREMENT NOT NULL,
                Assignee_Name VARCHAR(255) NOT NULL,
                Beneficiary_ID INT(11) NOT NULL,
                Assignee_ACLs VARCHAR(255) DEFAULT NULL,
                Timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                User_ID INT(11) NOT NULL,
                PRIMARY KEY  (Assignee_ID)
            )";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $query = "CREATE TABLE IF NOT EXISTS Assignee_ACL ( 
                Assignee_ACL_ID INT(11) AUTO_INCREMENT NOT NULL,
                Assignee_ID INT(11) NOT NULL,
                Active TINYINT(1) NOT NULL,
                ACL_ID INT(11) NOT NULL, 
                Notes TEXT DEFAULT NULL,
                Timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                User_ID INT(11) NOT NULL,
                PRIMARY KEY  (Assignee_ACL_ID)
            )";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $query = "CREATE TABLE IF NOT EXISTS Permits (
                Permit_ID INT(11) AUTO_INCREMENT NOT NULL,
                Access_Name VARCHAR(225) NOT NULL,
                ID_Files VARCHAR(255) DEFAULT NULL,
                ID_Number VARCHAR(255) NOT NULL,
                Permit_Start_Date DATE NOT NULL,
                Permit_End_Date DATE NOT NULL,
                Permit_Location VARCHAR(255) NOT NULL,
                Request_Date DATE DEFAULT NULL,
                Notes TEXT DEFAULT NULL,
                Contractor_ID INT(11) DEFAULT NULL,
                Beneficiary VARCHAR(255) DEFAULT NULL,
                Assignee VARCHAR(255) DEFAULT NULL,
                Permit_Status_ID INT(11) DEFAULT NULL,
                Permit_Issue_Date DATE DEFAULT NULL,
                Permit_File INT(11) DEFAULT NULL, 
                CreatedBy_User_ID INT(11) NOT NULL,
                EditedBy_User_ID INT(11) DEFAULT NULL,
                Timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY  (Permit_ID)
            )";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $query = "CREATE TABLE IF NOT EXISTS Permit_Status (
                Permit_Status_ID INT(11) AUTO_INCREMENT NOT NULL,
                Permit_ID INT(11) NOT NULL,
                Status ENUM('Pending', 'In-Process', 'Granted', 'Rejected', 'Cancelled'),
                Status_DateTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                Status_Notes TEXT DEFAULT NULL,
                EditedBy_User_ID INT(11) DEFAULT NULL,
                CompletedBy_User_ID INT(11) DEFAULT NULL,
                PRIMARY KEY  (Permit_Status_ID)
            )";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $query = "CREATE TABLE IF NOT EXISTS Permit_Files (
                File_ID INT(11) AUTO_INCREMENT NOT NULL,
                Permit_ID INT(11) NOT NULL,
                File_Class VARCHAR(225) NOT NULL,
                File_Link VARCHAR(225) NOT NULL,
                Upload_DateTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                User_ID INT(11) NOT NULL,
                PRIMARY KEY  (File_ID)
            )";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $query = "CREATE TABLE IF NOT EXISTS ACL (
                ACL_ID INT(11) AUTO_INCREMENT NOT NULL,
                ACL_Parameter VARCHAR(225) NOT NULL,
                ACL_Category VARCHAR(225) NOT NULL,
                DateTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                User_ID INT(11) NOT NULL,
                PRIMARY KEY  (ACL_ID)
            )";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $query = "CREATE TABLE IF NOT EXISTS BudgetGroup (
                Budget_Group_ID INT(11) AUTO_INCREMENT NOT NULL,
                Budget_Group_Title VARCHAR(255) NOT NULL,
                PRIMARY KEY  (Budget_Group_ID)
            )";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $this->createUser([
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => password_hash('admin', PASSWORD_BCRYPT),
            ]);

            return TRUE;
        }
        catch(Exceptions $e){
            return handleExceptions($e);
        }
    }

    public function numOfEntries(string $database){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM ".$database."");
            $stmt->execute();
            $result = $stmt->fetchAll();
            return count($result);
        }
        catch(Exceptions $e){
            return handleExceptions($e);
        }
    }

    public function createUser(array $data){
        try{
            $stmt = $this->conn->prepare("INSERT INTO users (name, email, password) VALUES (:name, :email, :password)");
            $stmt->bindParam(':name', $data['name']);
            $stmt->bindParam(':email', $data['email']);
            $stmt->bindParam(':password', $data['password']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            return handleException($e);
        }
    }

    public function updateUser(array $data){
        try{
            $password = $data['password'];
            $query_alpha = "UPDATE users SET name = :name, email = :email";
            $query_beta = $password == "" ? " WHERE id = :id" : ", password = :password WHERE id = :id";
            $query = $query_alpha . $query_beta;
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':name', $data['name']);
            $stmt->bindParam(':email', $data['email']);
            if($password != "")
                $stmt->bindParam(':password', password_hash($data['password'], PASSWORD_BCRYPT));
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            return handleException($e);
        }
    }

    public function getUsers(){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM users ORDER BY id DESC");
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }
        catch(Exceptions $e){
            return handleException($e);
        }
    }

    public function checkEmailExists(string $email){
        try{
            $id = $this->getUserByEmail($email)->id;
            if($id != NULL){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getUserByEmail(string $email){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM users WHERE email = :email");
            $stmt->bindParam(':email', $email);
            $stmt->execute();
            return $stmt->fetchAll()[0];
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getUserById(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT id, name, email, timestamp FROM users WHERE id = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return $stmt->fetchAll()[0];
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function createContract(array $data){ 
        try{
            $stmt = $this->conn->prepare("INSERT INTO Contracts (Contract_Title, Contract_Description, Contract_Total_Amount, Contractor_ID, Beneficiary, Assignee, Budget_Group_ID, Planned_Start_Date, Expected_End_Date, Actual_Start_Date, Actual_End_Date, Progress, Notes, CreatedBy_User_ID, Tags) VALUES (:contract_title, :contract_description, :contract_total_amount, :contractor_id, :beneficiary, :assignee, :budget_group_id, :planned_start_date, :expected_end_date, :actual_start_date, :actual_end_date, :progress, :notes, :createdby_user_id, :tags)");
            $stmt->bindParam(':contract_title', $data['contract_title']);
            $stmt->bindParam(':contract_description', $data['contract_description']);
            $stmt->bindParam(':contract_total_amount', $data['contract_total_amount']);
            $stmt->bindParam(':contractor_id', $data['contractor_id']);
            $stmt->bindParam(':beneficiary', $data['beneficiary']);
            $stmt->bindParam(':assignee', $data['assignee']);
            $stmt->bindParam(':budget_group_id', $data['budget_group_id']);
            $stmt->bindParam(':planned_start_date', $data['planned_start_date']);
            $stmt->bindParam(':expected_end_date', $data['expected_end_date']);
            $stmt->bindParam(':actual_start_date', $data['actual_start_date']);
            $stmt->bindParam(':actual_end_date', $data['actual_end_date']);
            $stmt->bindParam(':progress', $data['progress']);
            $stmt->bindParam(':notes', $data['notes']);
            $stmt->bindParam(':createdby_user_id', $data['createdby_user_id']);
            $stmt->bindParam(':tags', $data['tags']);
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getContracts(){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contracts ORDER BY Contract_ID DESC");
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getContractById(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contracts WHERE Contract_ID = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return $stmt->fetchAll()[0];
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getContractsByContractorAndBudget(int $contractor_id, int $budget_group_id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contracts WHERE Contractor_ID = :contractor_id AND Budget_Group_ID = :budget_group_id");
            $stmt->bindParam(':contractor_id', $contractor_id);
            $stmt->bindParam(':budget_group_id', $budget_group_id);
            $stmt->execute();
            return $stmt->fetchAll();
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getContractsByContractor(int $contractor_id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contracts WHERE Contractor_ID = :contractor_id");
            $stmt->bindParam(':contractor_id', $contractor_id);
            $stmt->execute();
            return $stmt->fetchAll();
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getContractsByBudgetGroup(int $budget_group_id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contracts WHERE Budget_Group_ID = :budget_group_id");
            $stmt->bindParam(':budget_group_id', $budget_group_id);
            $stmt->execute();
            return $stmt->fetchAll();
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }


    public function getContractsByExpiry(string $startdate, string $enddate){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contracts WHERE Expected_End_Date >= :start_date AND Expected_End_Date <= :end_date");
            $stmt->bindParam(':start_date', $startdate);
            $stmt->bindParam(':end_date', $enddate);
            $stmt->execute();
            return $stmt->fetchAll();
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getNumOfContractsByStatus(string $status){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contract_Status WHERE Status = :status");
            $stmt->bindParam(':status', $status);
            $stmt->execute();
            return count($stmt->fetchAll());
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updateContractAfterCreation(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Contracts SET Contract_Status_ID = :contract_status_id, Payment_Terms = :payment_terms, Files = :files WHERE Contract_ID = :contract_id");
            $stmt->bindParam(':contract_id', $data['contract_id']);
            $stmt->bindParam(':contract_status_id', $data['contract_status_id']);
            $stmt->bindParam(':payment_terms', $data['payment_terms']);
            $stmt->bindParam(':files', $data['files']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updateContractPayments(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Contracts SET Payment_Terms = :payment_terms WHERE Contract_ID = :contract_id");
            $stmt->bindParam(':contract_id', $data['id']);
            $stmt->bindParam(':payment_terms', $data['payments']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updateContract(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Contracts SET 
                Contract_Title = :title, 
                Contract_Description = :desc, 
                Contract_Total_Amount = :amount, 
                Contractor_ID = :contractor, 
                Beneficiary = :beneficiary, 
                Assignee = :assignee, 
                Budget_Group_ID = :budget, 
                Progress = :progress, 
                Notes = :notes, 
                Planned_Start_Date = :p_start_date, 
                Expected_End_Date = :p_end_date, 
                Actual_Start_Date = :a_start_date, 
                Actual_End_Date = :a_end_date, 
                EditedBy_User_ID = :editby_user_id, 
                Tags = :tags 
                WHERE Contract_ID = :id");
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':title', $data['contractTitle']);
            $stmt->bindParam(':desc', $data['contractDesc']);
            $stmt->bindParam(':amount', $data['contractAmount']);
            $stmt->bindParam(':contractor', $data['contractContractor']);
            $stmt->bindParam(':beneficiary', $data['contractBeneficiary']);
            $stmt->bindParam(':assignee', $data['contractAssignee']);
            $stmt->bindParam(':budget', $data['contractBudget']);
            $stmt->bindParam(':progress', $data['contractProgress']);
            $stmt->bindParam(':notes', $data['contractNotes']);
            $stmt->bindParam(':p_start_date', $data['contractPlannedStartDate']);
            $stmt->bindParam(':p_end_date', $data['contractPlannedEndDate']);
            $stmt->bindParam(':a_start_date', $data['contractActualStartDate']);
            $stmt->bindParam(':a_end_date', $data['contractActualEndDate']);
            $stmt->bindParam(':editby_user_id', $data['contractEditByUserID']);
            $stmt->bindParam(':tags', $data['contractTags']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function createFile(array $data){
        try{
            $stmt = $this->conn->prepare("INSERT INTO Contract_Files (Contract_ID, File_Name, File_Description, File_Class, File_Link, User_ID) VALUES (:contract_id, :file_name, :file_description, :file_class, :file_link, :user_id)");
            $stmt->bindParam(':contract_id', $data['contract_id']);
            $stmt->bindParam(':file_name', $data['file_name']);
            $stmt->bindParam(':file_description', $data['file_description']);
            $stmt->bindParam(':file_class', $data['file_class']);
            $stmt->bindParam(':file_link', $data['file_link']);
            $stmt->bindParam(':user_id', $data['user_id']);
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getFileById(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contract_Files WHERE File_ID = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return $stmt->fetchAll()[0];
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updateFile(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Contract_Files SET File_Name = :filename, File_Description = :filedesc, File_Class = :fileclass WHERE File_ID = :id");
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':filename', $data['fileName']);
            $stmt->bindParam(':filedesc', $data['fileDesc']);
            $stmt->bindParam(':fileclass', $data['fileClass']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updateContractFiles(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Contracts SET Files = :files WHERE Contract_ID = :contract_id");
            $stmt->bindParam(':contract_id', $data['id']);
            $stmt->bindParam(':files', $data['files']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function createStatus(array $data){
        try{
            $stmt = $this->conn->prepare("INSERT INTO Contract_Status (Contract_ID, Status) VALUES (:contract_id, :status)");
            $stmt->bindParam(':contract_id', $data['contract_id']);
            $stmt->bindParam(':status', $data['status']);
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getStatusById(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contract_Status WHERE Contract_Status_ID = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll()[0];
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updateContractStatus(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Contract_Status SET Status = :status WHERE Contract_Status_ID = :id");
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':status', $data['status']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function createPaymentTerms(array $data){
        try{
            $stmt = $this->conn->prepare("INSERT INTO Contract_Payment_Terms (Contract_ID, Payment_Due_Date, Payment_Amount, Payment_Status) VALUES (:contract_id, :payment_due_date, :payment_amount, :payment_status)");
            $stmt->bindParam(':contract_id', $data['contract_id']);
            $stmt->bindParam(':payment_due_date', $data['payment_due_date']);
            $stmt->bindParam(':payment_amount', $data['payment_amount']);
            $stmt->bindParam(':payment_status', $data['payment_status']);
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getPaymentTerms(){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contract_Payment_Terms ORDER BY CPT_ID DESC");
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getPaymentTermsById(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contract_Payment_Terms WHERE CPT_ID = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll()[0];
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getPaymentTermsByDates(string  $startdate, string $enddate, string $status){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contract_Payment_Terms WHERE Payment_Due_Date >= :start_date AND Payment_Due_Date <= :end_date AND Payment_Status = :status");
            $stmt->bindParam(':start_date', $startdate);
            $stmt->bindParam(':end_date', $enddate);
            $stmt->bindParam(':status', $status);
            $stmt->execute();
            return $stmt->fetchAll();
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getPendingPaymentTermsByExpiry(string $startdate, string $enddate){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contract_Payment_Terms WHERE Payment_Due_Date >= :start_date AND Payment_Due_Date <= :end_date AND Payment_Status = 'Pending'");
            $stmt->bindParam(':start_date', $startdate);
            $stmt->bindParam(':end_date', $enddate);
            $stmt->execute();
            return $stmt->fetchAll();
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getDuePaymentTermsByExpiry(string $startdate, string $enddate){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contract_Payment_Terms WHERE Payment_Due_Date >= :start_date AND Payment_Due_Date <= :end_date AND Payment_Status = 'Due'");
            $stmt->bindParam(':start_date', $startdate);
            $stmt->bindParam(':end_date', $enddate);
            $stmt->execute();
            return $stmt->fetchAll();
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getCompletedPaymentTermsByExpiry(string $startdate, string $enddate){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contract_Payment_Terms WHERE Payment_Due_Date >= :start_date AND Payment_Due_Date <= :end_date AND Payment_Status = 'Paid'");
            $stmt->bindParam(':start_date', $startdate);
            $stmt->bindParam(':end_date', $enddate);
            $stmt->execute();
            return $stmt->fetchAll();
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getCancelledPaymentTermsByExpiry(string $startdate, string $enddate){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contract_Payment_Terms WHERE Payment_Due_Date >= :start_date AND Payment_Due_Date <= :end_date AND Payment_Status = 'Cancelled'");
            $stmt->bindParam(':start_date', $startdate);
            $stmt->bindParam(':end_date', $enddate);
            $stmt->execute();
            return $stmt->fetchAll();
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updatePaymentTerms(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Contract_Payment_Terms SET Payment_Due_Date = :payment_due_date, Payment_Amount = :payment_amount, Payment_Status = :payment_status WHERE CPT_ID = :id");
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':payment_due_date', $data['payment_due_date']);
            $stmt->bindParam(':payment_amount', $data['payment_amount']);
            $stmt->bindParam(':payment_status', $data['payment_status']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updatePaymentTermsStatus(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Contract_Payment_Terms SET Payment_Status = :payment_status WHERE CPT_ID = :id");
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':payment_status', $data['payment_status']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function createContractor(array $data){
        try{
            $stmt = $this->conn->prepare("INSERT INTO Contractors (Company_Name, Notes) VALUES (:company_name, :notes)");
            $stmt->bindParam(':company_name', $data['companyName']);
            $stmt->bindParam(':notes', $data['notes']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getContractors(){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contractors ORDER BY Contractor_ID DESC");
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getContractorById(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contractors WHERE Contractor_ID = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll()[0];
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getNumberOfContractsPerContractor(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Contracts WHERE Contractor_ID = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll();
            return count($result);
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updateContractor(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Contractors SET Company_Name = :cName, Notes = :notes WHERE Contractor_ID = :id");
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':cName', $data['companyName']);
            $stmt->bindParam(':notes', $data['notes']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function createAssignee(array $data){
        try{
            $stmt = $this->conn->prepare("INSERT INTO Assignee (Assignee_Name, Beneficiary_ID, User_ID) VALUES (:assignee_name, :beneficiary_id, :user_id)");
            $stmt->bindParam(':assignee_name', $data['assigneeName']);
            $stmt->bindParam(':beneficiary_id', $data['beneficiaryID']);
            $stmt->bindParam(':user_id', $data['userID']);
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getAssignees(){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Assignee ORDER BY Assignee_ID DESC");
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getAssigneeById(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Assignee WHERE Assignee_ID = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll()[0];
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getAssigneesByACL(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Assignee_ACL WHERE ACL_ID = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updateAssigneeACLs(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Assignee SET Assignee_ACLs = :acl WHERE Assignee_ID = :id");
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':acl', $data['acl']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updateAssignee(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Assignee SET Assignee_Name = :aName, Beneficiary_ID = :bID WHERE Assignee_ID = :id");
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':aName', $data['assigneeName']);
            $stmt->bindParam(':bID', $data['beneficiaryID']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function createBeneficiary(array $data){
        try{
            $stmt = $this->conn->prepare("INSERT INTO Beneficiary (Beneficiary_Name) VALUES (:beneficiary_name)");
            $stmt->bindParam(':beneficiary_name', $data['beneficiaryName']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getBeneficiary(){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Beneficiary ORDER BY Beneficiary_ID DESC");
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getBeneficiaryById(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Beneficiary WHERE Beneficiary_ID = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll()[0];
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updateBeneficiary(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Beneficiary SET Beneficiary_Name = :bName WHERE Beneficiary_ID = :id");
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':bName', $data['beneficiaryName']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function createPermit(array $data){ 
        try{
            $stmt = $this->conn->prepare("INSERT INTO Permits (Access_Name, ID_Number, Permit_Start_Date, Permit_End_Date, Permit_Location, Request_Date, Notes, Contractor_ID, Beneficiary, Assignee, CreatedBy_User_ID) VALUES (:access_name, :id_number, :permit_start_date, :permit_end_date, :permit_location, :request_date, :notes, :contractor_id, :beneficiary, :assignee, :createdby_user_id)");
            $stmt->bindParam(':access_name', $data['access_name']);
            $stmt->bindParam(':id_number', $data['id_number']);
            $stmt->bindParam(':permit_start_date', $data['permit_start_date']);
            $stmt->bindParam(':permit_end_date', $data['permit_end_date']);
            $stmt->bindParam(':permit_location', $data['permit_location']);
            $stmt->bindParam(':request_date', $data['request_date']);
            $stmt->bindParam(':notes', $data['notes']);
            $stmt->bindParam(':contractor_id', $data['contractor_id']);
            $stmt->bindParam(':beneficiary', $data['beneficiary']);
            $stmt->bindParam(':assignee', $data['assignee']);
            $stmt->bindParam(':createdby_user_id', $data['createdby_user_id']);
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function createPermitStatus(array $data){
        try{
            $stmt = $this->conn->prepare("INSERT INTO Permit_Status (Permit_ID, Status, Status_Notes, CompletedBy_User_ID) VALUES (:permit_id, :status, :status_notes, :user)");
            $stmt->bindParam(':permit_id', $data['permit_id']);
            $stmt->bindParam(':status', $data['status']);
            $stmt->bindParam(':status_notes', $data['status_notes']);
            $stmt->bindParam(':user', $data['user']);
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function createPermitFile(array $data){
        try{
            $stmt = $this->conn->prepare("INSERT INTO Permit_Files (Permit_ID, File_Class, File_Link, User_ID) VALUES (:permit_id, :file_class, :file_link, :user_id)");
            $stmt->bindParam(':permit_id', $data['permit_id']);
            $stmt->bindParam(':file_class', $data['file_class']);
            $stmt->bindParam(':file_link', $data['file_link']);
            $stmt->bindParam(':user_id', $data['user_id']);
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getPermitFileById(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Permit_Files WHERE File_ID = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return $stmt->fetchAll()[0];
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updatePermitFile(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Permit_Files SET File_Link = :file_link, User_ID = :user_id WHERE File_ID = :id");
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':file_link', $data['file_link']);
            $stmt->bindParam(':user_id', $data['user_id']);
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updatePermitAfterCreation(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Permits SET Permit_Status_ID = :permit_status_id, ID_Files = :files WHERE Permit_ID = :permit_id");
            $stmt->bindParam(':permit_id', $data['permit_id']);
            $stmt->bindParam(':permit_status_id', $data['permit_status_id']);
            $stmt->bindParam(':files', $data['files']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updatePermitStatusID(int $status_ID, int $permit_ID){
        try{
            $stmt = $this->conn->prepare("UPDATE Permits SET Permit_Status_ID = :permit_status_id WHERE Permit_ID = :permit_id");
            $stmt->bindParam(':permit_id', $permit_ID);
            $stmt->bindParam(':permit_status_id', $status_ID);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getPermits(){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Permits ORDER BY Permit_ID DESC");
            $stmt->execute();
            return $stmt->fetchAll();
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getPermitByID(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Permits WHERE Permit_ID = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return $stmt->fetchAll()[0];
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getNumberOfPermitsByStatus(string $status){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Permit_Status WHERE Status = :status");
            $stmt->bindParam(':status', $status);
            $stmt->execute();
            $result = $stmt->fetchAll();
            return count($result);
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getPermitStatusByID(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Permit_Status WHERE Permit_Status_ID = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return $stmt->fetchAll()[0];
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getPermitStatusByPermitID(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Permit_Status WHERE Permit_ID = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return $stmt->fetchAll();
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function completePermit(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Permits SET Permit_Issue_Date = :issue_date, Permit_File = :file WHERE Permit_ID = :id");
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':issue_date', $data['issue_date']);
            $stmt->bindParam(':file', $data['permit_file_id']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function completePermitStatus(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Permit_Status SET Status = :status, Status_Notes = :notes, CompletedBy_User_ID = :user WHERE Permit_Status_ID = :id");
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':status', $data['status']);
            $stmt->bindParam(':notes', $data['notes']);
            $stmt->bindParam(':user', $data['user']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updatePermitStatus(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Permit_Status SET Status = :status, Status_Notes = :notes, EditedBy_User_ID = :user WHERE Permit_Status_ID = :id");
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':status', $data['status']);
            $stmt->bindParam(':notes', $data['notes']);
            $stmt->bindParam(':user', $data['user']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function deleteEntry($table, $key, $value){
        try{
            $stmt = $this->conn->prepare("DELETE FROM ".$table." WHERE ".$key." = :value");
            $stmt->bindParam(':value', $value);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function createACL(array $data){
        try{
            $stmt = $this->conn->prepare("INSERT INTO ACL (ACL_Parameter, ACL_Category, User_ID) VALUES (:acl_parameter, :acl_category, :user_id)");
            $stmt->bindParam(':acl_parameter', $data['acl_parameter']);
            $stmt->bindParam(':acl_category', $data['acl_category']);
            $stmt->bindParam(':user_id', $data['user_id']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getACL(){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM ACL ORDER BY ACL_ID DESC");
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getACLById(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM ACL WHERE ACL_ID = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll()[0];
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getACLByCategoryAndParameter(string $category, string $parameter){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM ACL WHERE ACL_Category = :category AND ACL_Parameter = :parameter");
            $stmt->bindParam(':category', $category);
            $stmt->bindParam(':parameter', $parameter);
            $stmt->execute();
            $result = $stmt->fetchAll()[0];
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getACLCategories(){
        try{
            $stmt = $this->conn->prepare("SELECT DISTINCT ACL_Category FROM ACL");
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getACLParametersByCategory(string $category){
        try{
            $stmt = $this->conn->prepare("SELECT ACL_Parameter FROM ACL WHERE ACL_Category = :category");
            $stmt->bindParam(':category', $category);
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updateACL(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE ACL SET ACL_Parameter = :acl_parameter, ACL_Category = :acl_category WHERE ACL_ID = :id");
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':acl_parameter', $data['acl_parameter']);
            $stmt->bindParam(':acl_category', $data['acl_category']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function createAssigneeACL(array $data){
        try{
            $stmt = $this->conn->prepare("INSERT INTO Assignee_ACL (Assignee_ID, Active, ACL_ID, Notes, User_ID) VALUES (:assignee_id, :active, :acl, :acl_notes, :user_id)");
            $stmt->bindParam(':assignee_id', $data['assignee_id']);
            $stmt->bindParam(':active', $data['active']);
            $stmt->bindParam(':acl', $data['acl']);
            $stmt->bindParam(':acl_notes', $data['acl_notes']);
            $stmt->bindParam(':user_id', $data['user_id']);
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getAssigneeACL(){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Assignee_ACL ORDER BY ACL_ID DESC");
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getAssigneeACLById(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM Assignee_ACL WHERE Assignee_ACL_ID = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll()[0];
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updateAssigneeACL(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE Assignee_ACL SET Active = :active, ACL_ID = :acl, Notes = :notes WHERE Assignee_ACL_ID = :id");
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':active', $data['acl_status']);
            $stmt->bindParam(':acl', $data['acl']);
            $stmt->bindParam(':notes', $data['acl_notes']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function createBudgetGroup(array $data){
        try{
            $stmt = $this->conn->prepare("INSERT INTO BudgetGroup (Budget_Group_Title) VALUES (:budgetgroup_title)");
            $stmt->bindParam(':budgetgroup_title', $data['budgetgroup_title']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getBudgetGroup(){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM BudgetGroup ORDER BY Budget_Group_ID DESC");
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function getBudgetGroupById(int $id){
        try{
            $stmt = $this->conn->prepare("SELECT * FROM BudgetGroup WHERE Budget_Group_ID = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll()[0];
            return $result;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

    public function updateBudgetGroup(array $data){
        try{
            $stmt = $this->conn->prepare("UPDATE BudgetGroup SET Budget_Group_Title = :budgetgroup_title WHERE Budget_Group_ID = :id");
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':budgetgroup_title', $data['budgetgroup_title']);
            if($stmt->execute()){
                return TRUE;
            }
            return FALSE;
        }
        catch(Exceptions $e){
            handleException($e);
        }
    }

}

?>