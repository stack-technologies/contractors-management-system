    <div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show  bg-gradient-dark" id="sidebar">
        <div class="c-sidebar-brand d-lg-down-none">
            <div class="c-sidebar-brand-full" width="18" height="46" alt="CoreUI Logo">
                <i class="cib-coreui-c h1"></i>
            </div>
            <div class="c-sidebar-brand-minimized" width="46" height="46" alt="CoreUI Logo">
                <i class="cib-coreui-c"></i>
            </div>
        </div>
        <ul class="c-sidebar-nav">
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="<?php dirPath() ?>/dashboard">
                    <div class="c-sidebar-nav-icon">
                        <i class="cil-speedometer"></i>
                    </div>
                    Dashboard
                </a>
            </li> 
            <?php if($_SESSION['user']['auth'] == TRUE){ ?>    
            <li class="c-sidebar-nav-title">CMS</li>
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="<?php dirPath() ?>/dashboard/contracts/list">
                    <div class="c-sidebar-nav-icon">
                        <i class="cil-book"></i>
                    </div> Contracts
                </a>
            </li>
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="<?php dirPath() ?>/dashboard/permits/list">
                    <div class="c-sidebar-nav-icon">
                        <i class="cib-pagekit"></i>
                    </div> Permits
                </a>
            </li>
            <li class="c-sidebar-nav-title">Admin</li>
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="<?php dirPath() ?>/dashboard/admin">
                    <div class="c-sidebar-nav-icon">
                        <i class="cil-gem"></i>
                    </div> Administrator
                </a>
            </li>
            <li class="c-sidebar-nav-title">App</li>
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="<?php dirPath() ?>/dashboard/user/list">
                    <div class="c-sidebar-nav-icon">
                        <i class="cil-wc"></i>
                    </div> Users
                </a>
            </li>
            <?php } ?>
        </ul>
        <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
    </div>
