<style>
    #alert-<?php echo $alert['type'] ?>{
        display: block;
    }
</style>
<?php
if($alert['type'] != ""){
?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8 col-sm-10 col-xs-12">
            <div class="alert alert-<?php echo $alert['type'] ?> text-center" role="alert" id="alert-<?php echo $alert['type'] ?>"><?php echo $alert['message'] ?></div>
        </div>
    </div>
</div>
<?php
}
?>