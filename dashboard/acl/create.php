<?php 
    require_once(__DIR__."/../inc/header.php"); 
    $model = new \Libraries\Model();
    $acl_categories = $model->getACLCategories();
    foreach($acl_categories as $acl_category){
        $list_of_categories[] = $acl_category->ACL_Category;
    }

    if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['create'])){
        for($i = 1; $i <= $_POST['num_of_acls']; $i++){
            $create = $model->createACL([
                'acl_parameter' => $_POST['acl_parameter_'.$i],
                'acl_category' => $_POST['acl_category_'.$i],
                'user_id' => $_SESSION['user']['id']
            ]);
        }
        if($create){
            $alert = [
                'type' => 'success',
                'message' => 'ACL(s) Created.'
            ];
        }
        else{
            $alert = [
                'type' => 'danger',
                'message' => 'Error Creating ACL(s). Server Error, Contact Adminstrator/Developer.'
            ];
        }
    }
?>
    <title>Create a New ACL - <?php echo $title ?></title>
    <style>
        .twitter-typeahead{
            width: 100%;
        }
        .tt-menu{
            background-color: white;
            border-bottom: 1px solid grey;
            border-left: 1px solid grey;
            border-right: 1px solid grey;
            border-bottom-right-radius: 5px;
            border-bottom-left-radius: 5px;
            width: 100%;
            cursor: pointer;
        }
        .tt-selectable{
            padding: 5px 20px;
        }
        .tt-selectable:hover{
            background-color: lightgrey;
        }
    </style>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/../inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/../inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                    <?php require(__DIR__.'/../inc/alert.php'); ?>
                        <div class="row justify-content-center">
                            <div class="col-sm-6 col-md-8 col-lg-10">
                                <form action="<?php $_PHP_SELF ?>" method="POST">
                                    <div class="card">
                                        <div class="card-header bg-light text-center"><strong>Add New ACL</strong></div>
                                        <div class="card-body">
                                            <input type="text" name="num_of_acls" id="num_of_acls" value="1" hidden>
                                            <table class="table table-responsive-sm">
                                                <thead class="table-borderless text-center">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Parameter</th>
                                                        <th>Category</th>
                                                        <th>
                                                            <button type="button" id="add_acl_btn" class="btn btn-sm btn-secondary bg-gradient-light mt-1">
                                                                <div class="c-icon">
                                                                    <i class="cil-library-add"></i>
                                                                </div>
                                                            </button>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody id="acl_rows">
                                                    <!--row-->
                                                    <tr>
                                                        <td class="text-center pt-3">1</td>
                                                        <td><input class="form-control" id="acl_parameter_1" type="text" name="acl_parameter_1" data-sanitize="escape" required></td>
                                                        <td><input class="form-control" id="acl_category_1" type="text" name="acl_category_1" data-sanitize="escape" required></td>
                                                        <td class="text-center">
                                                            <button type="button" data-id="1" name="acl_remove_1" id="acl_remove_1" class="acl-remove btn btn-sm">
                                                                <div class="c-icon">
                                                                    <i class="cil-trash text-danger"></i>
                                                                </div>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="card-footer">
                                            <button class="btn btn-sm btn-dark bg-gradient-dark w-100" name="create" type="submit">Add</button>
                                        </div>
                                    </div>
                                </form>
                                <!-- /.col-->
                            </div>
                            <!-- /.row-->
                        </div>
                    </div>
                </div>    
            </main>
        </div>
        
        <?php require_once(__DIR__."/../inc/footer.php"); ?>
        <script>
            $.validate({
                modules : 'sanitize'
            });
            $(document).on('click', '.acl-remove', function(){
                var dirPath = "<?php dirPath() ?>";
                var acl_row_num = $(this).data('id');
                var current_acls = $("#acl_rows").html().split('<!--row-->');
                var num_of_acls = current_acls.length - 1;
                var acl_parameter = [""];
                var acl_category = [""];

                for(var i = 1; i <= num_of_acls; i++){
                    acl_parameter.push($('#acl_parameter_'+i).val());
                    acl_category.push($('#acl_category_'+i).val());
                }

                current_acls.splice(acl_row_num, 1);
                acl_parameter.splice(acl_row_num, 1);
                acl_category.splice(acl_row_num, 1);
                var final_acls = '';
                for(var i = 1; i < current_acls.length; i++){
                    final_acls += '<!--row-->'
                        +'<tr>'
                            +'<td class="text-center pt-3">'+i+'</td>'
                            +'<td><input class="form-control" id="acl_parameter_'+i+'" type="text" name="acl_parameter_'+i+'" value="'+acl_parameter[i]+'" required></td>'
                            +'<td><input class="form-control" id="acl_category_'+i+'" type="text" name="acl_category_'+i+'" value="'+acl_category[i]+'" required></td>'
                            +'<td class="text-center">'
                                +'<button type="button" data-id="'+i+'" name="acl_remove_'+i+'" id="acl_remove_'+i+'" class="acl-remove btn btn-sm">'
                                    +'<div class="c-icon">'
                                        +'<i class="cil-trash text-danger"></i>'
                                    +'</div>'
                                +'</button>'
                            +'</td>'
                        +'</tr>';
                }
                $('#acl_rows').html(final_acls);
            });
            $(document).ready(function(){
                $('#add_acl_btn').click(function(){
                    var current_acls = $("#acl_rows").html();
                    var num_of_acls = current_acls.split('<!--row-->').length;
                    var acl_rows = '<!--row-->'
                        +'<tr>'
                            +'<td class="text-center pt-3">'+num_of_acls+'</td>'
                            +'<td><input class="form-control" id="acl_parameter_'+num_of_acls+'" type="text" name="acl_parameter_'+num_of_acls+'" required></td>'
                            +'<td><input class="form-control" id="acl_category_'+num_of_acls+'" type="text" name="acl_category_'+num_of_acls+'" required></td>'
                            +'<td class="text-center">'
                                +'<button type="button" data-id="'+num_of_acls+'" name="acl_remove_'+num_of_acls+'" id="acl_remove_'+num_of_acls+'" class="acl-remove btn btn-sm">'
                                    +'<div class="c-icon">'
                                        +'<i class="cil-trash text-danger"></i>'
                                    +'</div>'
                                +'</button>'
                            +'</td>'
                        +'</tr>';
                    $('#acl_rows').append(acl_rows);
                    $('#num_of_acls').val(num_of_acls);
                });

                // Defining the local dataset for Typeahead
                <?php
                function js_str($s){
                    return '"' . addcslashes($s, "\0..\37\"\\") . '"';
                }
                
                function js_array($array){
                    $temp = array_map('js_str', $array);
                    return '[' . implode(',', $temp) . ']';
                }
                
                echo 'var categories = ', js_array($list_of_categories), ';';
                ?>

                //Categories for TypeAhead
                var categories = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.whitespace,
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    local: categories
                });
                $('#acl_category_1').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                },
                {
                    name: 'Category',
                    source: categories
                });
            });
        </script>
    </div>
</body>
</html>