<?php 
    require_once(__DIR__."/../inc/header.php"); 
    $model = new \Libraries\Model();
    $acls = $model->getACL();
?>
    <title>List of ACL - <?php echo $title ?></title>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/../inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/../inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                    <?php require(__DIR__.'/../inc/alert.php'); ?>
                        <div class="row justify-content-center">
                            <div class="col-12 content-header pb-3">
                                <div class="row px-3">
                                    <div class="col-6">
                                        <h2>List of ACL</h2>
                                    </div>
                                    <div class="col-6 text-right">
                                        <a href="<?php dirpath() ?>/dashboard/acl/create" class="btn btn-primary bg-gradient-primary text-right">Add New Record</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <table class="table table-hover table-responsive-sm table-striped content-table text-center">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Parameter</th>
                                            <th>Category</th>
                                            <th># of Users</th>
                                            <th>Timestamp</th>
                                            <th>User</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach($acls as $acl){
                                            $numberOfAssignees = count($model->getAssigneesByACL($acl->ACL_ID));
                                            echo '
                                            <tr>
                                                <td>'.$acl->ACL_ID.'</td>
                                                <td>'.$acl->ACL_Parameter.'</td>
                                                <td>'.$acl->ACL_Category.'</td>
                                                <td><a href="'.$GLOBALS['configurations']['dirpath'].'/dashboard/assignee/list?acl='.$acl->ACL_ID.'">'.$numberOfAssignees.'</a></td>
                                                <td>'.$acl->DateTime.'</td>
                                                <td>'.$model->getUserById($acl->User_ID)->name.'</td>
                                                <td>
                                                    <a class="btn btn-sm btn-link edit" data-id="'.$acl->ACL_ID.'" data-toggle="modal" data-target="#editPopUp">
                                                        <div class="c-icon">
                                                            <i class="cil-pencil"></i>
                                                        </div>
                                                    </a>
                                                    <a class="btn btn-sm btn-link delete" data-id="'.$acl->ACL_ID.'" data-toggle="modal" data-target="#deletePopUp">
                                                        <div class="c-icon">
                                                            <i class="cil-trash"></i>
                                                        </div>
                                                    </a>
                                                </td>
                                            </tr>
                                            ';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.row-->
                        </div>
                    </div>
                </div>    
            </main>
        </div>
        
        <?php require_once(__DIR__."/../inc/footer.php"); ?>

    </div>

    <div id="editPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10" id="editInfo">
                                <h5>Edit Information</h5>
                                <p>Please Enter the new Information Below.</p>
                            </div>
                            <div class="col-12">
                                <div class="content-form mx-auto">
                                    <input type="number" name="editId" id="editId" required hidden>
                                    <div class="form-group">
                                        <label for="editParameter">Parameter</label>
                                        <input type="text" name="editParameter" id="editParameter" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="editCategory">Category</label>
                                        <input type="text" name="editCategory" id="editCategory" class="form-control" required>
                                    </div>
                                    <button name="editACL" id="editACL" class="btn btn-primary">EDIT ACL</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--editPopUp modal CLOSE-->

    <div id="deletePopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10">
                                <h5>Are You Sure You Want To Delete?</h5>
                                <br>
                                <button type="button" class="btn btn-danger text-white" id="delTrue">Yes</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--deletePopUp modal CLOSE-->
    
    <script>
        $(document).ready(function(){
            var dirPath = "<?php dirPath() ?>";

            $('.delete').on('click', function() {
                var id = $(this).data('id');
                $('#delTrue').click(function(){
                    var request = $.ajax({
                        url: dirPath + "/dashboard/req/delete.php",
                        type: "POST",
                        data: {
                            id : id,
                            key : 'ACL_ID',
                            table : 'ACL'
                            },
                        dataType: "html"
                    });

                    request.done(function(msg) {
                        console.log(msg);
                        $("#deletePopUp div div div div").html("ACL Deleted.<br><small>page will reload in 3 seconds</small>");
                        /// wait 3 seconds
                        setTimeout(function() {
                            window.location.reload(false)
                        }, 3000);
                    });

                    request.fail(function(jqXHR, textStatus) {
                        $("#deletePopUp div div div div").html("Error - Request failed: " + textStatus);
                    });
                });
            });

            $('.edit').on('click', function(){
                var id = $(this).data('id');
                var request = $.ajax({
                    url: dirPath + "/dashboard/req/edit.php",
                    type: "POST",
                    data: {
                        id : id,
                        table : 'ACL'
                        },
                    dataType: "html"
                });

                request.done(function(msg) {
                    var obj = JSON.parse(msg);
                    document.getElementById('editId').value = obj.ACL_ID;
                    document.getElementById('editParameter').value = obj.ACL_Parameter;
                    document.getElementById('editCategory').value = obj.ACL_Category;
                });

                request.fail(function(jqXHR, textStatus) {
                    $("#editPopUp div div div div").html("Error - Request failed: " + textStatus);
                });
            });
            $('#editACL').on('click', function(){
                var id = document.querySelector('#editId').value;
                var parameter = document.querySelector('#editParameter').value;
                var category = document.querySelector('#editCategory').value;
                
                if(parameter != "" || category != ""){
                    var request = $.ajax({
                        url: dirPath + "/dashboard/req/update.php",
                        type: "POST",
                        data: {
                            id : id,
                            parameter : parameter,
                            category : category,
                            table : 'ACL'
                            },
                        dataType: "html"    
                    });

                    request.done(function(msg) {
                        $("#editPopUp div div div div").html("ACL Updated.<br><small>page will reload in 3 seconds</small>");
                        /// wait 3 seconds
                        setTimeout(function() {
                            window.location.reload(false)
                        }, 3000);
                    });

                    request.fail(function(jqXHR, textStatus) {
                        $("#editPopUp div div div div").html("Error - Request failed: " + textStatus);
                    });
                }
                else{
                    $("#editPopUp div div div div").html("Error - Textfield Empty");
                    /// wait 3 seconds
                    setTimeout(function() {
                        window.location.reload(false)
                    }, 2000);
                }
            });
        });
    </script>

</body>
</html>