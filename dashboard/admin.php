<?php 
    require_once(__DIR__."/inc/header.php");
?>
    <title><?php echo $title ?></title>
    <style>
    .c-body a{
        text-decoration: none;
    }
    .c-body a:hover h1{
        color: #aaaaaa;
    }
    </style>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                        <div class="row w-100">    
                            <div class="col-12 py-3 px-4">
                                <h4>Administrator</h4>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <a href="<?php dirPath() ?>/dashboard/contractors/list">
                                <div class="card text-white bg-gradient-dark">
                                    <div class="card-body d-flex justify-content-between align-items-start pb-2 pt-3">
                                        <div class="row w-100">
                                            <div class="col-12">
                                                <h1>
                                                    <i class="cil-face"></i>
                                                </h1>
                                                <h4>Contractors</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <a href="<?php dirPath() ?>/dashboard/assignee/list">
                                <div class="card text-white bg-gradient-dark">
                                    <div class="card-body d-flex justify-content-between align-items-start pb-2 pt-3">
                                        <div class="row w-100">
                                            <div class="col-12">
                                                <h1>
                                                    <i class="cil-contact"></i>
                                                </h1>
                                                <h4>Assignee</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <a href="<?php dirPath() ?>/dashboard/beneficiary/list">
                                <div class="card text-white bg-gradient-dark">
                                    <div class="card-body d-flex justify-content-between align-items-start pb-2 pt-3">
                                        <div class="row w-100">
                                            <div class="col-12">
                                                <h1>
                                                    <i class="cil-bank"></i>
                                                </h1>
                                                <h4>Beneficiary</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <a href="<?php dirPath() ?>/dashboard/acl/list">
                                <div class="card text-white bg-gradient-dark">
                                    <div class="card-body d-flex justify-content-between align-items-start pb-2 pt-3">
                                        <div class="row w-100">
                                            <div class="col-12">
                                                <h1>
                                                    <i class="cib-buffer"></i>
                                                </h1>
                                                <h4>ACL</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <a href="<?php dirPath() ?>/dashboard/budget-group/list">
                                <div class="card text-white bg-gradient-dark">
                                    <div class="card-body d-flex justify-content-between align-items-start pb-2 pt-3">
                                        <div class="row w-100">
                                            <div class="col-12">
                                                <h1>
                                                    <i class="cil-money"></i>
                                                </h1>
                                                <h4>Budget Group</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div><!--row-->
                    </div>
                </div>
            </main>
        </div>
        
        <?php require_once(__DIR__."/inc/footer.php"); ?>
    </div>

</body>
</html>