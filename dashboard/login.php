<?php 
    require_once(__DIR__.'/inc/header.php'); 

    killActiveSessions();

    if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['login'])){
        $model = new \Libraries\Model();
        $user = $model->getUserByEmail($_POST['email']);
        if($user && password_verify($_POST['password'], $user->password)){
            session_start();
            $_SESSION['user']['id'] = $user->id;
            $_SESSION['user']['name'] = $user->name;
            $_SESSION['user']['email'] = $user->email;
            $_SESSION['user']['auth'] = TRUE;
            redirectTo('/dashboard');
        }
        else{
            $alert = [
                'type' => 'warning',
                'message' => 'Email or Password Incorrect!<br><small>contact system adminsitrator to reset password.</small>'
            ];
        }
    }
?>
    <title>Login - <?php echo $title ?></title>
</head>
<body class="c-app flex-row align-items-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                <h1>Login</h1>
                <p class="text-muted">Sign In to your account</p>
                <form action="<?php $_PHP_SELF ?>" method="post">
                <div class="input-group mb-3">
                  <div class="input-group-prepend"><span class="input-group-text">
                      <div class="c-icon">
                        <i class="cil-user"></i>
                      </div></span></div>
                  <input class="form-control" type="email" name="email" placeholder="Email" required>
                </div>
                <div class="input-group mb-4">
                  <div class="input-group-prepend"><span class="input-group-text">
                      <div class="c-icon">
                        <i class="cil-lock-locked"></i>
                      </div></span></div>
                  <input class="form-control" type="password" name="password" placeholder="Password" required>
                </div>
                <div class="row">
                  <div class="col-6">
                    <button class="btn btn-primary px-4" name="login" type="submit">Login</button>
                  </div>
                </div>
                </form>
              </div>
            </div>
            <div class="card text-white bg-primary py-auto d-md-down-none" style="width:44%">
              <div class="card-body text-center py-5">
                <div class="py-5">

                    <h4>Notice</h4>
                  <p>You are trying to access a Private Site. <br><hr>If you are a member and having trouble logging in, contact your system administrator through email.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="<?php dirPath() ?>/node_modules/@coreui/coreui/dist/js/coreui.bundle.min.js"></script>
  </body>

</body>
</html>