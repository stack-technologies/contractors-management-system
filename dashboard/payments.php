<?php 
    require_once(__DIR__."/inc/header.php"); 
    $model = new \Libraries\Model();
    $status = $_REQUEST['status'];
    $startDate = $_REQUEST['startdate'];
    $endDate = $_REQUEST['enddate'];

    $payments = $model->getPaymentTermsByDates($startDate, $endDate, $status);
?>
    <title>List of Payments - <?php echo $title ?></title>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                        <div class="row justify-content-center">
                            <div class="col-12 content-header pb-3">
                                <div class="row">
                                    <div class="col-12 col-sm-4 my-auto">
                                        <h2>List of Payments</h2>
                                    </div>
                                    <div class="col-12 col-sm-8">
                                        <div class="row">
                                            <div class="col-12 col-sm-4">
                                                <input type="date" class="form-control form-control-sm yearAmountDateSelector mb-2" name="startdate" id="startdate" value="<?php echo $startDate ?>" placeholder="Start Date">
                                            </div>
                                            <div class="col-12 col-sm-4">
                                                <input type="date" class="form-control form-control-sm yearAmountDateSelector" name="enddate" id="enddate" value="<?php echo $endDate ?>" placeholder="End Date">
                                            </div>
                                            <div class="col-12 col-sm-2">
                                                <select name="status" id="status" class="form-control form-control-sm">
                                                    <option value="Paid">Paid</option>
                                                    <option value="Due">Due</option>
                                                    <option value="Pending">Pending</option>
                                                    <option value="Cancelled">Cancelled</option>
                                                </select>
                                            </div>
                                            <div class="col-12 col-sm-2">
                                                <button class="btn btn-sm btn-primary" id="search">Search</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <table class="table table-hover table-responsive-sm table-striped content-table text-center">
                                    <thead>
                                        <tr>
                                            <th>Payment ID</th>
                                            <th>Contract Title</th>
                                            <th>Contract Progress</th>
                                            <th>Payment Amount</th>
                                            <th>Payment Due Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach($payments as $payment){
                                            $contract = $model->getContractById($payment->Contract_ID);
                                            echo '
                                            <tr>
                                                <td>'.$payment->CPT_ID.'</td>
                                                <td><a href="'.$GLOBALS['configurations']['dirpath'].'/dashboard/contracts/view/'.$contract->Contract_ID.'">'.$contract->Contract_Title.'</a></td>
                                                <td>'.$contract->Progress.'%</td>
                                                <td>$'.number_format($payment->Payment_Amount, 2).'</td>
                                                <td>'.$payment->Payment_Due_Date.'</td>
                                            </tr>
                                            ';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.row-->
                        </div>
                    </div>
                </div>    
            </main>
        </div>
        
        <?php require_once(__DIR__."/inc/footer.php"); ?>

    </div>

<script>
    $(document).ready(function(){
        var dirPath = "<?php dirPath() ?>";
        var stat = "<?php echo ucfirst($status) ?>";
        document.getElementById('status').value = stat;
        $('#search').click(function(){
            var startdate = $('#startdate').val();
            var enddate = $('#enddate').val();
            var status = $('#status').val();

            window.location.href = dirPath + "/dashboard/payments?startdate="+startdate+"&enddate="+enddate+"&status="+status;
        });
    })
</script>

</body>
</html>