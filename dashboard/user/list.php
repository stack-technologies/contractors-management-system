<?php 
    require_once(__DIR__."/../inc/header.php"); 
    $model = new \Libraries\Model();
    $users = $model->getUsers();
?>
    <title>List of Users - <?php echo $title ?></title>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/../inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/../inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                    <?php require(__DIR__.'/../inc/alert.php'); ?>
                        <div class="row justify-content-center">
                            <div class="col-12 content-header pb-3">
                                <div class="row px-3">
                                    <div class="col-6">
                                        <h2>List of Users</h2>
                                    </div>
                                    <div class="col-6 text-right">
                                        <a href="<?php dirpath() ?>/dashboard/user/create" class="btn btn-primary bg-gradient-primary text-right">Add New Record</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <table class="table table-hover table-responsive-sm table-striped content-table text-center">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        foreach($users as $user){
                                            echo '
                                            <tr>
                                                <td>'.$user->id.'</td>
                                                <td>'.$user->name.'</td>
                                                <td>'.$user->email.'</td>
                                                <td>
                                                    <a class="btn btn-sm btn-link edit" data-id="'.$user->id.'" data-toggle="modal" data-target="#editPopUp">
                                                        <div class="c-icon">
                                                            <i class="cil-external-link"></i>
                                                        </div>
                                                    </a>
                                                    <a class="btn btn-sm btn-link delete" data-id="'.$user->id.'" data-toggle="modal" data-target="#deletePopUp">
                                                        <div class="c-icon">
                                                            <i class="cil-trash"></i>
                                                        </div>
                                                    </a>
                                                </td>
                                            </tr>
                                            ';
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.row-->
                        </div>
                    </div>
                </div>    
            </main>
        </div>
        
        <?php require_once(__DIR__."/../inc/footer.php"); ?>

    </div>

    <div id="editPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10" id="editInfo">
                                <h5>Edit Information</h5>
                                <p>Please Enter the new Information Below.</p>
                            </div>
                            <div class="col-12">
                                <div class="content-form mx-auto">
                                    <input type="number" name="editId" id="editId" required hidden>
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="editName" id="editName" class="form-control" data-sanitize="escape" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" name="editEmail" id="editEmail" class="form-control" data-validation="email" data-sanitize="escape" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="text" name="editPassword" id="editPassword" class="form-control" data-sanitize="escape">
                                        <small class="text-muted">leave password empty if not changed</small>
                                    </div>
                                    <button name="editUser" id="editUser" class="btn btn-primary">EDIT USER</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--editPopUp modal CLOSE-->

    <div id="deletePopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10">
                                <h5>Are You Sure You Want To Delete?</h5>
                                <br>
                                <button type="button" class="btn btn-danger text-white" id="delTrue">Yes</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--deletePopUp modal CLOSE-->
    
    <script>
        $.validate({
            modules : 'sanitize',
        });

        $(document).ready(function(){
            var dirPath = "<?php dirPath() ?>";

            $('.delete').on('click', function() {
                var id = $(this).data('id');
                $('#delTrue').click(function(){
                    var request = $.ajax({
                        url: dirPath + "/dashboard/req/delete.php",
                        type: "POST",
                        data: {
                            id : id,
                            key : 'id',
                            table : 'users'
                            },
                        dataType: "html"
                    });

                    request.done(function(msg) {
                        console.log(msg);
                        $("#deletePopUp div div div div").html("User Deleted.<br><small>page will reload in 3 seconds</small>");
                        /// wait 3 seconds
                        setTimeout(function() {
                            window.location.reload(false)
                        }, 3000);
                    });

                    request.fail(function(jqXHR, textStatus) {
                        $("#deletePopUp div div div div").html("Error - Request failed: " + textStatus);
                    });
                });
            });
            $("#deletePopUp").on('hide.bs.modal', function(){
                $("#deletePopUp div div div div").html('<h3>Are You Sure You Want To Delete <span></span></h3><button type="button" class="btn btn-warning" id="delTrue">Yes</button><button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">No</button>');
            });

            $('.edit').on('click', function(){
                var id = $(this).data('id');
                var request = $.ajax({
                    url: dirPath + "/dashboard/req/edit.php",
                    type: "POST",
                    data: {
                        id : id,
                        table : 'users'
                        },
                    dataType: "html"
                });

                request.done(function(msg) {
                    var obj = JSON.parse(msg);
                    document.getElementById('editId').value = obj.id;
                    document.getElementById('editName').value = obj.name;
                    document.getElementById('editEmail').value = obj.email;
                    document.getElementById('editPassword').value = "";
                });

                request.fail(function(jqXHR, textStatus) {
                    $("#editPopUp div div div div").html("Error - Request failed: " + textStatus);
                });
            });
            $('#editUser').on('click', function(){
                var id = document.querySelector('#editId').value;
                var name = document.querySelector('#editName').value;
                var email = document.querySelector('#editEmail').value;
                var password = document.querySelector('#editPassword').value;
                
                var request = $.ajax({
                    url: dirPath + "/dashboard/req/update.php",
                    type: "POST",
                    data: {
                        id : id,
                        name : name,
                        email : email,
                        password: password,
                        table : 'users'
                        },
                    dataType: "html"    
                });

                request.done(function(msg) {
                    $("#editPopUp div div div div").html("User Updated.<br><small>refresh page to see changes</small>");
                });

                request.fail(function(jqXHR, textStatus) {
                    $("#editPopUp div div div div").html("Error - Request failed: " + textStatus);
                });
            });
            $("#editPopUp").on('hide.bs.modal', function(){
                $("#editPopUp div div div div").html('<div class="row"><div class="col-12" id="editInfo"><h5>Edit Information</h5><p>Please Enter the new Information Below.</p></div><div class="col-12"><div class="content-form mx-auto"><input type="number" name="editId" id="editId" required hidden><div class="form-group"><label for="name">Name</label><input type="text" name="editName" id="editName" class="form-control" required></div><div class="form-group"><label for="email">Email</label><input type="text" name="editEmail" id="editEmail" class="form-control" required></div><div class="form-group"><label for="password">Password</label><input type="text" name="editPassword" id="editPassword" class="form-control"><small class="text-muted">leave password empty if not changed</small></div><button name="editUser" id="editUser" class="form-control btn btn-dark">EDIT USER</button></div></div></div>');
            });
        });
    </script>

</body>
</html>