<?php 
    require_once(__DIR__."/../inc/header.php"); 
    $model = new \Libraries\Model();
    if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['create'])){
        $check_email = $model->checkEmailExists($_POST['email']);
        if($check_email){
            $alert = [
                'type' => 'warning',
                'message' => 'Error Creating User <b>'.$_POST['name'].'</b>. Email Already Exists.'
            ];
        }
        else{
            $create_user = $model->createUser([
                'name' => $_POST['name'],
                'email' => $_POST['email'],
                'password' => password_hash($_POST['password'], PASSWORD_BCRYPT)
            ]);
            if($create_user){
                $alert = [
                    'type' => 'success',
                    'message' => 'User Created <b>'.$_POST['name'].'</b>'
                ];
            }
            else{
                $alert = [
                    'type' => 'danger',
                    'message' => 'Error Creating User <b>'.$_POST['name'].'</b>. Server Error, Contact Adminstrator/Developer.'
                ];
            }
        }
    }

?>
    <title>Create a New ACL - <?php echo $title ?></title>
    <style>
        .twitter-typeahead{
            width: 100%;
        }
        .tt-menu{
            background-color: white;
            border-bottom: 1px solid grey;
            border-left: 1px solid grey;
            border-right: 1px solid grey;
            border-bottom-right-radius: 5px;
            border-bottom-left-radius: 5px;
            width: 100%;
            cursor: pointer;
        }
        .tt-selectable{
            padding: 5px 20px;
        }
        .tt-selectable:hover{
            background-color: lightgrey;
        }
    </style>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/../inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/../inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                    <?php require(__DIR__.'/../inc/alert.php'); ?>
                        <div class="row justify-content-center">
                            <div class="col-sm-6 col-md-8 col-lg-10">
                                <form action="<?php $_PHP_SELF ?>" method="POST">
                                    <div class="card">
                                        <div class="card-header text-center"><strong>Create User</strong></div>
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input class="form-control" id="name" type="text" name="name" placeholder="Enter Name.." data-sanitize="escape" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input class="form-control" id="email" type="email" name="email" placeholder="Enter Email.." autocomplete="false" data-validation="email" data-sanitize="escape" required>
                                            </div>    
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <input class="form-control" id="password" type="password" name="password" placeholder="Enter Password.." data-sanitize="escape" required> 
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <button class="btn btn-sm btn-dark" name="create" type="submit">CREATE</button>
                                        </div>
                                    </div>
                                </form>                                
                                <!-- /.col-->
                            </div>
                            <!-- /.row-->
                        </div>
                    </div>
                </div>    
            </main>
        </div>
        
        <?php require_once(__DIR__."/../inc/footer.php"); ?>
        <script>
            $.validate({
                modules : 'sanitize'
            });
        </script>
    </div>
</body>
</html>