<?php 
    require_once(__DIR__."/../inc/header.php"); 
    $model = new \Libraries\Model();
    $permits = $model->getPermits();

    if($_SERVER['REQUEST_METHOD'] == "POST"){
        /** Updating Permit Status and Getting TRUE OR FALSE in return **/
        $complete_status = $model->createPermitStatus([
            'permit_id' => $_POST['editId'],
            'status' => $_POST['status'], 
            'status_notes' => $_POST['notes'], 
            'user' => $_SESSION['user']['id'],
        ]);
        $permit_id = $_POST['editId'];
        if($complete_status != FALSE){
            $model->updatePermitStatusID($complete_status, $_POST['editId']);
            if($_POST['status'] == "Granted"){
                define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/contractors');
                $name = $_FILES['file']['name'];
                $current_timestamp = date('dmYhis', time());
                $file_class = "PERMIT";
                $file_extension = end(explode(".", $name));
                $filename = $file_class . $permit_id . $current_timestamp . '.' . $file_extension;
                if (file_exists(ROOT_DIR."/uploads/permits/" . $filename)){
                    $filename = $file_class . $permit_id . date('dmYhis', time()) . '.' . $file_extension;
                }
                if(!is_dir(ROOT_DIR."/uploads/permits/")){
                    mkdir(ROOT_DIR."/uploads/permits/", 0777, true);
                }
                move_uploaded_file($_FILES['file']["tmp_name"], ROOT_DIR."/uploads/permits/". $filename);
                $file_link = "uploads/permits/". $filename;
                /** Creating File Record in the Database and Getting File_ID in return **/
                $create_permitfile = $model->createPermitFile([
                    'permit_id' => $_POST['editId'],
                    'file_class' => $file_class,
                    'file_link' => $file_link,
                    'user_id' => $_SESSION['user']['id'],
                ]);
                $complete_permit = $model->completePermit([
                    'id' => $permit_id, 
                    'issue_date' => $_POST['permit_issue_date'], 
                    'permit_file_id' => $create_permitfile
                ]);

                if($complete_permit){
                    $alert = [
                        'type' => 'success',
                        'message' => 'Permit Updated.<br><small>Refresh to view changes.</small>'
                    ]; 
                }
                else{
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Error Updating Permit # <b>'.$permit_id.'</b>. Server Error, Contact Adminstrator/Developer.'
                    ];
                }
            }
            else{
                $alert = [
                    'type' => 'success',
                    'message' => 'Permit Updated.<br><small>Refresh to view changes.</small>'
                ];
            }
        }
    }

?>
    <title>List of Permits - <?php echo $title ?></title>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/../inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/../inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                    <?php require(__DIR__.'/../inc/alert.php'); ?>
                        <div class="row justify-content-center">
                            <div class="col-12 content-header pb-3">
                                <div class="row px-3">
                                    <div class="col-6">
                                        <h2>List of Permits</h2>
                                    </div>
                                    <div class="col-6 text-right">
                                        <a href="<?php dirpath() ?>/dashboard/permits/create" class="btn btn-primary bg-gradient-primary text-right">Add New Record</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <table class="table table-hover table-responsive-sm table-striped content-table">
                                    <thead class="table-borderless">
                                        <tr>
                                            <th>ID</th>
                                            <th>Access Name</th>
                                            <th>Requester</th>
                                            <th>
                                                <small>(Start Date <b>-</b> End Date)</small><br>
                                                Permit Peroid
                                            </th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach($permits as $permit){
                                            $status = $model->getPermitStatusById($permit->Permit_Status_ID)->Status;
                                            echo '
                                            <tr>
                                                <td>'.$permit->Permit_ID.'</td>
                                                <td>'.$permit->Access_Name.'</td>
                                                <td>'.$permit->Beneficiary.'</td>
                                                <td>'.str_replace('-', '/', $permit->Permit_Start_Date).' <b>-</b> '.str_replace('-', '/', $permit->Permit_End_Date).'</td>
                                                
                                                <td>'.$status.'</td>
                                                <td>
                                                    <a href="'.$GLOBALS['configurations']['dirpath'].'/dashboard/permits/view/'.$permit->Permit_ID.'" class="btn btn-sm btn-link" title="View"><i class="cil-external-link"></i></a>';
                                                    if($status == "Pending" || $status == "In-Process"){
                                                        echo '<a class="btn btn-sm btn-link text-primary edit" data-id="'.$permit->Permit_Status_ID.'" data-toggle="modal" data-target="#editPopUp" title="Edit"><i class="cil-pencil"></i></a>';
                                                    }
                                                    echo '
                                                </td>
                                            </tr>
                                            ';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.row-->
                        </div>
                    </div>
                </div>    
            </main>
        </div>
        
        <?php require_once(__DIR__."/../inc/footer.php"); ?>

    </div>

    <div id="editPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10" id="editInfo">
                                <h5>Edit Information</h5>
                                <p>Please Enter the new Information Below.</p>
                            </div>
                            <div class="col-12">
                                <form action="<?php $_PHP_SELF ?>" method="POST" id="updatePermitForm" enctype="multipart/form-data" class="needs-validation" novalidate>
                                    <div class="row justify-content-center">
                                        <div class="col-12 pb-4">
                                            <div class="card h-100">
                                                <div class="card-header bg-light"><strong>Permit Status Information</strong></div>
                                                <div class="card-body">
                                                    <input type="number" name="editId" id="editId" required hidden>
                                                    <div class="form-group">
                                                        <label for="status">Status</label>
                                                        <select name="status" id="status" class="form-control">
                                                            <option value="Pending">Pending</option>
                                                            <option value="In-Process">In-Process</option>
                                                            <option value="Granted">Granted</option>
                                                            <option value="Rejected">Rejected</option>
                                                            <option value="Cancelled">Cancelled</option>
                                                        </select>
                                                    </div>
                                                    <div id="additional_info">
                                                        <div class="form-group">
                                                            <label for="permit_issue_date">Permit Issue Date</label>
                                                            <input class="form-control" id="permit_issue_date" type="date" name="permit_issue_date" placeholder="yyyy-mm-dd" data-validation="date" disabled required>
                                                        </div>
                                                        <label for="file">Permit (<a href="<?php echo $GLOBALS['configurations']['dirpath'] .'/'.$permit_file_details->File_Link ?>">Current File Here</a>)</label><br>
                                                        <small class="text-muted">Selecting a New file will remove the previous one from this permit</small>
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" name="file" id="file" disabled>
                                                            <label class="custom-file-label" for="customFile">Choose New Permit File</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="notes">Notes</label>
                                                        <textarea name="notes" id="notes" class="form-control" rows="5" data-sanitize="escape"></textarea>
                                                    </div>
                                                </div>
                                            </div><!--card--> 
                                        </div><!--col-->
                                    </div><!--row-->
                                    <div class="card">
                                        <div class="card-footer">
                                            <button class="btn btn-primary w-100" name="editPermit" id="editPermit" type="button">EDIT PERMIT</button>
                                        </div>
                                    </div>    
                                </form>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--editPopUp modal CLOSE-->

    <script>
    //VALIDATIONS OPEN
    $.validate({
        modules : 'sanitize',
    });

    (function () {
        'use strict';
        var forms = document.querySelectorAll('.needs-validation');
        Array.prototype.slice.call(forms)
        .forEach(function (form) {
            $('#editPermit').on('click', function(){
                if (!form.checkValidity()) {
                    form.classList.add('was-validated');
                }
                else{
                    $('#updatePermitForm').submit();
                }
            })
        });
    })();
        //VALIDATIONS CLOSE
    $(document).on('change', '#status', function(){
        var val = $(this).val();
        if(val == "Granted"){
            $('#additional_info').show();
            $('#permit_issue_date').removeAttr("disabled");
            $('#file').removeAttr("disabled");
        }
        else{
            $('#additional_info').hide();
            $('#permit_issue_date').attr("disabled", "disabled");
            $('#file').attr("disabled", "disabled");
        }
    });
    $(document).ready(function(){
        var dirPath = "<?php dirPath() ?>";
        $('.edit').on('click', function(){
            var id = $(this).data('id');
            
            var request = $.ajax({
                url: dirPath + "/dashboard/req/edit.php",
                type: "POST",
                data: {
                    id : id,
                    table : 'Permit_Status'
                    },
                dataType: "html"
            });
            
            request.done(function(msg) {
                var obj = JSON.parse(msg);
                document.getElementById('editId').value = obj.Permit_ID;
                document.getElementById('status').value = obj.Status;
                document.getElementById('notes').value = obj.Status_Notes;
                if(obj.Status == "Granted"){
                    $('#additional_info').show();
                    $('#permit_issue_date').removeAttr("disabled");
                    $('#file').removeAttr("disabled");
                    $('#permit_issue_date').val('');
                }
                else{
                    $('#additional_info').hide();
                    $('#permit_issue_date').attr("disabled", "disabled");
                    $('#file').attr("disabled", "disabled");
                }
            });

            request.fail(function(jqXHR, textStatus) {
                $("#editPopUp div div div div").html("Error - Request failed: " + textStatus);
            });
        });
    });

    </script>

</body>
</html>