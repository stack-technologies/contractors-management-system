<?php 
    require_once(__DIR__."/../inc/header.php"); 
    $model = new \Libraries\Model();
    $contractors = $model->getContractors();
    $beneficiaries = $model->getBeneficiary();
    $assignees = $model->getAssignees();
    $permits = $model->getPermits();

    foreach($beneficiaries as $beneficiary){
        $list_of_beneficiaries[] = $beneficiary->Beneficiary_Name;
    }
    foreach($assignees as $assignee){
        $list_of_assignees[] = $assignee->Assignee_Name;
    }
    foreach($permits as $permit){
        $list_of_locations[] = $permit->Permit_Location;
    }

    if($_SERVER['REQUEST_METHOD'] == "POST"){
        $create = $model->createPermit([
            'access_name' => $_POST['access_name'],
            'id_number' => $_POST['id_number'], 
            'permit_start_date' => $_POST['permit_start_date'],
            'permit_end_date' => $_POST['permit_end_date'],
            'permit_location' => $_POST['permit_location'],
            'request_date' => $_POST['permit_request_date'],
            'notes' => $_POST['notes'],
            'contractor_id' => $_POST['contractor'],
            'beneficiary' => $_POST['beneficiary'],
            'assignee' => $_POST['assignee'],
            'createdby_user_id' => $_SESSION['user']['id'],
        ]);
        if($create != 'FALSE'){
            /** Creating Status and Getting Status_ID in return **/
            $create_status = $model->createPermitStatus([
                'permit_id' => $create,
                'status' => $_POST['status'], 
                'status_notes' => $_POST['status_notes'], 
                'user' => $_SESSION['user']['id'],
            ]);
            if($_POST['status'] == "Granted"){
                define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/contractors');
                $name = $_FILES['permit_file']['name'];
                $current_timestamp = date('dmYhis', time());
                $file_class = "PERMIT";
                $file_extension = end(explode(".", $name));
                $filename = $file_class . $create . $current_timestamp . '.' . $file_extension;
                if (file_exists(ROOT_DIR."/uploads/permits/" . $filename)){
                    $filename = $file_class . $create . date('dmYhis', time()) . '.' . $file_extension;
                }
                if(!is_dir(ROOT_DIR."/uploads/permits/")){
                    mkdir(ROOT_DIR."/uploads/permits/", 0777, true);
                }
                move_uploaded_file($_FILES['permit_file']["tmp_name"], ROOT_DIR."/uploads/permits/". $filename);
                $file_link = "uploads/permits/". $filename;
                
                /** Creating File Record in the Database and Getting File_ID in return **/
                $create_permitfile = $model->createPermitFile([
                    'permit_id' => $create,
                    'file_class' => $file_class,
                    'file_link' => $file_link,
                    'user_id' => $_SESSION['user']['id'],
                ]);
                
                $complete_permit = $model->completePermit([
                    'id' => $create, 
                    'issue_date' => $_POST['permit_issue_date'], 
                    'permit_file_id' => $create_permitfile
                ]);
            }

            /** Getting Files Details **/
            define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/contractors');
            $filenum = 1;
            foreach ($_FILES['files']['name'] as $key => $name) {
                $current_timestamp = date('dmYhis', time());
                if ($_FILES['files']['error'][$key] == 4) {
                    continue;
                }
                $file_class = "ID";
                $file_extension = end(explode(".", $name));
                $filename = $file_class . $create . "F" . $filenum . $current_timestamp . '.' . $file_extension;
                if (file_exists(ROOT_DIR."/uploads/permits/id/" . $filename)){
                    $filename = $file_class . $create . "F" . $filenum . date('dmYhis', time()) . '.' . $file_extension;
                }
                if(!is_dir(ROOT_DIR."/uploads/permits/id/")){
                    mkdir(ROOT_DIR."/uploads/permits/id/", 0777, true);
                }
                move_uploaded_file($_FILES['files']["tmp_name"][$key], ROOT_DIR."/uploads/permits/id/". $filename);
                $file_link = "uploads/permits/id/". $filename;
                $filenum++;

                /** Creating File Record in the Database and Getting File_ID in return **/
                $create_file[] = $model->createPermitFile([
                    'permit_id' => $create,
                    'file_class' => $file_class,
                    'file_link' => $file_link,
                    'user_id' => $_SESSION['user']['id'],
                ]);
            }
            $file_ids = implode(",",$create_file);

            /** Updating the Contract Details with Payment, Status and Files **/
            if($create_status != 'FALSE' || $file_ids != ''){
                $update_permit = $model->updatePermitAfterCreation([
                    'permit_id' => $create,
                    'permit_status_id' => $create_status,
                    'files' => $file_ids,
                ]);
                if($update_permit){
                    $alert = [
                        'type' => 'success',
                        'message' => 'Permit Created <b>'.$_POST['access_name'].'</b>.'
                    ];
                    // sleep(3);
                    // header('Location: '. $GLOBALS['configurations']['dirpath'] . '/dashboard/permits/complete/'.$create);
                }
            }
        }
        else{
            $alert = [
                'type' => 'danger',
                'message' => 'Error Creating Permit <b>'.$_POST['access_name'].'</b>. Server Error, Contact Adminstrator/Developer.'
            ];
        }
    }
?>
    <title>Create Permits - <?php echo $title ?></title>
    <style>
        .twitter-typeahead{
            width: 100%;
        }
        .tt-menu{
            background-color: white;
            border-bottom: 1px solid grey;
            border-left: 1px solid grey;
            border-right: 1px solid grey;
            border-bottom-right-radius: 5px;
            border-bottom-left-radius: 5px;
            width: 100%;
            cursor: pointer;
        }
        .tt-selectable{
            padding: 5px 20px;
        }
        .tt-selectable:hover{
            background-color: lightgrey;
        }
        #additional_info{
            display: none;
        }
    </style>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/../inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/../inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                    <?php require(__DIR__.'/../inc/alert.php'); ?>
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <form action="<?php $_PHP_SELF ?>" method="POST" id="createPermitForm" enctype="multipart/form-data" class="needs-validation" novalidate>
                                    <h3 class="text-center p-3 text-uppercase font-weight-bold">Add a New Permit</h3>
                                    <div class="row">
                                        <div class="col-md-6 pb-4">
                                            <div class="card h-100">
                                                <div class="card-header bg-light"><strong>Permit Request Information</strong></div>
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label for="access_name">Access Name</label>
                                                        <input class="form-control" id="access_name" type="text" name="access_name" data-sanitize="escape" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="files"><strong>ID</strong> (Passport/Civil/etc.)</label>
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" name="files[]" id="files" multiple required>
                                                            <label class="custom-file-label" for="customFile">Choose Multiple files</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="id_number">ID Number</label>
                                                        <input class="form-control" id="id_number" type="text" name="id_number" data-sanitize="escape" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <label for="permit_start_date">Start Date</label>
                                                                <input class="form-control" id="permit_start_date" type="date" name="permit_start_date" placeholder="yyyy-mm-dd" data-validation="date" required>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label for="permit_end_date">End Date</label>
                                                                <input class="form-control" id="permit_end_date" type="date" name="permit_end_date" placeholder="yyyy-mm-dd" data-validation="date" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="permit_location">Permit Location</label>
                                                        <input class="form-control" type="text" id="permit_location" name="permit_location" data-sanitize="escape" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="contractor">Contractor</label>
                                                        <select name="contractor" id="contractor" class="form-control">
                                                            <option value="0">Select...</option>
                                                        <?php
                                                        foreach($contractors as $contractor){
                                                            echo '<option value="'.$contractor->Contractor_ID.'">'.$contractor->Company_Name.'</option>';
                                                        }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="beneficiary">Beneficiary</label><br>
                                                        <input class="form-control" id="beneficiary" type="text" name="beneficiary" data-sanitize="escape">
                                                    </div>  
                                                    <div class="form-group">
                                                        <label for="permit_request_date">Request Date</label>
                                                        <input class="form-control" id="permit_request_date" type="date" name="permit_request_date" placeholder="yyyy-mm-dd" data-validation="date">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="notes">Permit Request Notes</label>
                                                        <textarea name="notes" id="notes" class="form-control" rows="5" data-sanitize="escape"></textarea>
                                                    </div>
                                                </div>
                                            </div><!--card--> 
                                        </div><!--col-->
                                        <div class="col-md-6 pb-4">
                                            <div class="card h-100">
                                                <div class="card-header bg-light"><strong>Request Status</strong></div>
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label for="assignee">Assignee</label><br>
                                                        <input class="form-control" id="assignee" type="text" name="assignee" data-sanitize="escape">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="status">Status</label>
                                                        <select name="status" id="status" class="form-control">
                                                            <option value="Pending">Pending</option>
                                                            <option value="In-Process">In-Process</option>
                                                            <option value="Granted">Granted</option>
                                                            <option value="Rejected">Rejected</option>
                                                            <option value="Cancelled">Cancelled</option>
                                                        </select>
                                                    </div>
                                                    <div id="additional_info">
                                                        <div class="form-group">
                                                            <label for="permit_issue_date">Permit Issue Date</label>
                                                            <input class="form-control" id="permit_issue_date" type="date" name="permit_issue_date" placeholder="yyyy-mm-dd" data-validation="date" disabled required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="permit_file">Permit</label>
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input" name="permit_file" id="permit_file" disabled required>
                                                                <label class="custom-file-label" for="customFile">Choose Permit File</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="status_notes">Status Notes</label>
                                                        <textarea name="status_notes" id="status_notes" class="form-control" rows="5" data-sanitize="escape"></textarea>
                                                    </div>
                                                </div>
                                            </div>    
                                        </div><!--col-->
                                    </div><!--row-->
                                    <div class="card">
                                        <div class="card-footer">
                                            <button class="btn btn-primary w-100" name="create" id="create" type="button">CREATE</button>
                                        </div>
                                    </div>    
                                </form>
                                <!-- /.col-->
                            </div>
                            <!-- /.row-->
                        </div>
                    </div>
                </div>    
            </main>
        </div>

        <?php require_once(__DIR__."/../inc/footer.php"); ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
        <script type="text/javascript">
            //VALIDATIONS OPEN
            $.validate({
                modules : 'sanitize',
            });

            (function () {
                'use strict';
                var forms = document.querySelectorAll('.needs-validation');
                Array.prototype.slice.call(forms)
                .forEach(function (form) {
                    $('#create').on('click', function(){
                        if (!form.checkValidity()) {
                            form.classList.add('was-validated');
                        }
                        else{
                            $('#createPermitForm').submit();
                        }
                    })
                });
            })();
            //VALIDATIONS CLOSE

            $(document).ready(function(){
                // Defining the local dataset for Typeahead
                <?php
                function js_str($s){
                    return '"' . addcslashes($s, "\0..\37\"\\") . '"';
                }
                
                function js_array($array){
                    $temp = array_map('js_str', $array);
                    return '[' . implode(',', $temp) . ']';
                }
                
                echo 'var beneficiaries = ', js_array($list_of_beneficiaries), ';';
                echo 'var assignees = ', js_array($list_of_assignees), ';';
                echo 'var locations = ', js_array($list_of_locations), ';';
                ?>

                //Beneficiaries for TypeAhead
                var locations = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.whitespace,
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    local: locations
                });
                $('#permit_location').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                },
                {
                    name: 'Location',
                    source: locations
                });

                //Beneficiaries for TypeAhead
                var beneficiaries = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.whitespace,
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    local: beneficiaries
                });
                $('#beneficiary').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                },
                {
                    name: 'Beneficiary',
                    source: beneficiaries
                });

                //Assignees for TypeAhead
                var assignees = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.whitespace,
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    local: assignees
                });
                $('#assignee').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1 
                },
                {
                    name: 'assignees',
                    source: assignees
                });

                $('#sidebar').addClass('c-sidebar-minimized');
                var dirPath = "<?php dirPath() ?>";
                $('#status').change(function(){
                    var val = $(this).val();
                    if(val == "Granted"){
                        $('#additional_info').show();
                        $('#permit_issue_date').removeAttr("disabled");
                        $('#permit_file').removeAttr("disabled");
                    }
                    else{
                        $('#additional_info').hide();
                        $('#permit_issue_date').attr("disabled", "disabled");
                        $('#permit_file').attr("disabled", "disabled");
                    }
                });
            });
        </script>
    </div>
</body>
</html>