<?php 
    require_once(__DIR__."/../inc/header.php"); 
    $model = new \Libraries\Model();
    $getID = $_GET['id'];
    $permit = $model->getPermitById($getID);
    $permit_status = $model->getPermitStatusById($permit->Permit_Status_ID);
    $contractor = $model->getContractorById($permit->Contractor_ID)->Company_Name;
    $beneficiary = $permit->Beneficiary;
    $assignee = $permit->Assignee;
    $current_status = $permit_status->Status;
    $permit_start_date = $permit->Permit_Start_Date;
    $permit_end_date = $permit->Permit_End_Date;
    $permit_request_date = $permit->Request_Date;
    if($permit->ID_Files != ""){
        $files = explode(',', $permit->ID_Files);
        foreach($files as $file){
            $file_details = $model->getPermitFileById($file);
            $fileclass[] = $file_details->File_Class;
            $filetimestamp[] = $file_details->Upload_DateTime;
            $fileuser[] = $model->getUserById($file_details->User_ID)->name;
            $filelink[] = $file_details->File_Link;
        }
    }

    //Get List of Status
    $statuses = $model->getPermitStatusByPermitID($permit->Permit_ID);

    if($current_status == "Granted"){
        $permit_issue_date = $permit->Permit_Issue_Date;
        $permit_file_details = $model->getPermitFileById($permit->Permit_File);
    }

    if($_SERVER['REQUEST_METHOD'] == "POST"){
        /** Updating Permit Status and Getting TRUE OR FALSE in return **/
        $complete_status = $model->createPermitStatus([
            'permit_id' => $permit->Permit_ID,
            'status' => $_POST['status'], 
            'status_notes' => $_POST['notes'], 
            'user' => $_SESSION['user']['id'],
        ]);
        if($complete_status != FALSE){
            $model->updatePermitStatusID($complete_status, $permit->Permit_ID);
            if($_POST['status'] == "Granted"){
                define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/contractors');
                $name = $_FILES['file']['name'];
                $current_timestamp = date('dmYhis', time());
                $file_class = "PERMIT";
                $file_extension = end(explode(".", $name));
                $filename = $file_class . $permit_id . $current_timestamp . '.' . $file_extension;
                if (file_exists(ROOT_DIR."/uploads/permits/" . $filename)){
                    $filename = $file_class . $permit_id . date('dmYhis', time()) . '.' . $file_extension;
                }
                if(!is_dir(ROOT_DIR."/uploads/permits/")){
                    mkdir(ROOT_DIR."/uploads/permits/", 0777, true);
                }
                move_uploaded_file($_FILES['file']["tmp_name"], ROOT_DIR."/uploads/permits/". $filename);
                $file_link = "uploads/permits/". $filename;
                /** Creating File Record in the Database and Getting File_ID in return **/
                $create_permitfile = $model->createPermitFile([
                    'permit_id' => $permit->Permit_ID,
                    'file_class' => $file_class,
                    'file_link' => $file_link,
                    'user_id' => $_SESSION['user']['id'],
                ]);
                $complete_permit = $model->completePermit([
                    'id' => $permit_id, 
                    'issue_date' => $_POST['permit_issue_date'], 
                    'permit_file_id' => $create_permitfile
                ]);

                if($complete_permit){
                    $alert = [
                        'type' => 'success',
                        'message' => 'Permit Updated.'
                    ]; 
                }
                else{
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Error Updating Permit # <b>'.$permit_id.'</b>. Server Error, Contact Adminstrator/Developer.'
                    ];
                }
            }
            else{
                $alert = [
                    'type' => 'success',
                    'message' => 'Permit Updated.'
                ];
            }
        }
    }
?>
    <title>Permit # <?php echo $permit->Permit_ID ?> - <?php echo $title ?></title>
    <style>
    .twitter-typeahead{
        width: 100%;
    }
    .tt-menu{
        background-color: white;
        border-bottom: 1px solid grey;
        border-left: 1px solid grey;
        border-right: 1px solid grey;
        border-bottom-right-radius: 5px;
        border-bottom-left-radius: 5px;
        width: 100%;
        cursor: pointer;
    }
    .tt-selectable{
        padding: 5px 20px;
    }
    .tt-selectable:hover{
        background-color: lightgrey;
    }
    .borderless thead th{
        border: 0px;
    }
    #editPopUp .modal-dialog, #codePopUp .modal-dialog {
        width: 95%;
        height: auto;
        padding: 0;
    }

    #editPopUp .modal-content, #codePopUp .modal-content {
        height: auto;
        min-height: 100%;
        border-radius: 0;
    }
    @media (min-width: 576px) {
        #editPopUp .modal-dialog, #codePopUp .modal-dialog { max-width: none; }
    }
    #aditional_info{
        display: none;
    }
    </style>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/../inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/../inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                    <?php require(__DIR__.'/../inc/alert.php'); ?>
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header bg-gradient-primary text-white">
                                        <div class="row">
                                            <div class="col-sm-6 my-auto">
                                                <small class="text-muted">Access Name:</small>
                                                <h5 class="mt-1"><?php echo $permit->Access_Name ?></h5>
                                            </div>
                                            <div class="col-sm-6 text-right">
                                                <?php 
                                                    $editedby_user_id = $permit_status->EditedBy_User_ID;
                                                    if($editedby_user_id != NULL){
                                                        echo '<button class="btn btn-sm btn-light"><small>last edit by:</small><br> '.$model->getUserById($editedby_user_id)->name .'</button>';
                                                    }
                                                ?>
                                                <button class="btn btn-sm btn-light mr-4"><small>created by:</small><br> <?php echo $model->getUserById($permit->CreatedBy_User_ID)->name ?></button>
                                                <a class="btn btn-link edit" title="Edit" data-id="<?php echo $permit->Permit_ID ?>" data-toggle="modal" data-target="#editPopUp">
                                                    <div class="c-icon">
                                                        <i class="cil-external-link"></i>
                                                    </div>
                                                </a>
                                                <a class="btn btn-link delete" title="Delete" data-id="<?php echo $permit->Permit_ID ?>" data-toggle="modal" data-target="#deletePopUp">
                                                    <div class="c-icon">
                                                        <i class="cil-trash"></i>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row py-4">    
                                            <div class="col-12 col-sm-6">
                                                <table class="table table-sm table-borderless">
                                                    <tr>
                                                        <td><small class="text-muted">ID #:</small></td>
                                                        <td><h5><?php echo $permit->ID_Number ?></h5></td>
                                                    </tr>
                                                    <tr>
                                                        <td><small class="text-muted">ID Files:</small></td>
                                                        <td>
                                                            <ul class="list-inline">
                                                                <?php
                                                                if($fileclass != NULL){
                                                                    foreach($fileclass as $key => $class){
                                                                        $i = $key + 1;
                                                                        echo '
                                                                        <li class="list-inline-item">
                                                                            <a class="btn btn-sm btn-dark bg-gradient-dark" href="'.$GLOBALS['configurations']['dirpath'].'/'.$filelink[$key].'" target="_blank">File # '.$i.'</a>
                                                                        </li>
                                                                        ';
                                                                    }
                                                                }    
                                                                ?>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><small class="text-muted">Start Date:</small></td>
                                                        <td><h5><?php echo str_replace("-", "/", $permit_start_date) ?></h5></td>
                                                    </tr>
                                                    <tr>
                                                        <td><small class="text-muted">End Date:</small></td>
                                                        <td><h5><?php echo str_replace("-", "/", $permit_end_date) ?></h5></td>
                                                    </tr>
                                                    <tr>
                                                        <td><small class="text-muted">Permit Location:</small></td>
                                                        <td><h5><?php echo $permit->Permit_Location ?></h5></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-12 col-sm-6" style="border-left: 1px solid lightgrey">
                                                <table class="table table-sm table-borderless">
                                                    <tr>
                                                        <td><small class="text-muted">Contractor:</small></td>
                                                        <td><h5><?php echo $contractor ?></h5></td>
                                                    </tr>
                                                    <tr>
                                                        <td><small class="text-muted">Beneficiary:</small></td>
                                                        <td><h5><?php echo $beneficiary ?></h5></td>
                                                    </tr>
                                                    <tr>
                                                        <td><small class="text-muted">Request Date:</small></td>
                                                        <td><h5><?php echo str_replace("-", "/", $permit_request_date) ?></h5></td>
                                                    </tr>
                                                    <tr>
                                                        <td><small class="text-muted">Notes:</small></td>
                                                        <td><h5><?php echo $permit_status->Status_Notes ?></h5></td>
                                                    </tr>
                                                    <tr>
                                                        <td><small class="text-muted">Assignee:</small></td>
                                                        <td><h5><?php echo $assignee ?></h5></td>
                                                    </tr>
                                                </table>    
                                            </div>
                                        </div>
                                        <div class="card mt-5">
                                            <div class="card-header bg-light">
                                                <div class="row">
                                                    <div class="col-12 mt-2">
                                                        <h5>Status History</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <h5><span class="h6 text-muted mr-2">Current Status: </span><button class="btn btn-sm btn-dark bg-gradient-dark"><?php echo $current_status ?></button></h5>
                                                <table class="table table-hover table-responsive-sm content-table text-center borderless">
                                                    <thead>
                                                        <tr>
                                                            <th>Status</th>
                                                            <th>Timestamp</th>
                                                            <th>User</th>
                                                            <th>Status Notes</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            foreach($statuses as $key => $status){
                                                                echo '
                                                                <tr>
                                                                    <td>'.$status->Status.'</td>
                                                                    <td>'.$status->Status_DateTime.'</td>
                                                                    <td>'.$model->getUserById($status->CompletedBy_User_ID)->name.'</td>
                                                                    <td>'.$status->Status_Notes.'</td>
                                                                </tr>
                                                                ';
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="card mt-5">
                                            <div class="card-header bg-light">
                                                <div class="row">
                                                    <div class="col-12 mt-2">
                                                        <h5>Files & attachments</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <table class="table table-hover table-responsive-sm content-table text-center borderless">
                                                    <thead>
                                                        <tr>
                                                            <th>Class</th>
                                                            <th>Timestamp</th>
                                                            <th>User</th>
                                                            <th>Link</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        if($fileclass != NULL){
                                                            foreach($fileclass as $key => $class){
                                                                echo '
                                                                <tr>
                                                                    <td>'.$fileclass[$key].'</td>
                                                                    <td>'.$filetimestamp[$key].'</td>
                                                                    <td>'.$fileuser[$key].'</td>
                                                                    <td><a href="'.$GLOBALS['configurations']['dirpath'].'/'.$filelink[$key].'" target="_blank">View File</a></td>
                                                                </tr>
                                                                ';
                                                            }
                                                            if($current_status == "Granted"){
                                                                echo '
                                                                <tr>
                                                                    <td>'.$permit_file_details->File_Class.'</td>
                                                                    <td>'.$permit_file_details->Upload_DateTime.'</td>
                                                                    <td>'.$model->getUserById($permit_file_details->User_ID)->name.'</td>
                                                                    <td><a href="'.$GLOBALS['configurations']['dirpath'].'/'.$permit_file_details->File_Link.'" target="_blank">View File</a></td>
                                                                </tr>
                                                                ';
                                                            }
                                                        }
                                                        else{
                                                            echo '<tr class="text-center"><td colspan="7">No Files or Attachments</td></tr>';
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <!-- /.row-->
                        </div>
                    </div>
                </div>    
            </main>
        </div>
        
        <?php require_once(__DIR__."/../inc/footer.php"); ?>

    </div>

    <div id="editPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10" id="editInfo">
                                <h5>Edit Information</h5>
                                <p>Please Enter the new Information Below.</p>
                            </div>
                            <div class="col-12">
                            <div class="card">
                            <form action="<?php $_PHP_SELF ?>" method="POST" id="updatePermitForm" enctype="multipart/form-data" class="needs-validation" novalidate>
                                <div class="card-body">
                                    <div class="row justify-content-center">
                                        <div class="col-md-6 pb-4">
                                            <div class="card h-100">
                                                <div class="card-header bg-light"><strong>Permit Status Information</strong></div>
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label for="status">Status</label>
                                                        <select name="status" id="status" class="form-control">
                                                            <option value="Pending">Pending</option>
                                                            <option value="In-Process">In-Process</option>
                                                            <option value="Granted">Granted</option>
                                                            <option value="Rejected">Rejected</option>
                                                            <option value="Cancelled">Cancelled</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="notes">Notes</label>
                                                        <textarea name="notes" id="notes" class="form-control" rows="5" value="<?php echo $permit_status->Status_Notes ?>" data-sanitize="escape"></textarea>
                                                    </div>
                                                </div>
                                            </div><!--card--> 
                                        </div><!--col-->
                                        <div class="col-md-6 pb-4" id="additional_info">
                                            <div class="card h-100">
                                                <div class="card-header bg-light"><strong>Additional Information</strong></div>
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label for="permit_issue_date">Permit Issue Date</label>
                                                        <input class="form-control" id="permit_issue_date" type="date" name="permit_issue_date" placeholder="yyyy-mm-dd" data-validation="date" disabled required>
                                                    </div>
                                                    <label for="file">Permit (<a href="<?php echo $GLOBALS['configurations']['dirpath'] .'/'.$permit_file_details->File_Link ?>">Current File Here</a>)</label><br>
                                                    <small class="text-muted">Selecting a New file will remove the previous one from this permit</small>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" name="file" id="file" disabled>
                                                        <label class="custom-file-label" for="customFile">Choose New Permit File</label>
                                                    </div>
                                                </div>
                                            </div>    
                                        </div><!--col-->
                                    </div><!--row-->
                                    <div class="card">
                                        <div class="card-footer">
                                            <button class="btn btn-primary w-100" name="editPermit" id="editPermit" type="button">EDIT PERMIT</button>
                                        </div>
                                    </div>    
                                </div>
                            </form>    
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--editPopUp modal CLOSE-->

    <div id="deletePopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10">
                                <h5>Are You Sure You Want To Delete?</h5>
                                <br>
                                <button type="button" class="btn btn-danger text-white" id="delTrue">Yes</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--deletePopUp modal CLOSE-->

    
    <script>
        //VALIDATIONS OPEN
        $.validate({
            modules : 'sanitize',
        });

        (function () {
            'use strict';
            var forms = document.querySelectorAll('.needs-validation');
            Array.prototype.slice.call(forms)
            .forEach(function (form) {
                $('#editPermit').on('click', function(){
                    if (!form.checkValidity()) {
                        form.classList.add('was-validated');
                    }
                    else{
                        $('#updatePermitForm').submit();
                    }
                })
            });
        })();
        //VALIDATIONS CLOSE

        $(document).on('change', '#status', function(){
            var val = $(this).val();
            if(val == "Granted"){
                $('#additional_info').show();
                $('#permit_issue_date').removeAttr("disabled");
                $('#file').removeAttr("disabled");
            }
            else{
                $('#additional_info').hide();
                $('#permit_issue_date').attr("disabled", "disabled");
                $('#file').attr("disabled", "disabled");
            }
        });
        $(document).ready(function(){
            var dirPath = "<?php dirPath() ?>";
            var permitStatusID = "<?php echo $permit->Permit_Status_ID ?>";
            var permitID = document.querySelector('#editId').value;
        });    

        //Permit Main Details Delete Button onClick()
        $('.delete').on('click', function() {
            var id = $(this).data('id');
            $('#delTrue').click(function(){
                var request = $.ajax({
                    url: dirPath + "/dashboard/req/delete.php",
                    type: "POST",
                    data: {
                        id : id,
                        key : 'Permit_ID',
                        table : 'Permits'
                        },
                    dataType: "html"
                });

                request.done(function(msg) {
                    console.log(msg);
                    $("#deletePopUp div div div div").html("Permit Deleted.<br><small>page will redirect in 3 seconds</small>");
                    /// wait 3 seconds
                    setTimeout(function() {
                        window.location.href = dirPath + "/dashboard/permits/list"
                    }, 3000);
                });

                request.fail(function(jqXHR, textStatus) {
                    $("#deletePopUp div div div div").html("Error - Request failed: " + textStatus);
                });
            });
        });    

        //Contract Main Details Edit Button onClick()
        //Brings Up the and Poulates the Popup
        $('.edit').on('click', function(){
            var id = $(this).data('id');
            <?php if($current_status == "Granted"){ ?>
                $('#additional_info').show();
                $('#permit_issue_date').removeAttr("disabled");
                $('#file').removeAttr("disabled");
                $('#permit_issue_date').val('<?php echo $permit->Permit_Issue_Date ?>');
            <?php } else{ ?>
                $('#additional_info').hide();
                $('#permit_issue_date').attr("disabled", "disabled");
                $('#file').attr("disabled", "disabled");
            <?php } ?>
            $('#status').val('<?php echo $current_status ?>');
            $('#notes').val('<?php echo $permit_status->Status_Notes ?>');
        });
    </script>


</body>
</html>
