<?php

    require_once(__DIR__."/../../lib/functions.php");

    $model = new \Libraries\Model();
    $table = $_POST['table'];
    $id = $_POST['id'];

    if($table == "users"){
        $data = $model->getUserById($id);
    }
    elseif ($table == "Contractors") {
        $data = $model->getContractorById($id);
    }
    elseif ($table == "Assignee") {
        $data = $model->getAssigneeById($id);
    }
    elseif ($table == "Beneficiary") {
        $data = $model->getBeneficiaryById($id);
    }
    elseif ($table == "Contracts") {
        $data = $model->getContractById($id);
    }
    elseif($table == "Contract_Files"){
        $data = $model->getFileById($id);
    }
    elseif($table == "Contract_Payment_Terms"){
        $data = $model->getPaymentTermsById($id);
    }
    elseif($table == "Contract_Files"){
        $data = $model->getFileById($id);
    }
    elseif($table == "Permit_Status"){
        $data = $model->getPermitStatusByID($id);
    }
    elseif($table == "ACL"){
        $data = $model->getACLByID($id);
    }
    elseif($table == "ACLByCategoryAndParameter"){
        $data = $model->getACLByCategoryAndParameter($_POST['category'], $_POST['parameter'])->ACL_ID;
    }
    elseif ($table == "Assignee_ACL") {
        $data = $model->getAssigneeACLById($id);
    }
    elseif ($table == "BudgetGroup") {
        $data = $model->getBudgetGroupById($id);
    }

    echo json_encode($data);

?>