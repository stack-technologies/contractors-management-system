<?php

    require_once(__DIR__."/../../lib/functions.php");

    $model = new \Libraries\Model();
    $table = $_POST['table'];

    if($table == "users"){
        $update = $model->updateUser([
            'id' => $_POST['id'],
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'password' => $_POST['password']
        ]);
    }
    elseif ($table == "ACL") {
        $update = $model->updateACL([
            'id' => $_POST['id'],
            'acl_parameter' => $_POST['parameter'],
            'acl_category' => $_POST['category'],
        ]);
    }
    elseif ($table == "Assignee_ACL") {
        $update = $model->updateAssigneeACL([
            'id' => $_POST['id'],
            'acl' => $_POST['acl'],
            'acl_status' => $_POST['active'],
            'acl_notes' => $_POST['notes'],
        ]);
    }
    elseif ($table == "Assignee_ACLs") {
        $update = $model->updateAssigneeACLs([
            'id' => $_POST['id'],
            'acl' => $_POST['acl'],
        ]);
    }
    elseif ($table == "Contractors") {
        $update = $model->updateContractor([
            'id' => $_POST['id'],
            'companyName' => $_POST['companyName'],
            'notes' => $_POST['notes'],
        ]);
    }
    elseif ($table == "Assignee") {
        $update = $model->updateAssignee([
            'id' => $_POST['id'],
            'assigneeName' => $_POST['assigneeName'],
            'beneficiaryID' => $_POST['beneficiaryID'],
        ]);
    }
    elseif ($table == "Beneficiary") {
        $update = $model->updateBeneficiary([
            'id' => $_POST['id'],
            'beneficiaryName' => $_POST['beneficiaryName'],
        ]);
    }
    elseif ($table == "Contracts") {
        $update = $model->updateContract([
            'id' => $_POST['id'],
            'contractTitle' => $_POST['contractTitle'],
            'contractDesc' => $_POST['contractDesc'],
            'contractAmount' => $_POST['contractAmount'],
            'contractContractor' => $_POST['contractContractor'],
            'contractBeneficiary' => $_POST['contractBeneficiary'],
            'contractAssignee' => $_POST['contractAssignee'],
            'contractBudget' => $_POST['contractBudget'],
            'contractProgress' => $_POST['contractProgress'],
            'contractNotes' => $_POST['contractNotes'],
            'contractPlannedStartDate' => $_POST['contractPlannedStartDate'],
            'contractPlannedEndDate' => $_POST['contractPlannedEndDate'],
            'contractActualStartDate' => $_POST['contractActualStartDate'],
            'contractActualEndDate' => $_POST['contractActualEndDate'],
            'contractEditByUserID' => $_POST['contractEditByUserID'],
            'contractTags' => $_POST['contractTags'],
        ]);
    }
    elseif ($table == "Contract_Status") {
        $update = $model->updateContractStatus([
            'id' => $_POST['id'], 
            'status' => $_POST['status'],
        ]);
    }
    elseif ($table == "Contracts_Payment") {
        $update = $model->updateContractPayments([
            'id' => $_POST['id'], 
            'payments' => $_POST['payments'],
        ]);
    }
    elseif ($table == "Contract_Payment_Terms") {
        $update = $model->updatePaymentTerms([
            'id' => $_POST['id'],
            'payment_due_date' => $_POST['paymentDate'],
            'payment_amount' => $_POST['paymentAmount'],
            'payment_status' => $_POST['paymentStatus'],
        ]);
    }
    elseif ($table == "Contract_Files") {
        $update = $model->updateFile([
            'id' => $_POST['id'],
            'fileName' => $_POST['fileName'],
            'fileDesc' => $_POST['fileDesc'],
            'fileClass' => $_POST['fileClass'],
        ]);
    }
    elseif ($table == "Contracts_File") {
        $update = $model->updateContractFiles([
            'id' => $_POST['id'], 
            'files' => $_POST['files'],
        ]);
    }
    elseif ($table == "BudgetGroup") {
        $update = $model->updateBudgetGroup([
            'id' => $_POST['id'],
            'budgetgroup_title' => $_POST['budgetgroup_title'],
        ]);
    }

    echo $update;

?>