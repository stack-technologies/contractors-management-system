<?php

    require_once(__DIR__."/../../lib/functions.php");

    $model = new \Libraries\Model();

    $paymentsCompletedCurrentYear = $model->getCompletedPaymentTermsByExpiry($_POST['startdate'], $_POST['enddate']);
    $numOfpaymentsCompletedCurrentYear = count($paymentsCompletedCurrentYear);
    foreach($paymentsCompletedCurrentYear as $paymentCompletedCurrentYear){
        $amountCompletedCurrentYear += $paymentCompletedCurrentYear->Payment_Amount;
    }
    $amountCompletedCurrentYear = number_format($amountCompletedCurrentYear, 0);
    echo $numOfpaymentsCompletedCurrentYear."|". $amountCompletedCurrentYear;

?>