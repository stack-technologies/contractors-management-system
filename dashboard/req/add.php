<?php

    require_once(__DIR__."/../../lib/functions.php");

    $model = new \Libraries\Model();
    $table = $_POST['table'];

    if ($table == "Contract_Payment_Terms") {
        $create = $model->createPaymentTerms([
            'contract_id' => $_POST['id'],
            'payment_due_date' => $_POST['paymentDate'],
            'payment_amount' => $_POST['paymentAmount'],
            'payment_status' => $_POST['paymentStatus'],
        ]);
    }
    elseif ($table == "Contract_Files"){
        /** Getting Files Details **/
        define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT'] . "/contractors");
        $current_timestamp = date('d-m-Y-h-i-s', time());
        $filenum = 1;
        foreach ($_FILES['files']['name'] as $key => $name) {
            if ($_FILES['files']['error'][$key] == 4) {
                continue;
            }
            $file_name = $_POST['file_name_'.$filenum];
            $file_class = $_POST['file_class_'.$filenum];
            $file_description = $_POST['file_description_'.$filenum];
            $file_extension = end(explode(".", $name));
            $filename = $file_name . "_" . $current_timestamp . '.' . $file_extension;
            if (file_exists(ROOT_DIR."/uploads/files/" . $file_name)){
                $filename = $file_name . "_" . date('d-m-Y-h-i-s', time()) . '.' . $file_extension;
            }
            if(!is_dir(ROOT_DIR."/uploads/files/")){
                mkdir(ROOT_DIR."/uploads/files/", 0777, true);
            }
            move_uploaded_file($_FILES['files']["tmp_name"][$key], ROOT_DIR."/uploads/files/". $filename);
            $file_link = "/uploads/files/". $filename;
            $filenum++;

            /** Creating File Record in the Database and Getting File_ID in return **/
            $create_file[] = $model->createFile([
                'contract_id' => $create,
                'file_name' => $file_name,
                'file_description' => $file_description,
                'file_class' => $file_class,
                'file_link' => $file_link,
                'user_id' => $_SESSION['user']['id'],
            ]);
        }
    }

    echo $create;

?>