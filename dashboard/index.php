<?php 
    require_once(__DIR__."/inc/header.php");
    $model = new \Libraries\Model();
    $numOfContracts = $model->numOfEntries('Contracts');
    $numOfContractors = $model->numOfEntries('Contractors');
    $numOfUsers = $model->numOfEntries('users');
    $numOfPayments = $model->numOfEntries('Contract_Payment_Terms');
    $numOfAssignees = $model->numOfEntries('Assignee');

    $payments = $model->getPaymentTerms();
    $numOfDuePayments = 0;
    $numOfPendingPayments = 0;
    $numOfPaidPayments = 0;
    foreach($payments as $payment){
        if($payment->Payment_Status == "Due"){
            $amountDuePayments += $payment->Payment_Amount;
            $numOfDuePayments++;
        }
        elseif($payment->Payment_Status == "Pending"){
            $amountPendingPayments += $payment->Payment_Amount;
            $numOfPendingPayments++;
        }
        elseif($payment->Payment_Status == "Paid"){
            $amountPaidPayments += $payment->Payment_Amount;
            $numOfPaidPayments++;
        }
    }
    $totalOfPaymentsAmount = number_format($amountDuePayments + $amountPaidPayments, 0);
    $amountPendingPayments = number_format($amountPendingPayments, 0);
    $amountDuePayments = number_format($amountDuePayments, 0);
    $amountPaidPayments = number_format($amountPaidPayments, 0);

    //Details of Contracts Expiring This Month
    $date = new DateTime('now');
    $date->modify('first day of this month');
    $currentMonthStartDate = $date->format('yy-m-d');
    $date = new DateTime('now');
    $date->modify('last day of this month');
    $currentMonthEndDate = $date->format('yy-m-d');
    $currentMonthExpiryContracts = $model->getContractsByExpiry($currentMonthStartDate, $currentMonthEndDate);
    $numOfContractsExpiringCurrentMonth = count($currentMonthExpiryContracts);

    //Details of Contracts Expiring Next Month
    $date = new DateTime('now');
    $date->modify('first day of next month');
    $nextMonthStartDate = $date->format('yy-m-d');
    $date = new DateTime('now');
    $date->modify('last day of next month');
    $nextMonthEndDate = $date->format('yy-m-d');
    $nextMonthExpiryContracts = $model->getContractsByExpiry($nextMonthStartDate, $nextMonthEndDate);
    $numOfContractsExpiringNextMonth = count($nextMonthExpiryContracts);

    //Details of Payments Due in Next 30 Days
    $date = new DateTime('now');
    $currentDate = $date->format('yy-m-d');
    $date = new DateTime('now');
    $date->modify('+30 days');
    $plus30Date = $date->format('yy-m-d');
    $paymentsDueWithin30Days = $model->getDuePaymentTermsByExpiry($currentDate, $plus30Date);
    $numOfpaymentsDueWithin30Days = count($paymentsDueWithin30Days);
    foreach($paymentsDueWithin30Days as $paymentDueWithin30Days){
        $amountDueWithin30Days += $paymentDueWithin30Days->Payment_Amount;
    }
    $amountDueWithin30Days = number_format($amountDueWithin30Days, 0);

    //Details of Payments Completed This Month
    $paymentsCompletedCurrentMonth = $model->getCompletedPaymentTermsByExpiry($currentMonthStartDate, $currentMonthEndDate);
    $numOfpaymentsCompletedCurrentMonth = count($paymentsCompletedCurrentMonth);
    foreach($paymentsCompletedCurrentMonth as $paymentCompletedCurrentMonth){
        $amountCompletedCurrentMonth += $paymentCompletedCurrentMonth->Payment_Amount;
    }
    $amountCompletedCurrentMonth = number_format($amountCompletedCurrentMonth, 0);

    //Details of Payments Completed This Year
    $date = new DateTime('now');
    $date->modify('first day of january');
    $currentYearStartDate = $date->format('yy-m-d');
    $paymentsCompletedCurrentYear = $model->getCompletedPaymentTermsByExpiry($currentYearStartDate, $currentDate);
    $numOfpaymentsCompletedCurrentYear = count($paymentsCompletedCurrentYear);
    foreach($paymentsCompletedCurrentYear as $paymentCompletedCurrentYear){
        $amountCompletedCurrentYearTemp += $paymentCompletedCurrentYear->Payment_Amount;
    }
    $amountCompletedCurrentYear = number_format($amountCompletedCurrentYearTemp, 0);
    
    //Data For Payments Pie Chart
    $paymentsPendingCurrentYear = $model->getPendingPaymentTermsByExpiry($currentYearStartDate, $currentDate);
    foreach($paymentsPendingCurrentYear as $paymentPendingCurrentYear){
        $amountPendingCurrentYearTemp += $paymentPendingCurrentYear->Payment_Amount;
    }
    $amountPendingCurrentYear = number_format($amountPendingCurrentYearTemp, 0);

    $paymentsDueCurrentYear = $model->getDuePaymentTermsByExpiry($currentYearStartDate, $currentDate);
    foreach($paymentsDueCurrentYear as $paymentDueCurrentYear){
        $amountDueCurrentYearTemp += $paymentDueCurrentYear->Payment_Amount;
    }
    $amountDueCurrentYear = number_format($amountDueCurrentYearTemp, 0);

    $paymentsCancelledCurrentYear = $model->getCancelledPaymentTermsByExpiry($currentYearStartDate, $currentDate);
    foreach($paymentsCancelledCurrentYear as $paymentCancelledCurrentYear){
        $amountCancelledCurrentYearTemp += $paymentCancelledCurrentYear->Payment_Amount;
    }
    $amountCancelledCurrentYear = number_format($amountCancelledCurrentYearTemp, 0);

    //Get Permits Data
    $permits = $model->getPermits();
    $numberOfActivePermits = 0;
    $numberOfExpiredPermits = 0;
    foreach($permits as $permit){
        $permit_status = $model->getPermitStatusByID($permit->Permit_Status_ID);
        if($permit_status->Status == "Granted" && $permit->Permit_Start_Date < $currentDate && $permit->Permit_End_Date > $currentDate){
            $numberOfActivePermits++;
        }
        elseif($permit_status->Status == "Granted" && $permit->Permit_End_Date < $currentDate){
            $numberOfExpiredPermits++;
        }
    }
    $numberOfPendingPermits = $model->getNumberOfPermitsByStatus('Pending');
    $numberOfInProcessPermits = $model->getNumberOfPermitsByStatus('In-Process');
?>
    <title><?php echo $title ?></title>
    <style>
    a:hover{
        text-decoration: none;
    }
    .btn-light{
        background-color: rgba(0, 4, 54, 0.555);
        border: none;
        color: #fff;
    }
    </style>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                        <!--Quick Stats-->
                        <div class="row w-100">    
                            <div class="col-12 py-3 px-4">
                                <h4>Quick Stats</h4>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="card text-white bg-gradient-dark">
                                    <div class="card-body d-flex justify-content-between align-items-start pb-2 pt-3">
                                        <div class="row w-100">
                                            <div class="col-12">
                                                <h1><?php echo $numOfContracts ?></h1>
                                            </div>
                                            <div class="col-12">
                                                <div>Contracts</div>
                                            </div>
                                            <div class="col-6 text-right pl-3 pr-1 pt-2">
                                                <a href="<?php dirPath() ?>/dashboard/contracts/create" class="w-100 btn btn-sm btn-light">Add New</a>
                                            </div>
                                            <div class="col-6 text-right pr-3 pl-1 pt-2">
                                                <a href="<?php dirPath() ?>/dashboard/contracts/list" class="w-100 btn btn-sm btn-light">View List</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-->
                            <div class="col-sm-6 col-md-3">
                                <div class="card text-white bg-gradient-dark">
                                    <div class="card-body d-flex justify-content-between align-items-start pb-2 pt-3">
                                        <div class="row w-100">
                                            <div class="col-12">
                                                <h1><?php echo $numOfContractors ?></h1>
                                            </div>
                                            <div class="col-12">
                                                <div>Contractors</div>
                                            </div>
                                            <div class="col-6 text-right pl-3 pr-1 pt-2">
                                                <a href="<?php dirPath() ?>/dashboard/contractors/create" class="w-100 btn btn-sm btn-light">Add New</a>
                                            </div>
                                            <div class="col-6 text-right pr-3 pl-1 pt-2">
                                                <a href="<?php dirPath() ?>/dashboard/contractors/list" class="w-100 btn btn-sm btn-light">View List</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-->
                            <div class="col-sm-12 col-md-3">
                                <div class="card text-white bg-gradient-dark">
                                    <div class="card-body d-flex justify-content-between align-items-start pb-2 pt-3">
                                        <div class="row w-100">
                                            <div class="col-12">
                                                <h1><?php echo $numOfAssignees ?></h1>
                                            </div>
                                            <div class="col-12">
                                                <div>Assignees</div>
                                            </div>
                                            <div class="col-6 text-right pl-3 pr-1 pt-2">
                                                <a href="<?php dirPath() ?>/dashboard/assignee/create" class="w-100 btn btn-sm btn-light">Add New</a>
                                            </div>
                                            <div class="col-6 text-right pr-3 pl-1 pt-2">
                                                <a href="<?php dirPath() ?>/dashboard/assignee/list" class="w-100 btn btn-sm btn-light">View List</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-->
                            <div class="col-sm-12 col-md-3">
                                <div class="card text-white bg-gradient-dark">
                                    <div class="card-body d-flex justify-content-between align-items-start pb-2 pt-3">
                                        <div class="row w-100">
                                            <div class="col-12">
                                                <h1><?php echo $numOfUsers ?></h1>
                                            </div>
                                            <div class="col-12">
                                                <div>Users</div>
                                            </div>
                                            <div class="col-6 text-right pl-3 pr-1 pt-2">
                                                <a href="<?php dirPath() ?>/dashboard/users" class="w-100 btn btn-sm btn-light">Add New</a>
                                            </div>
                                            <div class="col-6 text-right pr-3 pl-1 pt-2">
                                                <a href="<?php dirPath() ?>/dashboard/users" class="w-100 btn btn-sm btn-light">View List</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-->
                        </div><!--row-->
                        <!--Contracts Info-->
                        <div class="card bg-transparent my-5">
                            <div class="card-header bg-gradient-secondary">
                                <h4 class="mt-1">Contracts Information</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">    
                                    <div class="col-md-6 col-12">
                                        <div class="row">
                                            <div class="col-12">
                                                <a href="<?php dirPath() ?>/dashboard/contracts/list?exp=1">
                                                    <div class="card text-white bg-gradient-primary">
                                                        <div class="card-body d-flex justify-content-between align-items-start py-2">
                                                            <div class="row w-100">
                                                                <div class="col-12 w-100">
                                                                    <h1 class="w-100"><?php echo $numOfContractsExpiringCurrentMonth ?></h1>
                                                                </div>
                                                                <div class="col-12 w-100">
                                                                    <div class="w-100">Contracts Expiring Current Month</div>
                                                                </div>
                                                                <div class="col-12 pt-2">
                                                                    <button class="btn btn-sm btn-light">View List</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-12">
                                                <a href="<?php dirPath() ?>/dashboard/contracts/list?exp=2">
                                                    <div class="card text-white bg-gradient-primary">
                                                        <div class="card-body d-flex justify-content-between align-items-start py-2">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <h1><?php echo $numOfContractsExpiringNextMonth ?></h1>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div>Contracts Expiring Next Month</div>
                                                                </div>
                                                                <div class="col-12 pt-2">
                                                                    <button class="btn btn-sm btn-light">View List</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a> 
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-->
                                    <div class="col-md-6 col-12">
                                        <canvas id="contracts-chart" width="100" height="40"></canvas>
                                    </div>
                                    <!-- /.col-->
                                </div><!--row-->
                                <!--Payments-->
                                <div class="row py-4 h-100">
                                    <div class="col-md-4 col-12 d-flex align-items-stretch">
                                        <div class="card text-white bg-gradient-primary w-100">
                                            <div class="card-body d-flex justify-content-between align-items-start py-2">
                                                <div class="row h-100 w-100">
                                                    <div class="col-12 d-flex align-items-stretch">
                                                        <div class="row w-100">
                                                            <div class="col-12 col-xl-6">
                                                                <h2 id="yearPayments">$<?php echo $amountCompletedCurrentYear ?></h2>  
                                                            </div>
                                                            <div class="col-12 col-xl-6 ml-auto">
                                                                <input type="date" class="form-control form-control-sm yearAmountDateSelector mb-2" name="startdate" id="startdate" value="<?php echo $currentYearStartDate ?>" placeholder="Start Date">
                                                                <input type="date" class="form-control form-control-sm yearAmountDateSelector" name="enddate" id="enddate" value="<?php echo $currentDate ?>" placeholder="End Date">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 pt-3 mt-auto">
                                                        <div class="row">
                                                            <div class="col-12 col-lg-7">
                                                                <div>Total Payments</div>
                                                                <div id="numOfYearPayments"># of Payments: <?php echo $numOfpaymentsCompletedCurrentYear ?></div>
                                                            </div>
                                                            <div class="col-12 col-lg-5 mt-auto">
                                                                <button class="w-100 btn btn-sm btn-light" id="completedPaymentsViewListBtn">View List</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-->
                                    <div class="col-md-4 col-12 d-flex align-items-stretch">
                                        <div class="card text-white bg-gradient-primary w-100">
                                            <div class="card-body d-flex justify-content-between align-items-start py-2">
                                                <div class="row h-100 w-100">
                                                    <div class="col-12">
                                                        <h2>$<?php echo $amountCompletedCurrentMonth ?></h2>
                                                    </div>
                                                    <div class="col-12 pt-3 mt-auto">
                                                        <div class="row">
                                                            <div class="col-12 col-lg-7">
                                                                <div>Current Month</div>
                                                                <div># of Payments: <?php echo $numOfpaymentsCompletedCurrentMonth ?></div>
                                                            </div>
                                                            <div class="col-12 col-lg-5 mt-auto">
                                                                <a href="<?php dirPath() ?>/dashboard/payments?startdate=<?php echo $currentMonthStartDate ?>&enddate=<?php echo $currentMonthEndDate ?>&status=paid" class="w-100 btn btn-sm btn-light">View List</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-->
                                    <div class="col-md-4 col-12 d-flex align-items-stretch">
                                        <div class="card text-white bg-gradient-primary w-100">
                                            <div class="card-body d-flex justify-content-between align-items-start py-2">
                                                <div class="row h-100 w-100">
                                                    <div class="col-12">
                                                        <h2>$<?php echo $amountDueWithin30Days ?></h2>
                                                    </div>
                                                    <div class="col-12 pt-3">
                                                        <div class="row">
                                                            <div class="col-12 col-lg-7">
                                                                <div>Amount Due Within 30 Days</div>
                                                                <div># of Payments: <?php echo $numOfpaymentsDueWithin30Days ?></div>
                                                            </div>
                                                            <div class="col-12 col-lg-5 mt-auto">
                                                                <a href="<?php dirPath() ?>/dashboard/payments?startdate=<?php echo $currentDate ?>&enddate=<?php echo $plus30Date ?>&status=due" class="w-100 btn btn-sm btn-light">View List</a>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-->
                                </div>
                                <!--Total Payments-->
                                <div class="row">
                                    <div class="col-12 py-3 px-4">
                                        <h4>Total Payments</h4>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-12 col-md-6 my-auto">
                                                <canvas id="payments-chart"></canvas>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <div class="row">
                                                    <div class="col-12 col-md-6">
                                                        <div class="card bg-light-primary">
                                                            <div class="card-body d-flex justify-content-between align-items-start py-2">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <h2>$<?php echo $totalOfPaymentsAmount ?></h2>
                                                                    </div>
                                                                    <div class="col-12 pt-3">
                                                                        <div>Total of All Payments</div>
                                                                        <div># of Payments: <?php echo $numOfPayments ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /.col-->
                                                    <div class="col-12 col-md-6">
                                                        <div class="card bg-light-primary">
                                                            <div class="card-body d-flex justify-content-between align-items-start py-2">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <h2>$<?php echo $amountPendingPayments ?></h2>
                                                                    </div>
                                                                    <div class="col-12 pt-3">
                                                                        <div>Total of Pending Payments</div>
                                                                        <div># of Payments: <?php echo $numOfPendingPayments ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /.col-->
                                                    <div class="col-12 col-md-6">
                                                        <div class="card bg-light-primary">
                                                            <div class="card-body d-flex justify-content-between align-items-start py-2">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <h2>$<?php echo $amountDuePayments ?></h2>
                                                                    </div>
                                                                    <div class="col-12 pt-3">
                                                                        <div>Total of Due Payments</div>
                                                                        <div># of Payments: <?php echo $numOfDuePayments ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /.col-->
                                                    <div class="col-12 col-md-6">
                                                        <div class="card bg-light-primary">
                                                            <div class="card-body d-flex justify-content-between align-items-start py-2">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <h2>$<?php echo $amountPaidPayments ?></h2>
                                                                    </div>
                                                                    <div class="col-12 pt-3">
                                                                        <div>Total of Paid Payments</div>
                                                                        <div># of Payments: <?php echo $numOfPaidPayments ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /.col-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--row-->
                            </div>
                        </div>
                        <!--Permits-->
                        <div class="card bg-transparent">
                            <div class="card-header bg-gradient-secondary">
                                <h4 class="mt-1">Permits</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-12 col-md-3 col-sm-6">
                                                <div class="card bg-secondary">
                                                    <div class="card-body d-flex justify-content-between align-items-start py-2">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <h2><?php echo $numberOfPendingPermits ?></h2>
                                                            </div>
                                                            <div class="col-12 pt-3">
                                                                <div>Total Pending</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.col-->
                                            <div class="col-12 col-md-3 col-sm-6">
                                                <div class="card bg-secondary">
                                                    <div class="card-body d-flex justify-content-between align-items-start py-2">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <h2><?php echo $numberOfInProcessPermits ?></h2>
                                                            </div>
                                                            <div class="col-12 pt-3">
                                                                <div>Total In-Process</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.col-->
                                            <div class="col-12 col-md-3 col-sm-6">
                                                <div class="card bg-secondary">
                                                    <div class="card-body d-flex justify-content-between align-items-start py-2">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <h2><?php echo $numberOfActivePermits ?></h2>
                                                            </div>
                                                            <div class="col-12 pt-3">
                                                                <div>Total Currently Active</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.col-->
                                            <div class="col-12 col-md-3 col-sm-6">
                                                <div class="card bg-secondary">
                                                    <div class="card-body d-flex justify-content-between align-items-start py-2">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <h2><?php echo $numberOfExpiredPermits ?></h2>
                                                            </div>
                                                            <div class="col-12 pt-3">
                                                                <div>Total Expired</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.col-->
                                        </div>
                                    </div>
                                </div><!--row-->
                            </div>
                        </div>        
                    </div>
                </div>
            </main>
        </div>
        
        <?php require_once(__DIR__."/inc/footer.php"); ?>
    </div>

<script>
$(document).ready(function(){

    new Chart(document.getElementById("contracts-chart"), {
        type: 'pie',
        data: {
            labels: ["Under Review", "Approved", "In-Progress", "On Hold", "Completed", "Cancelled"],
            datasets: [{
                label: "Contracts (in #)",
                backgroundColor: ["#70a8ff", "#90b86f", "#9f9f9f", "#fecf41", "#dbdbdb", "#eb5f50"], 
                data: [
                    <?php echo $model->getNumOfContractsByStatus('Under Review') ?>,
                    <?php echo $model->getNumOfContractsByStatus('Approved') ?>, 
                    <?php echo $model->getNumOfContractsByStatus('In-Progress') ?>, 
                    <?php echo $model->getNumOfContractsByStatus('On Hold') ?>, 
                    <?php echo $model->getNumOfContractsByStatus('Completed') ?>, 
                    <?php echo $model->getNumOfContractsByStatus('Cancelled') ?>,
                ]
            }]
        },
        options: {
            title: {
                display: true,
                padding: 20, 
                text: 'Number of Contracts by Status'
            }, 
            responsive: true, 
            legend: {
                position: 'right', 
                align: 'center', 
                labels: {
                    boxWidth: 10, 
                    fontSize: 8, 
                    padding: 5, 
                },
            },
            elements: {
                arc: {
                    borderWidth: 0
                }
            }
        }
    });

    new Chart(document.getElementById("payments-chart"), {
        type: 'pie',
        data: {
            labels: ["Pending", "Due", "Paid", "Cancelled"],
            datasets: [{
                label: "Contracts (in #)",
                backgroundColor: ["#dbdbdb","#70a8ff","#90b86f","#eb5f50"],
                data: [
                    <?php echo $amountPendingCurrentYearTemp ?>, 
                    <?php echo $amountDueCurrentYearTemp ?>, 
                    <?php echo $amountCompletedCurrentYearTemp ?>, 
                    <?php echo $amountCancelledCurrentYearTemp ?>, 
                ]
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Total of Payments From 1st Jan to Today', 
                padding: 5, 
            }, 
            responsive: true, 
            legend: {
                position: 'top', 
                labels: {
                    boxWidth: 10, 
                    fontSize: 8, 
                    padding: 5, 
                },
            },
            elements: {
                arc: {
                    borderWidth: 0
                }
            }, 
            tooltips: {
                mode: 'label',
                callbacks: {
                    label: function(tooltipItem, data) { 
                        var indice = tooltipItem.index;
                        return data.labels[indice] + ": $" + Number(data.datasets[0].data[indice]).toFixed(0).replace(/./g, function(c, i, a) {
                            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                        });
                    }
                }
            }, 
        }
    });

    var dirPath = "<?php dirPath() ?>";
    var startdate = $('#startdate').val();
    var enddate = $('#enddate').val();
    $('.yearAmountDateSelector').change(function(){
        startdate = $('#startdate').val();
        enddate = $('#enddate').val();

        var request = $.ajax({
            url: dirPath + "/dashboard/req/payments.php",
            type: "POST",
            data: {
                startdate : startdate,
                enddate : enddate,
                },
            dataType: "html"    
        });

        request.done(function(msg) {
            var result = msg.split('|');
            $('#yearPayments').html('$'+result[1]);
            $('#numOfYearPayments').html('# of Payments: '+result[0]);
        });

        request.fail(function(jqXHR, textStatus) {
            $('#yearPayments').html("Error");
        });
    });

    $('#completedPaymentsViewListBtn').click(function(){
        startdate = $('#startdate').val();
        enddate = $('#enddate').val();

        window.location.href = dirPath + "/dashboard/payments?status=paid&startdate=" + startdate + "&enddate=" + enddate;
    });
});
</script>

</body>
</html>