<?php 
    require_once(__DIR__."/../inc/header.php"); 
    $model = new \Libraries\Model();

    if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['create'])){
        $create = $model->createContractor([
            'companyName' => $_POST['company-name'],
            'notes' => $_POST['notes'],
        ]);
        if($create){
            $alert = [
                'type' => 'success',
                'message' => 'Contractor Created <b>'.$_POST['company-name'].'</b>'
            ];
        }
        else{
            $alert = [
                'type' => 'danger',
                'message' => 'Error Creating Contractor <b>'.$_POST['company-name'].'</b>. Server Error, Contact Adminstrator/Developer.'
            ];
        }
    }
?>
    <title>Create a New Contractor - <?php echo $title ?></title>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/../inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/../inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                    <?php require(__DIR__.'/../inc/alert.php'); ?>
                        <div class="row justify-content-center">
                            <div class="col-sm-6 col-md-8 col-lg-10">
                                <form action="<?php $_PHP_SELF ?>" method="POST">
                                    <div class="card">
                                        <div class="card-header text-center"><strong>Add a New Contractor</strong></div>
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="company-name">Company Name</label>
                                                <input class="form-control" id="company-name" type="text" name="company-name" data-sanitize="escape" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="notes">Notes</label>
                                                <textarea name="notes" id="notes" class="form-control" rows="5" data-sanitize="escape" required></textarea>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <button class="btn btn-sm btn-dark w-100" name="create" type="submit">Add</button>
                                        </div>
                                    </div>
                                </form>
                                <!-- /.col-->
                            </div>
                            <!-- /.row-->
                        </div>
                    </div>
                </div>    
            </main>
        </div>
        
        <?php require_once(__DIR__."/../inc/footer.php"); ?>
        <script>
            $.validate({
                modules : 'sanitize'
            });
        </script>
    </div>
</body>
</html>