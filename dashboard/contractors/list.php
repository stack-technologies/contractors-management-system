<?php 
    require_once(__DIR__."/../inc/header.php"); 
    $model = new \Libraries\Model();
    $contractors = $model->getContractors();
?>
    <title>List of Contractors - <?php echo $title ?></title>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/../inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/../inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                    <?php require(__DIR__.'/../inc/alert.php'); ?>
                        <div class="row justify-content-center">
                            <div class="col-12 content-header pb-3">
                                <div class="row px-3">
                                    <div class="col-6">
                                        <h2>List of Contractors</h2>
                                    </div>
                                    <div class="col-6 text-right">
                                        <a href="<?php dirpath() ?>/dashboard/contractors/create" class="btn btn-primary bg-gradient-primary text-right">Add New Record</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <table class="table table-hover table-responsive-sm table-striped content-table text-center">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Company Name</th>
                                            <th># of Contracts</th>
                                            <th>Notes</th>
                                            <th>Timestamp</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach($contractors as $contractor){
                                            $numberOfContracts = $model->getNumberOfContractsPerContractor($contractor->Contractor_ID);
                                            echo '
                                            <tr>
                                                <td>'.$contractor->Contractor_ID.'</td>
                                                <td>'.$contractor->Company_Name.'</td>
                                                <td><a href="'.$GLOBALS['configurations']['dirpath'].'/dashboard/contracts/list?contractor='.$contractor->Contractor_ID.'">'.$numberOfContracts.'</a></td>
                                                <td>'.$contractor->Notes.'</td>
                                                <td>'.$contractor->DateTime.'</td>
                                                <td>
                                                    <a class="btn btn-sm btn-link edit" data-id="'.$contractor->Contractor_ID.'" data-toggle="modal" data-target="#editPopUp">
                                                        <div class="c-icon">
                                                            <i class="cil-pencil"></i>
                                                        </div>
                                                    </a>
                                                    <a class="btn btn-sm btn-link delete" data-id="'.$contractor->Contractor_ID.'" data-toggle="modal" data-target="#deletePopUp">
                                                        <div class="c-icon">
                                                            <i class="cil-trash"></i>
                                                        </div>
                                                    </a>
                                                </td>
                                            </tr>
                                            ';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.row-->
                        </div>
                    </div>
                </div>    
            </main>
        </div>
        
        <?php require_once(__DIR__."/../inc/footer.php"); ?>

    </div>

    <div id="editPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10" id="editInfo">
                                <h5>Edit Information</h5>
                                <p>Please Enter the new Information Below.</p>
                            </div>
                            <div class="col-12">
                                <div class="content-form mx-auto">
                                    <input type="number" name="editId" id="editId" required hidden>
                                    <div class="form-group">
                                        <label for="editCompanyName">Company Name</label>
                                        <input type="text" name="editCompanyName" id="editCompanyName" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="editNotes">Notes</label>
                                        <textarea name="editNotes" id="editNotes" class="form-control" required></textarea>
                                    </div>
                                    <button name="editContractor" id="editContractor" class="btn btn-primary">EDIT CONTRACTOR</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--editPopUp modal CLOSE-->

    <div id="deletePopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10">
                                <h5>Are You Sure You Want To Delete?</h5>
                                <br>
                                <button type="button" class="btn btn-danger text-white" id="delTrue">Yes</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--deletePopUp modal CLOSE-->
    
    <script>
        $(document).ready(function(){
            var dirPath = "<?php dirPath() ?>";

            $('.delete').on('click', function() {
                var id = $(this).data('id');
                $('#delTrue').click(function(){
                    var request = $.ajax({
                        url: dirPath + "/dashboard/req/delete.php",
                        type: "POST",
                        data: {
                            id : id,
                            key : 'Contractor_ID',
                            table : 'Contractors'
                            },
                        dataType: "html"
                    });

                    request.done(function(msg) {
                        console.log(msg);
                        $("#deletePopUp div div div div").html("Contractor Deleted.<br><small>page will reload in 3 seconds</small>");
                        /// wait 3 seconds
                        setTimeout(function() {
                            window.location.reload(false)
                        }, 3000);
                    });

                    request.fail(function(jqXHR, textStatus) {
                        $("#deletePopUp div div div div").html("Error - Request failed: " + textStatus);
                    });
                });
            });

            $('.edit').on('click', function(){
                var id = $(this).data('id');
                var request = $.ajax({
                    url: dirPath + "/dashboard/req/edit.php",
                    type: "POST",
                    data: {
                        id : id,
                        table : 'Contractors'
                        },
                    dataType: "html"
                });

                request.done(function(msg) {
                    var obj = JSON.parse(msg);
                    document.getElementById('editId').value = obj.Contractor_ID;
                    document.getElementById('editCompanyName').value = obj.Company_Name;
                    document.getElementById('editNotes').value = obj.Notes;
                });

                request.fail(function(jqXHR, textStatus) {
                    $("#editPopUp div div div div").html("Error - Request failed: " + textStatus);
                });
            });
            $('#editContractor').on('click', function(){
                var id = document.querySelector('#editId').value;
                var companyName = document.querySelector('#editCompanyName').value;
                var notes = document.querySelector('#editNotes').value;
                
                if(companyName != "" || notes != ""){
                    var request = $.ajax({
                        url: dirPath + "/dashboard/req/update.php",
                        type: "POST",
                        data: {
                            id : id,
                            companyName : companyName,
                            notes : notes,
                            table : 'Contractors'
                            },
                        dataType: "html"    
                    });

                    request.done(function(msg) {
                        $("#editPopUp div div div div").html("Contractor Updated.<br><small>page will reload in 3 seconds</small>");
                        /// wait 3 seconds
                        setTimeout(function() {
                            window.location.reload(false)
                        }, 3000);
                    });

                    request.fail(function(jqXHR, textStatus) {
                        $("#editPopUp div div div div").html("Error - Request failed: " + textStatus);
                    });
                }
                else{
                    $("#editPopUp div div div div").html("Error - Textfield Empty");
                    /// wait 3 seconds
                    setTimeout(function() {
                        window.location.reload(false)
                    }, 2000);
                }
            });
        });
    </script>

</body>
</html>