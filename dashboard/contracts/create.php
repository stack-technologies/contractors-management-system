<?php 
    require_once(__DIR__."/../inc/header.php"); 
    $model = new \Libraries\Model();
    $contractors = $model->getContractors();
    $beneficiaries = $model->getBeneficiary();
    $assignees = $model->getAssignees();
    $budgetgroups = $model->getBudgetGroup();

    foreach($beneficiaries as $beneficiary){
        $list_of_beneficiaries[] = $beneficiary->Beneficiary_Name;
    }
    foreach($assignees as $assignee){
        $list_of_assignees[] = $assignee->Assignee_Name;
    }

    if($_SERVER['REQUEST_METHOD'] == "POST"){
        $create = $model->createContract([
            'contract_title' => $_POST['title'],
            'contract_description' => $_POST['description'],
            'contract_total_amount' => $_POST['total_amount'],
            'contractor_id' => $_POST['contractor'],
            'beneficiary' => $_POST['beneficiary'],
            'assignee' => $_POST['assignee'],
            'budget_group_id' => $_POST['budget'],
            'planned_start_date' => $_POST['planned_start_date'],
            'expected_end_date' => $_POST['planned_end_date'],
            'actual_start_date' => $_POST['actual_start_date'],
            'actual_end_date' => $_POST['actual_end_date'],
            'progress' => $_POST['progress'],
            'notes' => $_POST['notes'],
            'createdby_user_id' => $_SESSION['user']['id'],
            'tags' => $_POST['tags'],
        ]);
        if($create != 'FALSE'){
            /** Creating Status and Getting Status_ID in return **/
            $create_status = $model->createStatus([
                'contract_id' => $create,
                'status' => $_POST['status'],
            ]);

            /** Creating Payment Details and Getting Payment_ID in return **/
            for($i = 1; $i <= $_POST['num_of_payments']; $i++){
                $create_payment[$i] = $model->createPaymentTerms([
                    'contract_id' => $create,
                    'payment_due_date' => $_POST['payment_due_date_'.$i],
                    'payment_amount' => $_POST['payment_amount_'.$i],
                    'payment_status' => $_POST['payment_status_'.$i],
                ]);
            }

            /** Getting Files Details **/
            define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/contractors');
            $filenum = 1;
            foreach ($_FILES['files']['name'] as $key => $name) {
                $current_timestamp = date('d-m-Y-h-i-s', time());
                if ($_FILES['files']['error'][$key] == 4) {
                    continue;
                }
                $file_name = $_POST['file_name_'.$filenum];
                $file_class = $_POST['file_class_'.$filenum];
                $file_description = $_POST['file_description_'.$filenum];
                $file_extension = end(explode(".", $name));
                $filename = $file_name . "_" . $current_timestamp . '.' . $file_extension;
                if (file_exists(ROOT_DIR."/uploads/files/" . $filename)){
                    $filename = $file_name . "_" . date('d-m-Y-h-i-s', time()) . '.' . $file_extension;
                }
                if(!is_dir(ROOT_DIR."/uploads/files/")){
                    mkdir(ROOT_DIR."/uploads/files/", 0777, true);
                }
                move_uploaded_file($_FILES['files']["tmp_name"][$key], ROOT_DIR."/uploads/files/". $filename);
                $file_link = "uploads/files/". $filename;
                $filenum++;

                /** Creating File Record in the Database and Getting File_ID in return **/
                $create_file[] = $model->createFile([
                    'contract_id' => $create,
                    'file_name' => $file_name,
                    'file_description' => $file_description,
                    'file_class' => $file_class,
                    'file_link' => $file_link,
                    'user_id' => $_SESSION['user']['id'],
                ]);
            }
            $file_ids = implode(",",$create_file);
            $payment_ids = implode(",", $create_payment);

            /** Updating the Contract Details with Payment, Status and Files **/
            if($create_status != 'FALSE' || $create_payment != 'FALSE'){
                $update_status = $model->updateContractAfterCreation([
                    'contract_id' => $create,
                    'contract_status_id' => $create_status,
                    'payment_terms' => $payment_ids,
                    'files' => $file_ids,
                ]);
                if($update_status){
                    $alert = [
                        'type' => 'success',
                        'message' => 'Contract Created <b>'.$_POST['title'].'</b>'
                    ];
                }
            }
        }
        else{
            $alert = [
                'type' => 'danger',
                'message' => 'Error Creating Contract <b>'.$_POST['title'].'</b>. Server Error, Contact Adminstrator/Developer.'
            ];
        }
    }
?>
    <title>Create Contracts - <?php echo $title ?></title>
    <link rel="stylesheet" href="<?php dirPath() ?>/assets/plugins/bs-tags/bootstrap-taginput.css">
    <style>
        .twitter-typeahead{
            width: 100%;
        }
        .tt-menu{
            background-color: white;
            border-bottom: 1px solid grey;
            border-left: 1px solid grey;
            border-right: 1px solid grey;
            border-bottom-right-radius: 5px;
            border-bottom-left-radius: 5px;
            width: 100%;
            cursor: pointer;
        }
        .tt-selectable{
            padding: 5px 20px;
        }
        .tt-selectable:hover{
            background-color: lightgrey;
        }
    </style>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/../inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/../inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                    <?php require(__DIR__.'/../inc/alert.php'); ?>
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <form action="<?php $_PHP_SELF ?>" method="POST" id="createContractForm" enctype="multipart/form-data" class="needs-validation" novalidate>
                                    <h3 class="text-center p-3 text-uppercase font-weight-bold">Add a New Contract</h3>
                                    <div class="row">
                                        <div class="col-md-6 pb-4">
                                            <div class="card h-100">
                                                <div class="card-header bg-light"><strong>Contract Information</strong></div>
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label for="title">Title</label>
                                                        <input class="form-control" id="title" type="text" name="title" data-sanitize="escape" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="description">Description</label>
                                                        <textarea name="description" id="description" class="form-control" rows="5" data-sanitize="escape" required></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="total_amount">Total Amount</label>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <div class="c-icon">
                                                                        <i class="cil-dollar"></i>
                                                                    </div>
                                                                </span>
                                                            </div>
                                                            <input class="form-control" id="total_amount" type="number" name="total_amount" data-validation="number" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="contractor">Contractor</label>
                                                        <select name="contractor" id="contractor" class="form-control">
                                                        <?php
                                                        foreach($contractors as $contractor){
                                                            echo '<option value="'.$contractor->Contractor_ID.'">'.$contractor->Company_Name.'</option>';
                                                        }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="beneficiary">Beneficiary</label><br>
                                                        <input class="form-control" id="beneficiary" type="text" name="beneficiary" data-sanitize="escape" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="assignee">Assignee</label><br>
                                                        <input class="form-control" id="assignee" type="text" name="assignee" data-sanitize="escape" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="budget">Budget</label>
                                                        <select name="budget" id="budget" class="form-control">
                                                            <?php
                                                            foreach($budgetgroups as $budgetgroup){
                                                                echo '<option value="'.$budgetgroup->Budget_Group_ID.'">'.$budgetgroup->Budget_Group_Title.'</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="progress">Progress <input class="btn btn-sm btn-primary" id="progress-display" value="0%"></label>
                                                        <input type="range" class="custom-range" name="progress" value="0" min="0" max="100" step="1" id="progress" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="status">Status</label>
                                                        <select name="status" id="status" class="form-control">
                                                            <option value="Under Review">Under Review</option>
                                                            <option value="Approved">Approved</option>
                                                            <option value="In-Progress">In-Progress</option>
                                                            <option value="On Hold">On Hold</option>
                                                            <option value="Completed">Completed</option>
                                                            <option value="Cancelled">Cancelled</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div><!--card--> 
                                        </div><!--col-->
                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="card-header bg-light"><strong>Contract Payments</strong></div>
                                                <div class="card-body">
                                                    <input type="text" name="num_of_payments" id="num_of_payments" value="1" hidden>
                                                    <table class="table table-responsive-sm">
                                                        <thead>
                                                            <tr>
                                                                <th>Due Date</th>
                                                                <th>Amount</th>
                                                                <th>Status</th>
                                                                <th>
                                                                    <button type="button" id="add_payment_btn" class="btn btn-sm btn-secondary mt-1">
                                                                        <div class="c-icon">
                                                                            <i class="cil-library-add"></i>
                                                                        </div>
                                                                    </button>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="payment_rows">
                                                            <!--row-->
                                                            <tr>
                                                                <td><input class="form-control" id="payment_due_date_1" type="date" name="payment_due_date_1" placeholder="yyyy-mm-dd" data-validation="date" required></td>
                                                                <td><input class="form-control" id="payment_amount_1" type="text" name="payment_amount_1"  data-validation="number" required></td>
                                                                <td>
                                                                    <select name="payment_status_1" id="payment_status_1" class="form-control">
                                                                        <option value="Pending">Pending</option>
                                                                        <option value="Paid">Paid</option>
                                                                        <option value="Due">Due</option>
                                                                        <option value="Cancelled">Cancelled</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <button type="button" data-id="1" name="payment_remove_1" id="payment_remove_1" class="payment-remove btn btn-sm btn-danger mt-1">
                                                                        <div class="c-icon">
                                                                            <i class="cil-trash"></i>
                                                                        </div>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>    
                                            </div>
                                            <div class="card">
                                                <div class="card-header bg-light"><strong>Dates</strong></div>
                                                <div class="card-body"> 
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <label for="planned_start_date">Planned Start Date</label>
                                                                <input class="form-control" id="planned_start_date" type="date" name="planned_start_date" placeholder="yyyy-mm-dd" data-validation="date" required>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label for="planned_end_date">Planned End Date</label>
                                                                <input class="form-control" id="planned_end_date" type="date" name="planned_end_date" placeholder="yyyy-mm-dd" data-validation="date" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <label for="actual_start_date">Actual Start Date</label>
                                                                <input class="form-control" id="actual_start_date" type="date" name="actual_start_date" placeholder="yyyy-mm-dd" data-validation="date" required>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label for="actual_end_date">Actual End Date</label>
                                                                <input class="form-control" id="actual_end_date" type="date" name="actual_end_date" placeholder="yyyy-mm-dd" data-validation="date" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header bg-light"><strong>Extra Information</strong></div>
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label for="tags">Tags</label>
                                                        <input class="form-control" type="text" id="tags" name="tags" data-sanitize="escape" data-role="tagsinput" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="notes">Notes</label>
                                                        <textarea name="notes" id="notes" class="form-control" rows="5" data-sanitize="escape" required></textarea>
                                                    </div>
                                                </div>
                                            </div>        
                                        </div><!--col-->
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-header bg-light"><strong>Files</strong> & Attachments</div>
                                                <div class="card-body">        
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" name="files[]" id="files" multiple>
                                                        <label class="custom-file-label" for="customFile">Choose Multiple files</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row px-3" id="files_details"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--row-->
                                    <div class="card">
                                        <div class="card-footer">
                                            <button class="btn btn-primary w-100" name="create" id="create" type="button">CREATE</button>
                                        </div>
                                    </div>    
                                </form>
                                <!-- /.col-->
                            </div>
                            <!-- /.row-->
                        </div>
                    </div>
                </div>    
            </main>
        </div>

        <?php require_once(__DIR__."/../inc/footer.php"); ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
        <script type="text/javascript">
            //VALIDATIONS OPEN
            $.validate({
                modules : 'sanitize',
            });

            (function () {
                'use strict';
                var forms = document.querySelectorAll('.needs-validation');
                Array.prototype.slice.call(forms)
                .forEach(function (form) {
                    $('#create').on('click', function(){
                        if (!form.checkValidity()) {
                            form.classList.add('was-validated');
                        }
                        else{
                            $('#createContractForm').submit();
                        }
                    })
                });
            })();
            //VALIDATIONS CLOSE

            $(document).on('click', '.payment-remove', function(){
                var dirPath = "<?php dirPath() ?>";
                var payment_row_num = $(this).data('id');
                var current_payments = $("#payment_rows").html().split('<!--row-->');
                var num_of_payments = current_payments.length - 1;
                var payment_due_dates = [""];
                var payment_amounts = [""];
                var payment_status = [""];

                for(var i = 1; i <= num_of_payments; i++){
                    payment_due_dates.push($('#payment_due_date_'+i).val());
                    payment_amounts.push($('#payment_amount_'+i).val());
                    payment_status.push($('#payment_status_'+i).val());
                }

                current_payments.splice(payment_row_num, 1);
                payment_due_dates.splice(payment_row_num, 1);
                payment_amounts.splice(payment_row_num, 1);
                payment_status.splice(payment_row_num, 1);
                var final_payments = '';
                for(var i = 1; i < current_payments.length; i++){
                    final_payments += '<!--row-->'
                        +'<tr>'
                            +'<td><input class="form-control" id="payment_due_date_'+i+'" type="date" name="payment_due_date_'+i+'" placeholder="yyyy-mm-dd" value="'+payment_due_dates[i]+'" required></td>'
                            +'<td><input class="form-control" id="payment_amount_'+i+'" type="text" name="payment_amount_'+i+'" value="'+payment_amounts[i]+'" required></td>'
                            +'<td>'
                                +'<select name="payment_status_'+i+'" id="payment_status_'+i+'" value="'+payment_status[i]+'" class="form-control">'
                                    +'<option value="Pending">Pending</option>'
                                    +'<option value="Paid">Paid</option>'
                                    +'<option value="Due">Due</option>'
                                    +'<option value="Cancelled">Cancelled</option>'
                                +'</select>'
                            +'</td>'
                            +'<td>'
                                +'<button type="button" data-id="'+i+'" name="payment_remove_'+i+'" id="payment_remove_'+i+'" class="payment-remove btn btn-sm btn-danger mt-1">'
                                    +'<div class="c-icon">'
                                        +'<i class="cil-trash"></i>'
                                    +'</div>'
                                +'</button>'
                            +'</td>'
                        +'</tr>';
                }
                $('#payment_rows').html(final_payments);
                for(var i = 1; i < current_payments.length; i++){
                    $('#payment_status_'+i).val(payment_status[i]);
                }
            });
            $(document).ready(function(){
                // Defining the local dataset for Typeahead
                <?php
                function js_str($s){
                    return '"' . addcslashes($s, "\0..\37\"\\") . '"';
                }
                
                function js_array($array){
                    $temp = array_map('js_str', $array);
                    return '[' . implode(',', $temp) . ']';
                }
                
                echo 'var beneficiaries = ', js_array($list_of_beneficiaries), ';';
                echo 'var assignees = ', js_array($list_of_assignees), ';';
                ?>

                //Beneficiaries for TypeAhead
                var beneficiaries = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.whitespace,
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    local: beneficiaries
                });
                $('#beneficiary').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                },
                {
                    name: 'beneficiaries',
                    source: beneficiaries
                });

                //Assignees for TypeAhead
                var assignees = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.whitespace,
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    local: assignees
                });
                $('#assignee').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1 
                },
                {
                    name: 'assignees',
                    source: assignees
                });

                $('#sidebar').addClass('c-sidebar-minimized');
                var dirPath = "<?php dirPath() ?>";
                $('#progress').change(function(){
                    $('#progress-display').val($(this).val()+"%");
                });
                $('#progress-display').keyup(function(){
                    var val = $(this).val();
                    if(val > 100){
                        val = 100;
                        $('#progress-display').val(val);
                    }
                    else if(val < 0 || isNaN(val)){
                        val = 0;
                        $('#progress-display').val(val);
                    }
                    $('#progress').val(val);
                });
                $('#add_payment_btn').click(function(){
                    var current_payments = $("#payment_rows").html();
                    var num_of_payments = current_payments.split('<!--row-->').length;
                    var payment_rows = '<!--row-->'
                        +'<tr>'
                            +'<td><input class="form-control" id="payment_due_date_'+num_of_payments+'" type="date" name="payment_due_date_'+num_of_payments+'" placeholder="yyyy-mm-dd" required></td>'
                            +'<td><input class="form-control" id="payment_amount_'+num_of_payments+'" type="text" name="payment_amount_'+num_of_payments+'" required></td>'
                            +'<td>'
                                +'<select name="payment_status_'+num_of_payments+'" id="payment_status_'+num_of_payments+'" class="form-control">'
                                    +'<option value="Pending">Pending</option>'
                                    +'<option value="Paid">Paid</option>'
                                    +'<option value="Due">Due</option>'
                                    +'<option value="Cancelled">Cancelled</option>'
                                +'</select>'
                            +'</td>'
                            +'<td>'
                                +'<button type="button" data-id="'+num_of_payments+'" name="payment_remove_'+num_of_payments+'" id="payment_remove_'+num_of_payments+'" class="payment-remove btn btn-sm btn-danger mt-1">'
                                    +'<div class="c-icon">'
                                        +'<i class="cil-trash"></i>'
                                    +'</div>'
                                +'</button>'
                            +'</td>'
                        +'</tr>';
                    $('#payment_rows').append(payment_rows);
                    $('#num_of_payments').val(num_of_payments);
                });
                $('#files').change(function(){
                    var files = $(this)[0].files;
                    console.log(files);
                    var file_details = '';
                    var filenum = 1;
                    for(var i = 0; i < files.length; i++){
                        file_details += 
                        '<div class="col-md-6 col-12 p-3">'
                            +'<div class="card bg-light">'
                                +'<div class="card-header"><h4>File # ' + filenum + '</h4></div>'
                                +'<div class="card-body">'
                                    +'<div class="row">'
                                        +'<div class="col-sm-6">'
                                            +'<label for="file_name_'+filenum+'">File Name</label>'
                                            +'<input class="form-control" id="file_name_'+filenum+'" type="text" name="file_name_'+filenum+'" required>'
                                        +'</div>'
                                        +'<div class="col-sm-6">'
                                            +'<label for="file_class_'+filenum+'">File Class</label>'
                                            +'<select name="file_class_'+filenum+'" id="file_class_'+filenum+'" class="form-control">'
                                                +'<option value="Contract">Contract</option>'
                                                +'<option value="Attachment">Attachment</option>'
                                                +'<option value="Invoice">Invoice</option>'
                                                +'<option value="Others">Others</option>'
                                            +'</select>'
                                        +'</div>'
                                        +'<div class="col-12">'
                                            +'<label for="file_description_'+filenum+'">File Description</label>'
                                            +'<textarea class="form-control" id="file_description_'+filenum+'" name="file_description_'+filenum+'" required></textarea>'
                                        +'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</div>'
                        +'</div>';
                        filenum++;                
                    }
                    $('#files_details').html(file_details);
                });
            });
        </script>
        <script src="<?php dirPath() ?>/assets/plugins/bs-tags/bootstrap-tagsinput.js"></script>
    </div>
</body>
</html>