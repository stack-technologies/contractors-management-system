<?php 
    require_once(__DIR__."/../inc/header.php"); 
    $model = new \Libraries\Model();
    $contractors = $model->getContractors();
    $budgetgroups = $model->getBudgetGroup();

    $getContractor = $_REQUEST['contractor'];
    $getBudgetGroup = $_REQUEST['budget'];
    $expiry = $_REQUEST['exp'];
    if($expiry == NULL){
        if($getContractor != NULL && $getBudgetGroup == NULL){
            $contracts = $model->getContractsByContractor($getContractor);
        }
        else if($getContractor == NULL && $getBudgetGroup != NULL){
            $contracts = $model->getContractsByBudgetGroup($getBudgetGroup);
        }
        else if($getContractor != NULL && $getBudgetGroup != NULL){
            $contracts = $model->getContractsByContractorAndBudget($getContractor, $getBudgetGroup);
        }
        else{
            $contracts = $model->getContracts();
        }
    }
    elseif($expiry == "1"){
        $semiTitle = " - Expiring Current Month";
        $date = new DateTime('now');
        $date->modify('first day of this month');
        $currentMonthStartDate = $date->format('yy-m-d');
        $date = new DateTime('now');
        $date->modify('last day of this month');
        $currentMonthEndDate = $date->format('yy-m-d');
        $contracts = $model->getContractsByExpiry($currentMonthStartDate, $currentMonthEndDate);
    }
    elseif($expiry == "2"){
        $semiTitle = " - Expiring in Next Month";
        $date = new DateTime('now');
        $date->modify('first day of next month');
        $nextMonthStartDate = $date->format('yy-m-d');
        $date = new DateTime('now');
        $date->modify('last day of second month');
        $secondMonthEndDate = $date->format('yy-m-d');
        $contracts = $model->getContractsByExpiry($nextMonthStartDate, $secondMonthEndDate);
    }
?>
    <title>List of Contracts <?php echo $semiTitle ?> - <?php echo $title ?></title>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/../inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/../inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                        <div class="row justify-content-center">
                            <div class="col-12 content-header pb-3">
                                <div class="row px-3">
                                    <div class="col-3">
                                        <h2>List of Contracts <?php echo $semiTitle ?></h2>
                                    </div>
                                    <div class="col-9 text-right">
                                        <label for="filter" class="small">Filter By: </label>
                                        <select name="filter_budgetgroup" id="filter_budgetgroup">
                                            <option value="all">Budget Group</option>
                                            <?php
                                            foreach($budgetgroups as $budgetgroup){
                                                echo '<option value="'.$budgetgroup->Budget_Group_ID.'">'.$budgetgroup->Budget_Group_Title.'</option>';
                                            }
                                            ?>
                                        </select>
                                        <small>&</small>
                                        <select name="filter_contractor" id="filter_contractor">
                                            <option value="all">Contractor</option>
                                            <?php
                                            foreach($contractors as $contractor){
                                                echo '<option value="'.$contractor->Contractor_ID.'">'.$contractor->Company_Name.'</option>';
                                            }
                                            ?>
                                        </select>
                                        <button class="btn btn-sm p-0 px-2 small btn-secondary" id="filter">Apply Filter</button>
                                        <a href="<?php dirpath() ?>/dashboard/contracts/create" class="btn btn-primary bg-gradient-primary text-right ml-4">Add New Record</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <table class="table table-hover table-responsive-sm table-striped content-table text-center">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Title</th>
                                            <th>Progress</th>
                                            <th>Contractor</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="list">
                                        <?php
                                        foreach($contracts as $contract){
                                            $contractor = $model->getContractorById($contract->Contractor_ID)->Company_Name;
                                            $status = $model->getStatusById($contract->Contract_Status_ID)->Status;
                                            echo '
                                            <tr>
                                                <td>'.$contract->Contract_ID.'</td>
                                                <td>'.$contract->Contract_Title.'</td>
                                                <td>'.$contract->Progress.'%</td>
                                                <td>'.$contractor.'</td>
                                                <td>'.$status.'</td>
                                                <td>
                                                    <a href="'.$GLOBALS['configurations']['dirpath'].'/dashboard/contracts/view/'.$contract->Contract_ID.'" class="btn btn-sm btn-link" ><i class="cil-external-link"></i></a>
                                                </td>
                                            </tr>
                                            ';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.row-->
                        </div>
                    </div>
                </div>    
            </main>
        </div>
        
        <?php require_once(__DIR__."/../inc/footer.php"); ?>

    </div>

    <script>
        $(document).ready(function(){
            var dirPath = "<?php dirPath() ?>";
            $('#filter').click(function(){
                var budget_group = document.getElementById('filter_budgetgroup').value;
                var contractor = document.getElementById('filter_contractor').value;
                if(budget_group != "all" && contractor != "all"){
                    window.location.replace(dirPath+"/dashboard/contracts/list?contractor="+contractor+"&budget="+budget_group+"");
                }
                else if(budget_group == "all" && contractor != "all"){
                    window.location.replace(dirPath+"/dashboard/contracts/list?contractor="+contractor+"");
                }
                else if(budget_group != "all" && contractor == "all"){
                    window.location.replace(dirPath+"/dashboard/contracts/list?budget="+budget_group+"");
                }
            });
        });
    </script>        

</body>
</html>