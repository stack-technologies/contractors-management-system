<?php 
    require_once(__DIR__."/../inc/header.php"); 
    $model = new \Libraries\Model();
    $contractors = $model->getContractors();
    $beneficiaries = $model->getBeneficiary();
    $assignees = $model->getAssignees();
    $budgetgroups = $model->getBudgetGroup();

    foreach($beneficiaries as $beneficiary){
        $list_of_beneficiaries[] = $beneficiary->Beneficiary_Name;
    }
    foreach($assignees as $assignee){
        $list_of_assignees[] = $assignee->Assignee_Name;
    }
    $getID = $_GET['id'];
    $contract = $model->getContractById($getID);
    $contractor = $model->getContractorById($contract->Contractor_ID)->Company_Name;
    $beneficiary = $contract->Beneficiary;
    $assignee = $contract->Assignee;
    $budget = $model->getBudgetGroupByID($contract->Budget_Group_ID)->Budget_Group_Title;
    $status = $model->getStatusById($contract->Contract_Status_ID)->Status;
    $payment_id = explode(",", $contract->Payment_Terms);
    foreach ($payment_id as $key => $paymentID) {
        $payments[] = $model->getPaymentTermsById($paymentID);
    }
    if($contract->Files != ""){
        $files = explode(',', $contract->Files);
        foreach($files as $file){
            $file_details = $model->getFileById($file);
            $filename[] = $file_details->File_Name;
            $fileclass[] = $file_details->File_Class;
            $filedesc[] = $file_details->File_Description;
            $filetimestamp[] = $file_details->Upload_DateTime;
            $fileuser[] = $model->getUserById($file_details->User_ID)->name;
            $filelink[] = $file_details->File_Link;
        }
    }
    $tags = explode(',', $contract->Tags);

    //Getting the Difference between Dates
    $date = new DateTime('now');
    $currentDate = $date->format('yy-m-d');
    $start_date_diff_color = 'green';
    $end_date_diff_color = 'green';
    //Start Dates
    $planned_start_date = new DateTime($contract->Planned_Start_Date);
    $actual_start_date = new DateTime($contract->Actual_Start_Date);
    //End Dates
    $planned_end_date = new DateTime($contract->Expected_End_Date);
    $actual_end_date = new DateTime($contract->Actual_End_Date);

    //If Dates are Empty
    //Difference
    if($contract->Planned_Start_Date == "0000-00-00"){
        $start_date_diff = $actual_start_date->diff($date)->format("%a");
        $start_date_diff_color = 'orange';
    }
    elseif($contract->Actual_Start_Date == "0000-00-00"){
        $start_date_diff = $date->diff($planned_start_date)->format("%a");
        $start_date_diff_color = 'orange';
    }
    else{
        $start_date_diff = $actual_start_date->diff($planned_start_date)->format("%a");
        if($contract->Planned_Start_Date > $contract->Actual_Start_Date){
            $start_date_diff_color = 'red';
        }
    }
    
    if($contract->Expected_End_Date == "0000-00-00"){
        $end_date_diff = $actual_end_date->diff($date)->format("%a");
        $end_date_diff_color = 'orange';
    }
    elseif($contract->Actual_End_Date == "0000-00-00"){
        $end_date_diff = $date->diff($planned_end_date)->format("%a");
        $end_date_diff_color = 'orange';
    }
    else{
        $end_date_diff = $actual_end_date->diff($planned_end_date)->format("%a");
        if($contract->Expected_End_Date > $contract->Actual_End_Date){
            $end_date_diff_color = 'red';
        }
    }

    //Duration
    if($contract->Planned_Start_Date == "0000-00-00"){
        $planned_dates_duration = $planned_end_date->diff($date)->format("%a");
    }
    elseif($contract->Expected_End_Date == "0000-00-00"){
        $planned_dates_duration = $date->diff($planned_start_date)->format("%a");
    }
    else{
        $planned_dates_duration = $planned_end_date->diff($planned_start_date)->format("%a");
    }

    if($contract->Actual_Start_Date == "0000-00-00"){
        $actual_dates_duration = $actual_end_date->diff($date)->format("%a");
    }
    elseif($contract->Actual_End_Date == "0000-00-00"){
        $actual_dates_duration = $date->diff($actual_start_date)->format("%a");
    }
    else{
        $actual_dates_duration = $actual_end_date->diff($actual_start_date)->format("%a");
    }

    if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['addFile'])){
        /** Getting Files Details **/
        define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT'] . "/contractors");
        $current_timestamp = date('d-m-Y-h-i-s', time());
        $name = $_FILES['file']['name'];
        $file_name = $_POST['add_file_name'];
        $file_class = $_POST['add_file_class'];
        $file_description = $_POST['add_file_description'];
        $file_extension = end(explode(".", $name));
        $filename = $file_name . "_" . $current_timestamp . '.' . $file_extension;
        if (file_exists(ROOT_DIR."/uploads/files/" . $file_name)){
            $filename = $file_name . "_" . date('d-m-Y-h-i-s', time()) . '.' . $file_extension;
        }
        if(!is_dir(ROOT_DIR."/uploads/files/")){
            mkdir(ROOT_DIR."/uploads/files/", 0777, true);
        }
        move_uploaded_file($_FILES['file']["tmp_name"], ROOT_DIR."/uploads/files/". $filename);
        $file_link = "/uploads/files/". $filename;

        /** Creating File Record in the Database and Getting File_ID in return **/
        $create_file = $model->createFile([
            'contract_id' => $contract->Contract_ID,
            'file_name' => $file_name,
            'file_description' => $file_description,
            'file_class' => $file_class,
            'file_link' => $file_link,
            'user_id' => $_SESSION['user']['id'],
        ]);
        $add_new_file_id = $contract->Files . "," . $create_file;
        $updateContractFiles = $model->updateContractFiles([
            'id' => $contract->Contract_ID,
            'files' => $add_new_file_id,
        ]);
        header("Refresh:0");
    }
?>
    <title>Contract # <?php echo $contract->Contract_ID ?> - <?php echo $title ?></title>
    <link rel="stylesheet" href="<?php dirPath() ?>/assets/plugins/bs-tags/bootstrap-taginput.css">
    <style>
    .twitter-typeahead{
        width: 100%;
    }
    .tt-menu{
        background-color: white;
        border-bottom: 1px solid grey;
        border-left: 1px solid grey;
        border-right: 1px solid grey;
        border-bottom-right-radius: 5px;
        border-bottom-left-radius: 5px;
        width: 100%;
        cursor: pointer;
    }
    .tt-selectable{
        padding: 5px 20px;
    }
    .tt-selectable:hover{
        background-color: lightgrey;
    }
    .borderless thead th{
        border: 0px;
    }
    #editPopUp .modal-dialog, #codePopUp .modal-dialog {
        width: 95%;
        height: auto;
        padding: 0;
    }

    #editPopUp .modal-content, #codePopUp .modal-content {
        height: auto;
        min-height: 100%;
        border-radius: 0;
    }
    @media (min-width: 576px) {
        #editPopUp .modal-dialog, #codePopUp .modal-dialog { max-width: none; }
    }
    #start-date-diff{
        color : <?php echo $start_date_diff_color ?>;
    }
    #end-date-diff{
        color : <?php echo $end_date_diff_color ?>;
    }
    </style>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/../inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/../inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                    <?php require(__DIR__.'/../inc/alert.php'); ?>
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header bg-primary text-white">
                                        <div class="row">
                                            <div class="col-sm-6 my-auto">
                                                <h5 class="mt-1"><?php echo $contract->Contract_Title ?></h5>
                                            </div>
                                            <div class="col-sm-6 text-right">
                                                <?php 
                                                    $editedby_user_id = $contract->EditedBy_User_ID;
                                                    if($editedby_user_id != NULL){
                                                        echo '<button class="btn btn-sm btn-light"><small>last edit by:</small><br> '.$model->getUserById($editedby_user_id)->name .'</button>';
                                                    }
                                                ?>
                                                <button class="btn btn-sm btn-light mr-4"><small>created by:</small><br> <?php echo $model->getUserById($contract->CreatedBy_User_ID)->name ?></button>
                                                <a class="btn btn-link edit" title="Edit" data-id="<?php echo $contract->Contract_ID ?>" data-toggle="modal" data-target="#editPopUp">
                                                    <div class="c-icon">
                                                        <i class="cil-external-link"></i>
                                                    </div>
                                                </a>
                                                <a class="btn btn-link delete" title="Delete" data-id="<?php echo $contract->Contract_ID ?>" data-toggle="modal" data-target="#deletePopUp">
                                                    <div class="c-icon">
                                                        <i class="cil-trash"></i>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">    
                                            <div class="col-12">
                                                <small class="text-muted">Description:</small>
                                                <h5><?php echo $contract->Contract_Description ?></h5>
                                                <small class="text-muted">Notes:</small>
                                                <h5><?php echo $contract->Notes ?></h5>
                                                <small class="text-muted">Tags:</small>
                                                <p>
                                                <?php 
                                                    foreach($tags as $tag){
                                                        echo '<button class="btn btn-sm btn-primary mx-1">'.$tag.'</button>';
                                                    }
                                                ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row h-100">
                                            <div class="col-md-6 col-12">
                                                <div class="card h-100">
                                                    <div class="card-header bg-light">
                                                        <h5 class="mt-2">Dates</h5>
                                                    </div>
                                                    <div class="card-body">
                                                        <table class="table table-responsive borderless">
                                                            <thead>
                                                                <tr>
                                                                    <th></th>
                                                                    <th>Planned</th>
                                                                    <th>Actual</th>
                                                                    <th>Difference</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <th>Start Date</th>
                                                                    <td><button class="btn btn-sm btn-dark"><?php echo str_replace("-", "/", $contract->Planned_Start_Date) ?></button></td>
                                                                    <td><button class="btn btn-sm btn-dark"><?php echo str_replace("-", "/", $contract->Actual_Start_Date) ?></button></td>
                                                                    <td id="start-date-diff"><?php echo $start_date_diff ?> days</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>End Date</th>
                                                                    <td><button class="btn btn-sm btn-dark"><?php echo str_replace("-", "/", $contract->Expected_End_Date) ?></button></td>
                                                                    <td><button class="btn btn-sm btn-dark"><?php echo str_replace("-", "/", $contract->Actual_End_Date) ?></button></td>
                                                                    <td id="end-date-diff"><?php echo $end_date_diff ?> days</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Duration</th>
                                                                    <td><?php echo $planned_dates_duration ?> days</td>
                                                                    <td><?php echo $actual_dates_duration ?> days</td>
                                                                    <td></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="card h-100">
                                                    <div class="card-header bg-light">
                                                        <h5 class="mt-2">Information</h5>
                                                    </div>
                                                    <div class="card-body">
                                                        <table class="table table-responsive-sm">
                                                            <tr>
                                                                <th style="border: 0px;">Amount</th>
                                                                <td style="border: 0px;">$ <?php echo $contract->Contract_Total_Amount ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Status</th>
                                                                <td><?php echo $status ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Progress</th>
                                                                <td><?php echo $contract->Progress ?>%</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Contractor</th>
                                                                <td><?php echo $contractor ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Beneficiary</th>
                                                                <td><?php echo $beneficiary ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Assignee</th>
                                                                <td><?php echo $assignee ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Budget</th>
                                                                <td><?php echo $budget ?></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card mt-3">
                                            <div class="card-header bg-light">
                                                <div class="row">
                                                    <div class="col-sm-6 mt-2">
                                                        <h5>Payments</h5>
                                                    </div>
                                                    <div class="col-sm-6 text-right">
                                                        <a class="btn btn-link add-payment" title="Add" data-toggle="modal" data-target="#addPaymentPopUp">
                                                            <div class="c-icon">
                                                                <i class="cil-library-add"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <table class="table table-responsive-sm text-center borderless">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Due Date</th>
                                                            <th>Amount</th>
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        foreach ($payments as $payment) {
                                                            echo '
                                                            <tr>
                                                                <td>'.$payment->CPT_ID.'</td>
                                                                <td>'.$payment->Payment_Due_Date.'</td>
                                                                <td>'.$payment->Payment_Amount.'</td>
                                                                <td>'.$payment->Payment_Status.'</td>
                                                                <td>
                                                                    <a class="btn btn-sm btn-link edit-payment" title="Edit Payment" data-id="'.$payment->CPT_ID.'" data-toggle="modal" data-target="#editPaymentPopUp">
                                                                        <div class="c-icon">
                                                                            <i class="cil-external-link"></i>
                                                                        </div>
                                                                    </a>
                                                                    <a class="btn btn-sm btn-link delete-payment" title="Delete Payment" data-id="'.$payment->CPT_ID.'" data-toggle="modal" data-target="#deletePopUp">
                                                                        <div class="c-icon">
                                                                            <i class="cil-trash"></i>
                                                                        </div>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            ';
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header bg-light"><div class="row">
                                                    <div class="col-sm-6 mt-2">
                                                        <h5>Files & attachments</h5>
                                                    </div>
                                                    <div class="col-sm-6 text-right">
                                                        <a class="btn btn-link add-file" title="Add" data-toggle="modal" data-target="#addFilePopUp">
                                                            <div class="c-icon">
                                                                <i class="cil-library-add"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <table class="table table-hover table-responsive-sm content-table text-center borderless">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Class</th>
                                                            <th>Description</th>
                                                            <th>Timestamp</th>
                                                            <th>User</th>
                                                            <th>Link</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        if($filename != NULL){
                                                            foreach($filename as $key => $name){
                                                                echo '
                                                                <tr>
                                                                    <td>'.$name.'</td>
                                                                    <td>'.$fileclass[$key].'</td>
                                                                    <td>'.$filedesc[$key].'</td>
                                                                    <td>'.$filetimestamp[$key].'</td>
                                                                    <td>'.$fileuser[$key].'</td>
                                                                    <td><a href="'.$GLOBALS['configurations']['dirpath'].'/'.$filelink[$key].'" target="_blank">View File</a></td>
                                                                    <td>
                                                                        <a class="btn btn-sm btn-link edit-file" title="Edit File" data-id="'.$files[$key].'" data-toggle="modal" data-target="#editFilePopUp">
                                                                            <div class="c-icon">
                                                                                <i class="cil-external-link"></i>
                                                                            </div>
                                                                        </a>
                                                                        <a class="btn btn-sm btn-link delete-file" title="Delete File" data-id="'.$files[$key].'" data-toggle="modal" data-target="#deletePopUp">
                                                                            <div class="c-icon">
                                                                                <i class="cil-trash"></i>
                                                                            </div>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                ';
                                                            }
                                                        }
                                                        else{
                                                            echo '<tr class="text-center"><td colspan="7">No Files or Attachments</td></tr>';
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <!-- /.row-->
                        </div>
                    </div>
                </div>    
            </main>
        </div>
        
        <?php require_once(__DIR__."/../inc/footer.php"); ?>

    </div>

    <div id="editPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10" id="editInfo">
                                <h5>Edit Information</h5>
                                <p>Please Enter the new Information Below.</p>
                            </div>
                            <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6 pb-4">
                                            <div class="card h-100">
                                                <div class="card-header bg-light"><strong>Contract Information</strong></div>
                                                <div class="card-body">
                                                    <input type="number" name="editId" id="editId" value="<?php echo $contract->Contract_ID ?>" required hidden>
                                                    <div class="form-group">
                                                        <label for="editTitle">Title</label>
                                                        <input class="form-control" id="editTitle" type="text" name="editTitle" value="<?php echo $contract->Contract_Title ?>" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="editDescription">Description</label>
                                                        <textarea name="description" id="editDescription" class="form-control" rows="5" value="<?php echo $contract->Contract_Description ?>" required><?php echo $contract->Contract_Description ?></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="editTotal_amount">Total Amount</label>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <div class="c-icon">
                                                                        <i class="cil-dollar"></i>
                                                                    </div>
                                                                </span>
                                                            </div>
                                                            <input class="form-control" id="editTotal_amount" type="number" name="editTotal_amount" value="<?php echo $contract->Contract_Total_Amount ?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="editContractor">Contractor</label>
                                                        <select name="editContractor" id="editContractor" class="form-control">
                                                            <option value="<?php echo $contract->Contractor_ID ?>"><?php echo $contractor ?></option>
                                                        <?php
                                                        foreach($contractors as $contractor){
                                                            if($contractor->Contractor_ID != $contract->Contractor_ID)
                                                            echo '<option value="'.$contractor->Contractor_ID.'">'.$contractor->Company_Name.'</option>';
                                                        }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="editBeneficiary">Beneficiary</label><br>
                                                        <input class="form-control" id="editBeneficiary" type="text" name="editBeneficiary" value="<?php echo $beneficiary ?>" data-sanitize="escape" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="editAssignee">Assignee</label><br>
                                                        <input class="form-control" id="editAssignee" type="text" name="editAssignee" value="<?php echo $assignee ?>" data-sanitize="escape" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="editBudget">Budget</label>
                                                        <select name="editBudget" id="editBudget" class="form-control">
                                                            <option value="<?php echo $contract->Budget_Group_ID ?>"><?php echo $budget ?></option>
                                                            <?php
                                                            foreach($budgetgroups as $budgetgroup){
                                                                if($budgetgroup->Budget_Group_ID != $contract->Budget_Group_ID)
                                                                echo '<option value="'.$budgetgroup->Budget_Group_ID.'">'.$budgetgroup->Budget_Group_Title.'</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="editProgress">Progress <input class="btn btn-sm btn-primary" id="editProgress-display" value="<?php echo $contract->Progress ?>%"/></label>
                                                        <input type="range" class="custom-range" name="editProgress" value="<?php echo $contract->Progress ?>" min="0" max="100" step="1" id="editProgress" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="editStatus">Status</label>
                                                        <select name="editStatus" id="editStatus" class="form-control">
                                                            <option value="<?php echo $contract->Contract_Status_ID ?>"><?php echo $status ?></option>
                                                            <option value="Under Review">Under Review</option>
                                                            <option value="Approved">Approved</option>
                                                            <option value="In-Progress">In-Progress</option>
                                                            <option value="On Hold">On Hold</option>
                                                            <option value="Completed">Completed</option>
                                                            <option value="Cancelled">Cancelled</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div><!--card--> 
                                        </div><!--col-->
                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="card-header bg-light"><strong>Dates</strong></div>
                                                <div class="card-body"> 
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <label for="editPlanned_start_date">Planned Start Date</label>
                                                                <input class="form-control" id="editPlanned_start_date" type="date" name="editPlanned_start_date" placeholder="yyyy-mm-dd" value="<?php echo $contract->Planned_Start_Date ?>" required>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label for="editPlanned_end_date">Planned End Date</label>
                                                                <input class="form-control" id="editPlanned_end_date" type="date" name="editPlanned_end_date" placeholder="yyyy-mm-dd" value="<?php echo $contract->Expected_End_Date ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <label for="editActual_start_date">Actual Start Date</label>
                                                                <input class="form-control" id="editActual_start_date" type="date" name="editActual_start_date" placeholder="yyyy-mm-dd" value="<?php echo $contract->Actual_Start_Date ?>" required>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label for="editActual_end_date">Actual End Date</label>
                                                                <input class="form-control" id="editActual_end_date" type="date" name="editActual_end_date" placeholder="yyyy-mm-dd" value="<?php echo $contract->Actual_End_Date ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header bg-light"><strong>Extra Information</strong></div>
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label for="tags">Tags</label>
                                                        <input class="form-control" type="text" id="editTags" name="editTags" value="<?php echo $contract->Tags ?>" data-role="tagsinput" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="editNotes">Notes</label>
                                                        <textarea name="editNotes" id="editNotes" class="form-control" rows="5" value="<?php echo $contract->Notes ?>" required><?php echo $contract->Notes ?></textarea>
                                                    </div>
                                                </div>
                                            </div>        
                                        </div><!--col-->
                                    </div><!--row-->
                                    <div class="card">
                                        <div class="card-footer">
                                            <button class="btn btn-primary w-100" name="editContract" id="editContract">EDIT CONTRACT</button>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--editPopUp modal CLOSE-->

    <div id="editPaymentPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10" id="editInfo">
                                <h5>Edit Information</h5>
                                <p>Please Enter the new Information Below.</p>
                            </div>
                            <div class="col-12">
                                <div class="content-form mx-auto">
                                    <input type="text" name="edit_payment_id" id="edit_payment_id" value="" hidden />
                                    <div class="form-group">
                                        <label for="edit_payment_due_date">Due Date</label>
                                        <input class="form-control" id="edit_payment_due_date" type="date" name="edit_payment_due_date" placeholder="yyyy-mm-dd" value="" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit_payment_amount">Amount</label>
                                        <input class="form-control" id="edit_payment_amount" type="text" name="edit_payment_amount" value="" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit_payment_status">Status</label>
                                        <select name="edit_payment_status" id="edit_payment_status" class="form-control">
                                            <option value="Pending">Pending</option>
                                            <option value="Paid">Paid</option>
                                            <option value="Due">Due</option>
                                            <option value="Cancelled">Cancelled</option>
                                        </select>
                                    </div>
                                    <button name="updatePayment" id="updatePayment" class="btn btn-primary">EDIT</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--editPaymentPopUp modal CLOSE-->

    <div id="addPaymentPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10" id="editInfo">
                                <h5>Add Information</h5>
                                <p>Please Enter the new Information Below.</p>
                            </div>
                            <div class="col-12">
                                <div class="content-form mx-auto">
                                    <div class="form-group">
                                        <label for="add_payment_due_date">Due Date</label>
                                        <input class="form-control" id="add_payment_due_date" type="date" name="add_payment_due_date" placeholder="yyyy-mm-dd" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="add_payment_amount">Amount</label>
                                        <input class="form-control" id="add_payment_amount" type="text" name="add_payment_amount" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="add_payment_status">Status</label>
                                        <select name="add_payment_status" id="add_payment_status" class="form-control">
                                            <option value="Pending">Pending</option>
                                            <option value="Paid">Paid</option>
                                            <option value="Due">Due</option>
                                            <option value="Cancelled">Cancelled</option>
                                        </select>
                                    </div>
                                    <button name="addPayment" id="addPayment" class="btn btn-primary">ADD</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--addPaymentPopUp modal CLOSE-->

    <div id="editFilePopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10" id="editInfo">
                                <h5>Edit Information</h5>
                                <p>Please Enter the new Information Below.</p>
                            </div>
                            <div class="col-12">
                                    <div class="card bg-light">
                                        <div class="card-header"><h4>File Details</h4></div>
                                        <div class="card-body">
                                            <div class="row">
                                                <input type="text" name="edit_file_id" id="edit_file_id" hidden />
                                                <div class="col-sm-6">
                                                    <label for="edit_file_name">File Name</label>
                                                    <input class="form-control" id="edit_file_name" type="text" name="edit_file_name" value="" required>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="edit_file_class">File Class</label>
                                                    <select name="edit_file_class" id="edit_file_class" class="form-control">
                                                        <option value="Contract">Contract</option>
                                                        <option value="Attachment">Attachment</option>
                                                        <option value="Invoice">Invoice</option>
                                                        <option value="Others">Others</option>
                                                    </select>
                                                </div>
                                                <div class="col-12">
                                                    <label for="edit_file_description">File Description</label>
                                                    <textarea class="form-control" id="edit_file_description" name="edit_file_description" value="" required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button name="updateFile" id="updateFile" class="btn btn-primary">EDIT</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--editFilePopUp modal CLOSE-->

    <div id="addFilePopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10" id="editInfo">
                                <h5>Add Information</h5>
                                <p>Please Enter the new Information Below.</p>
                            </div>
                            <div class="col-12">
                                <form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data">
                                <div class="card bg-light">
                                    <div class="card-header"><h4>File Details</h4></div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12 pb-3">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="file" id="file">
                                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="add_file_name">File Name</label>
                                                <input class="form-control" id="add_file_name" type="text" name="add_file_name" value="" required>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="add_file_class">File Class</label>
                                                <select name="add_file_class" id="add_file_class" class="form-control">
                                                    <option value="Contract">Contract</option>
                                                    <option value="Attachment">Attachment</option>
                                                    <option value="Invoice">Invoice</option>
                                                    <option value="Others">Others</option>
                                                </select>
                                            </div>
                                            <div class="col-12">
                                                <label for="add_file_description">File Description</label>
                                                <textarea class="form-control" id="add_file_description" name="add_file_description" value="" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button name="addFile" id="addFile" type="submit" class="btn btn-primary">ADD</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--addFilePopUp modal CLOSE-->

    <div id="deletePopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10">
                                <h5>Are You Sure You Want To Delete?</h5>
                                <br>
                                <button type="button" class="btn btn-danger text-white" id="delTrue">Yes</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--deletePopUp modal CLOSE-->

    
    <script>
        $(document).ready(function(){
            var dirPath = "<?php dirPath() ?>";
            var contractStatusID = "<?php echo $contract->Contract_Status_ID ?>";
            var contractID = document.querySelector('#editId').value;

                // Defining the local dataset for Typeahead
                <?php
                function js_str($s){
                    return '"' . addcslashes($s, "\0..\37\"\\") . '"';
                }
                
                function js_array($array){
                    $temp = array_map('js_str', $array);
                    return '[' . implode(',', $temp) . ']';
                }
                
                echo 'var beneficiaries = ', js_array($list_of_beneficiaries), ';';
                echo 'var assignees = ', js_array($list_of_assignees), ';';
                ?>
                
                // Beneficiaries for TypeAhead
                var beneficiaries = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.whitespace,
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    local: beneficiaries
                });
                $('#editBeneficiary').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                },
                {
                    name: 'beneficiaries',
                    source: beneficiaries
                });

                // Assignees for TypeAhead
                var assignees = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.whitespace,
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    local: assignees
                });
                $('#editAssignee').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1 
                },
                {
                    name: 'assignees',
                    source: assignees
                });


            //Contract Induidual Payment Delete Button onClick()
            $('.delete-payment').on('click', function() {
                var id = $(this).data('id');
                $('#delTrue').click(function(){
                    var request = $.ajax({
                        url: dirPath + "/dashboard/req/delete.php",
                        type: "POST",
                        data: {
                            id : id,
                            key : 'CPT_ID',
                            table : 'Contract_Payment_Terms'
                            },
                        dataType: "html"
                    });

                    request.done(function(msg) {
                        //Updating the Payment ID's in Contracts Table
                        var contract_payment_ids = "<?php echo $contract->Payment_Terms ?>".split(",");
                        //Find and Remove the Deleted Payment ID
                        for(var i = 0; i < contract_payment_ids.length; i++){
                            if(contract_payment_ids[i] == id){
                                contract_payment_id_index = i;
                            }
                        }
                        contract_payment_ids.splice(contract_payment_id_index, 1);
                        
                        //Send Update Request to "Contracts" Table
                        var request = $.ajax({
                            url: dirPath + "/dashboard/req/update.php",
                            type: "POST",
                            data: {
                                id : contractID,
                                payments : contract_payment_ids.join(),
                                table : 'Contracts_Payment'
                                },
                            dataType: "html"
                        });

                        request.done(function(msg) {
                            $("#deletePopUp div div div div").html("Payment Deleted.<br><small>page will reload in 3 seconds</small>");
                            // wait 3 seconds
                            setTimeout(function() {
                                window.location.reload(false)
                            }, 3000);
                        });

                        request.fail(function(jqXHR, textStatus) {
                            $("#deletePopUp div div div div").html("Error - Request failed: " + textStatus);
                        });
                    });

                    request.fail(function(jqXHR, textStatus) {
                        $("#deletePopUp div div div div").html("Error - Request failed: " + textStatus);
                    });
                });
            });

            //Contract Induidual Payment Add Button onClick()
            $('.add-payment').on('click', function(){
                $('#addPayment').on('click', function(){
                    var addPaymentDate = document.getElementById('add_payment_due_date').value;
                    var addPaymentAmount = document.getElementById('add_payment_amount').value;
                    var addPaymentStatus = document.getElementById('add_payment_status').value;
                    
                    var request = $.ajax({
                        url: dirPath + "/dashboard/req/add.php",
                        type: "POST",
                        data: {
                            id : contractID,
                            paymentDate : addPaymentDate,
                            paymentAmount : addPaymentAmount,
                            paymentStatus : addPaymentStatus,
                            table : 'Contract_Payment_Terms'
                            },
                        dataType: "html"
                    });

                    request.done(function(msg) {
                        //Updating Payment ID's in "Contracts" Table
                        var contract_payment_ids = "<?php echo $contract->Payment_Terms ?>,"+ msg + "";

                        var request = $.ajax({
                            url: dirPath + "/dashboard/req/update.php",
                            type: "POST",
                            data: {
                                id : contractID,
                                payments : contract_payment_ids,
                                table : 'Contracts_Payment'
                                },
                            dataType: "html"
                        });

                        request.done(function(msg) {
                            $("#addPaymentPopUp div div div div").html("Payment Added.<br><small>page will reload in 3 seconds</small>");
                            // wait 3 seconds
                            setTimeout(function() {
                                window.location.reload(false)
                            }, 3000);
                        });

                        request.fail(function(jqXHR, textStatus) {
                            $("#addPaymentPopUp div div div div").html("Error - Request failed: " + textStatus);
                        });
                    });

                    request.fail(function(jqXHR, textStatus) {
                        $("#addPaymentPopUp div div div div").html("Error - Request failed: " + textStatus);
                    });
                });
            });

            //Contract Induidual Payment Edit Button onClick()
            $('.edit-payment').on('click', function(){
                var id = $(this).data('id');
                var request = $.ajax({
                    url: dirPath + "/dashboard/req/edit.php",
                    type: "POST",
                    data: {
                        id : id,
                        table : 'Contract_Payment_Terms'
                        },
                    dataType: "html"
                });

                request.done(function(msg) {
                    var obj = JSON.parse(msg);
                    document.getElementById('edit_payment_id').value = obj.CPT_ID;
                    document.getElementById('edit_payment_due_date').value = obj.Payment_Due_Date;
                    document.getElementById('edit_payment_amount').value = obj.Payment_Amount;
                    document.getElementById('edit_payment_status').value = obj.Payment_Status;

                    //Updating
                    $('#updatePayment').on('click', function(){
                        var editPaymentID = document.getElementById('edit_payment_id').value;
                        var editPaymentDate = document.getElementById('edit_payment_due_date').value;
                        var editPaymentAmount = document.getElementById('edit_payment_amount').value;
                        var editPaymentStatus = document.getElementById('edit_payment_status').value;

                        var request = $.ajax({
                            url: dirPath + "/dashboard/req/update.php",
                            type: "POST",
                            data: {
                                id : editPaymentID,
                                paymentDate : editPaymentDate,
                                paymentAmount : editPaymentAmount,
                                paymentStatus : editPaymentStatus,
                                table : 'Contract_Payment_Terms'
                                },
                            dataType: "html"    
                        });

                        request.done(function(msg) {
                            $("#editPaymentPopUp div div div div").html("Payment Updated.<br><small>page will reload in 3 seconds</small>");
                            /// wait 3 seconds
                            setTimeout(function() {
                                window.location.reload(false)
                            }, 3000);
                        });
                        request.fail(function(jqXHR, textStatus) {
                            $("#editPaymentPopUp div div div div").html("Error - Request failed: " + textStatus);
                        });
                    });

                });

                request.fail(function(jqXHR, textStatus) {
                    $("#editPaymentPopUp div div div div").html("Error - Request failed: " + textStatus);
                });
            });

            //Contract Induidual Payment Delete Button onClick()
            $('.delete-file').on('click', function() {
                var id = $(this).data('id');
                $('#delTrue').click(function(){
                    var request = $.ajax({
                        url: dirPath + "/dashboard/req/delete.php",
                        type: "POST",
                        data: {
                            id : id,
                            key : 'File_ID',
                            table : 'Contract_Files'
                            },
                        dataType: "html"
                    });

                    request.done(function(msg) {
                        //Updating the Files ID's in Contracts Table
                        var contract_file_ids = "<?php echo $contract->Files ?>".split(",");
                        //Find and Remove the Deleted Files ID
                        for(var i = 0; i < contract_file_ids.length; i++){
                            if(contract_file_ids[i] == id){
                                contract_file_id_index = i;
                            }
                        }
                        contract_file_ids.splice(contract_file_id_index, 1);
                        
                        //Send Update Request to "Contracts" Table
                        var request = $.ajax({
                            url: dirPath + "/dashboard/req/update.php",
                            type: "POST",
                            data: {
                                id : contractID,
                                files : contract_file_ids.join(),
                                table : 'Contracts_File'
                                },
                            dataType: "html"
                        });

                        request.done(function(msg) {
                            $("#deletePopUp div div div div").html("File Deleted.<br><small>page will reload in 3 seconds</small>");
                            // wait 3 seconds
                            setTimeout(function() {
                                window.location.reload(false)
                            }, 3000);
                        });

                        request.fail(function(jqXHR, textStatus) {
                            $("#deletePopUp div div div div").html("Error - Request failed: " + textStatus);
                        });
                    });

                    request.fail(function(jqXHR, textStatus) {
                        $("#deletePopUp div div div div").html("Error - Request failed: " + textStatus);
                    });
                });
            });

            //Contract Induidual File Edit Button onClick()
            $('.edit-file').on('click', function(){
                var id = $(this).data('id');
                var request = $.ajax({
                    url: dirPath + "/dashboard/req/edit.php",
                    type: "POST",
                    data: {
                        id : id,
                        table : 'Contract_Files'
                        },
                    dataType: "html"
                });

                request.done(function(msg) {
                    var obj = JSON.parse(msg);
                    document.getElementById('edit_file_id').value = obj.File_ID;
                    document.getElementById('edit_file_name').value = obj.File_Name;
                    document.getElementById('edit_file_class').value = obj.File_Class;
                    document.getElementById('edit_file_description').value = obj.File_Description;

                    //Updating
                    $('#updateFile').on('click', function(){
                        var editFileID = document.getElementById('edit_file_id').value;
                        var editFileName = document.getElementById('edit_file_name').value;
                        var editFileClass = document.getElementById('edit_file_class').value;
                        var editFileDesc = document.getElementById('edit_file_description').value;

                        var request = $.ajax({
                            url: dirPath + "/dashboard/req/update.php",
                            type: "POST",
                            data: {
                                id : editFileID,
                                fileName : editFileName,
                                fileClass : editFileClass,
                                fileDesc : editFileDesc,
                                table : 'Contract_Files'
                                },
                            dataType: "html"    
                        });

                        request.done(function(msg) {
                            $("#editFilePopUp div div div div").html("File Updated.<br><small>page will reload in 3 seconds</small>");
                            /// wait 3 seconds
                            setTimeout(function() {
                                window.location.reload(false)
                            }, 3000);
                        });
                        request.fail(function(jqXHR, textStatus) {
                            $("#editFilePopUp div div div div").html("Error - Request failed: " + textStatus);
                        });
                    });

                });

                request.fail(function(jqXHR, textStatus) {
                    $("#editFilePopUp div div div div").html("Error - Request failed: " + textStatus);
                });
            });
                        
            //Contract Main Details Delete Button onClick()
            $('.delete').on('click', function() {
                var id = $(this).data('id');
                $('#delTrue').click(function(){
                    var request = $.ajax({
                        url: dirPath + "/dashboard/req/delete.php",
                        type: "POST",
                        data: {
                            id : id,
                            key : 'Contract_ID',
                            table : 'Contracts'
                            },
                        dataType: "html"
                    });

                    request.done(function(msg) {
                        console.log(msg);
                        $("#deletePopUp div div div div").html("Contract Deleted.<br><small>page will reload in 3 seconds</small>");
                        /// wait 3 seconds
                        setTimeout(function() {
                            window.location.reload(false)
                        }, 3000);
                    });

                    request.fail(function(jqXHR, textStatus) {
                        $("#deletePopUp div div div div").html("Error - Request failed: " + textStatus);
                    });
                });
            });

            //Contract Main Details Edit Button onClick()
            //Brings Up the and Poulates the Popup
            $('.edit').on('click', function(){
                var id = $(this).data('id');
                $('#editProgress').change(function(){
                    $('#editProgress-display').val($(this).val()+"%");
                });
                $('#editProgress-display').keyup(function(){
                    var val = $(this).val();
                    if(val > 100){
                        val = 100;
                        $('#editProgress-display').val(val);
                    }
                    else if(val < 0 || isNaN(val)){
                        val = 0;
                        $('#editProgress-display').val(val);
                    }
                    $('#editProgress').val(val);
                });
            });
            //Updates the Contract Main Details
            $(document).on('click', '#editContract', function(){
                var id = document.querySelector('#editId').value;
                var contractTitle = document.querySelector('#editTitle').value;
                var contractDesc = document.querySelector('#editDescription').value;
                var contractAmount = document.querySelector('#editTotal_amount').value;
                var contractContractor = document.querySelector('#editContractor').value;
                var contractBeneficiary = document.querySelector('#editBeneficiary').value;
                var contractAssignee = document.querySelector('#editAssignee').value;
                var contractBudget = document.querySelector('#editBudget').value;
                var contractProgress = document.querySelector('#editProgress').value;
                var contractStatus = document.querySelector('#editStatus').value;
                var contractNotes = document.querySelector('#editNotes').value;
                var contractPlannedStartDate = document.querySelector('#editPlanned_start_date').value;
                var contractPlannedEndDate = document.querySelector('#editPlanned_end_date').value;
                var contractActualStartDate = document.querySelector('#editActual_start_date').value;
                var contractActualEndDate = document.querySelector('#editActual_end_date').value;
                var contractEditByUserID = "<?php echo $_SESSION['user']['id']; ?>";
                var contractTags = document.querySelector('#editTags').value;

                var request = $.ajax({
                    url: dirPath + "/dashboard/req/update.php",
                    type: "POST",
                    data: {
                        id : id,
                        contractTitle : contractTitle,
                        contractDesc : contractDesc,
                        contractAmount : contractAmount,
                        contractContractor : contractContractor,
                        contractBeneficiary : contractBeneficiary,
                        contractAssignee : contractAssignee,
                        contractBudget : contractBudget,
                        contractProgress : contractProgress,
                        contractNotes : contractNotes,
                        contractPlannedStartDate : contractPlannedStartDate,
                        contractPlannedEndDate : contractPlannedEndDate,
                        contractActualStartDate : contractActualStartDate,
                        contractActualEndDate : contractActualEndDate,
                        contractEditByUserID : contractEditByUserID,
                        contractTags : contractTags,
                        table : 'Contracts'
                        },
                    dataType: "html"    
                });

                request.done(function(msg) {
                    if(contractStatus != contractStatusID){
                        var request = $.ajax({
                            url: dirPath + "/dashboard/req/update.php",
                            type: "POST",
                            data: {
                                id : contractStatusID, 
                                status : contractStatus, 
                                table : 'Contract_Status'
                                },
                            dataType: "html"    
                        });

                        request.done(function(msg) {
                            $("#editPopUp div div div div").html("Contract Updated.<br><small>page will refresh in 3 seconds</small>");
                            /// wait 3 seconds
                            setTimeout(function() {
                                window.location.reload(false)
                            }, 3000);
                        });

                        request.fail(function(jqXHR, textStatus) {
                            $("#editPopUp div div div div").html("Error - Request failed: " + textStatus);
                        });
                    }
                    else{
                        $("#editPopUp div div div div").html("Contract Updated.<br><small>page will refresh in 3 seconds</small>");
                        /// wait 3 seconds
                        setTimeout(function() {
                            window.location.reload(false)
                        }, 3000);
                    }
                    
                });

                request.fail(function(jqXHR, textStatus) {
                    $("#editPopUp div div div div").html("Error - Request failed: " + textStatus);
                });
            });

        });
    </script>
    <script src="<?php dirPath() ?>/assets/plugins/bs-tags/bootstrap-tagsinput.js"></script>


</body>
</html>
