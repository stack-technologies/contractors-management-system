<?php 
    require_once(__DIR__."/../inc/header.php"); 
    $model = new \Libraries\Model();
    $getACLID = $_REQUEST['acl'];
    if($getACLID == NULL){
        $assignees = $model->getAssignees();
    }
    else{
        $tempassignees = $model->getAssigneesByACL($getACLID);
        foreach($tempassignees as $tempassignee){
            $assignee_id[] = $tempassignee->Assignee_ID;
        }
        $assignee_ids = array_values(array_unique($assignee_id));
        foreach($assignee_ids as $assigneeid){
            $assignees[] = $model->getAssigneeByID($assigneeid);
        }
    }
?>
    <title>List of Assignee - <?php echo $title ?></title>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/../inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/../inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                    <?php require(__DIR__.'/../inc/alert.php'); ?>
                        <div class="row justify-content-center">
                            <div class="col-12 content-header pb-3">
                                <div class="row px-3">
                                    <div class="col-6">
                                        <h2>List of Assignee</h2>
                                    </div>
                                    <div class="col-6 text-right">
                                        <a href="<?php dirpath() ?>/dashboard/assignee/create" class="btn btn-primary bg-gradient-primary text-right">Add New Record</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <table class="table table-hover table-responsive-sm table-striped content-table text-center">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Assignee Name</th>
                                            <th>Beneficiary</th>
                                            <th>Timestamp</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach($assignees as $assignee){
                                            echo '
                                            <tr>
                                                <td>'.$assignee->Assignee_ID.'</td>
                                                <td>'.$assignee->Assignee_Name.'</td>
                                                <td>'.$model->getBeneficiaryById($assignee->Beneficiary_ID)->Beneficiary_Name.'</td>
                                                <td>'.$assignee->Timestamp.'</td>
                                                <td>
                                                    <a href="'.$GLOBALS['configurations']['dirpath'].'/dashboard/assignee/view/'.$assignee->Assignee_ID.'" class="btn btn-sm btn-link" >
                                                        <i class="cil-external-link"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            ';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.row-->
                        </div>
                    </div>
                </div>    
            </main>
        </div>
        
        <?php require_once(__DIR__."/../inc/footer.php"); ?>

    </div>
    
</body>
</html>