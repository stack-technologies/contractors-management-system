<?php 
    require_once(__DIR__."/../inc/header.php"); 
    $model = new \Libraries\Model();
    $beneficiaries = $model->getBeneficiary();
    $categories = $model->getACLCategories();

    $getID = $_GET['id'];
    $assignee = $model->getAssigneeById($getID);
    $beneficiary = $model->getBeneficiaryByID($assignee->Beneficiary_ID)->Beneficiary_Name;
    $assignee_acls = explode(',', $assignee->Assignee_ACLs);

    if(count($assignee_acls) != 0){ 
        $aclcategories = array();
        foreach($assignee_acls as $assignee_acl){
            $tempacl = json_decode(json_encode($model->getAssigneeACLById($assignee_acl)), true);
            $acl = $model->getACLById($tempacl['ACL_ID']);
            $acl_cat = $acl->ACL_Category;
            if(array_search($acl_cat, $aclcategories) === FALSE){
                array_push($aclcategories, $acl_cat);
            }
            $tempacl = array_merge($tempacl, array(
                'ACL_Category' => $acl_cat,
                'ACL_Parameter' => $acl->ACL_Parameter,
            ));
            $acls[] = $tempacl;
        }
    }

    if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['addAssigneeACL'])){
        for($i = 1; $i <= $_POST['num_of_assignee_acls']; $i++){
            $acl_status = $_POST['acl_status_'.$i];
            if($acl_status == ""){
                $acl_status = 0;
            }
            elseif($acl_status == "on"){
                $acl_status = 1;
            }
            $getACLCategory = $_POST['acl_category_'.$i];
            $getACLParameter = $_POST['acl_parameter_'.$i];
            $acl = $model->getACLByCategoryAndParameter($getACLCategory, $getACLParameter);
            $createAssigneeACL[] = $model->createAssigneeACL([
                'assignee_id' => $assignee->Assignee_ID, 
                'active' => $acl_status, 
                'acl' => $acl->ACL_ID, 
                'acl_notes' => $_POST['acl_notes_'.$i], 
                'user_id' => $_SESSION['user']['id'],
            ]);
        }
        $assigneeACLs = implode (",", $createAssigneeACL);
        $addACLToAssignee = $model->updateAssigneeACLs([
            'id' => $assignee->Assignee_ID, 
            'acl' => $assignee->Assignee_ACLs.",".$assigneeACLs, 
        ]);
        if($addACLToAssignee){
            $alert = [
                'type' => 'success',
                'message' => 'Assignee ACL Added'
            ];
            header("refresh:1;");
        }
        else{
            $alert = [
                'type' => 'danger',
                'message' => 'Error Creating Assignee ACLs. Server Error, Contact Adminstrator/Developer.'
            ];
        }
    }

?>
    <title>Assignee # <?php echo $assignee->Assignee_ID ?> - <?php echo $title ?></title>
    <style>
    .twitter-typeahead{
        width: 100%;
    }
    .tt-menu{
        background-color: white;
        border-bottom: 1px solid grey;
        border-left: 1px solid grey;
        border-right: 1px solid grey;
        border-bottom-right-radius: 5px;
        border-bottom-left-radius: 5px;
        width: 100%;
        cursor: pointer;
    }
    .tt-selectable{
        padding: 5px 20px;
    }
    .tt-selectable:hover{
        background-color: lightgrey;
    }
    .borderless thead th{
        border: 0px;
    }
    #editPopUp .modal-dialog, #codePopUp .modal-dialog {
        width: 95%;
        height: auto;
        padding: 0;
    }

    #editPopUp .modal-content, #codePopUp .modal-content {
        height: auto;
        min-height: 100%;
        border-radius: 0;
    }
    @media (min-width: 576px) {
        #editPopUp .modal-dialog, #codePopUp .modal-dialog { max-width: none; }
    }
    </style>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/../inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/../inc/navbar.php");?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                    <?php require(__DIR__.'/../inc/alert.php'); ?>
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header bg-gradient-primary text-white">
                                        <div class="row">
                                            <div class="col-sm-6 my-auto">
                                                <small class="text-muted">Assignee Name:</small>
                                                <h5 class="mt-1"><?php echo $assignee->Assignee_Name ?></h5>
                                                <small class="text-muted">Beneficiary:</small>
                                                <h5 class="mt-1"><?php echo $beneficiary ?></h5>
                                            </div>
                                            <div class="col-sm-6 text-right">
                                                <button class="btn btn-sm btn-light"><small>Created By:</small><br> <?php echo $model->getUserById($assignee->User_ID)->name ?></button>
                                                <button class="btn btn-sm btn-light"><small>Timestamp:</small><br> <?php echo $assignee->Timestamp ?></button><br>
                                                <a class="btn btn-link add mt-3" title="Add ACL(s)" data-toggle="modal" data-target="#addACLPopUp">
                                                    <div class="c-icon">
                                                        <i class="cil-library-add"></i>
                                                    </div>
                                                </a>        
                                                <a class="btn btn-link edit mt-3" title="Edit" data-id="<?php echo $assignee->Assignee_ID ?>" data-toggle="modal" data-target="#editPopUp">
                                                    <div class="c-icon">
                                                        <i class="cil-pencil"></i>
                                                    </div>
                                                </a>
                                                <a class="btn btn-link delete mt-3" title="Delete" data-id="<?php echo $assignee->Assignee_ID ?>" data-toggle="modal" data-target="#deletePopUp">
                                                    <div class="c-icon">
                                                        <i class="cil-trash"></i>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row py-2">    
                                            <div class="col-12">
                                                <?php
                                                foreach($aclcategories as $category){
                                                    echo '<div class="alert alert-dark text-center">ACL - '.$category.'</div>
                                                    <div class="row">';
                                                    foreach($acls as $acl){
                                                        if($acl['ACL_Category'] == $category){
                                                            if($acl['Active'] == 0){
                                                                $status = "";
                                                            }
                                                            else{
                                                                $status = "checked";
                                                            }
                                                            echo '
                                                            <div class="col-4">
                                                            <div class="card">
                                                                <div class="card-header bg-gradient-dark text-white px-4 p-1">
                                                                    <div class="row">
                                                                        <div class="col-sm-6 my-auto">
                                                                            <small class="text-muted">Parameter:</small>
                                                                            <h5 class="mt-1">'.$acl['ACL_Parameter'].'</h5>
                                                                        </div>    
                                                                        <div class="col-sm-6 text-right my-auto p-0">
                                                                            <a class="btn btn-link btn-sm editACL" title="Edit ACL" data-id="'.$acl['Assignee_ACL_ID'].'" data-toggle="modal" data-target="#editACLPopUp">
                                                                                <div class="c-icon">
                                                                                    <i class="cil-pencil"></i>
                                                                                </div>
                                                                            </a>
                                                                            <a class="btn btn-link btn-sm deleteACL" title="Delete" data-id="'.$acl['Assignee_ACL_ID'].'" data-toggle="modal" data-target="#deleteACLPopUp">
                                                                                <div class="c-icon">
                                                                                    <i class="cil-trash"></i>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                    </div>    
                                                                </div>
                                                                <div class="card-body pl-4 p-1">
                                                                    <small class="text-muted">Notes:</small>
                                                                    <p>'.$acl['Notes'].'</p>
                                                                    <small class="text-muted">Timestamp:</small>
                                                                    <p>'.$acl['Timestamp'].'</p>
                                                                    <div class="row">
                                                                        <div class="col-12 col-sm-6">
                                                                            <small class="text-muted">Added By:</small>
                                                                            <p>'.$model->getUserByID($acl['User_ID'])->name.'</p>                                                                
                                                                        </div>
                                                                        <div class="col-12 col-sm-6">
                                                                            <small class="text-muted">Active:</small><br>
                                                                            <input class="mt-1 acl_status" type="checkbox" '.$status.'>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                            </div>';
                                                        }
                                                    }
                                                    echo "</div>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <!-- /.row-->
                        </div>
                    </div>
                </div>    
            </main>
        </div>
        
        <?php require_once(__DIR__."/../inc/footer.php"); ?>

    </div>

    <div id="editPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10" id="editInfo">
                                <h5>Edit Information</h5>
                                <p>Please Enter the new Information Below.</p>
                            </div>
                            <div class="col-12">
                                <div class="content-form mx-auto">
                                    <input type="number" name="editId" id="editId" required hidden>
                                    <div class="form-group">
                                        <label for="editAssigneeName">Assignee Name</label>
                                        <input type="text" name="editAssigneeName" id="editAssigneeName" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="editBeneficiary">Beneficiary</label>
                                        <select name="editBeneficiary" id="editBeneficiary" class="form-control">
                                            <?php
                                            foreach($beneficiaries as $beneficiary){
                                                echo '<option value="'.$beneficiary->Beneficiary_ID.'">'.$beneficiary->Beneficiary_Name.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <button name="editAssignee" id="editAssignee" class="btn btn-primary w-100">EDIT ASSIGNEE</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--editPopUp modal CLOSE-->

    <div id="deletePopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10">
                                <h5>Are You Sure You Want To Delete?</h5>
                                <br>
                                <button type="button" class="btn btn-danger text-white" id="delTrue">Yes</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--deletePopUp modal CLOSE-->

    <div id="addACLPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10" id="editInfo">
                                <h5>Edit Information</h5>
                                <p>Please Enter the new Information Below.</p>
                            </div>
                            <div class="col-12">
                            <form action="<?php $_PHP_SELF ?>" method="POST">
                                <div class="alert alert-dark text-center m-0 mt-3 p-1">Add Assignee ACL(s)</div>
                                <input type="text" name="num_of_assignee_acls" id="num_of_assignee_acls" value="1" hidden>
                                <table class="table table-responsive-sm">
                                    <thead class="table-borderless text-center">
                                        <tr>
                                            <th>Active</th>
                                            <th>Category</th>
                                            <th>Parameter</th>
                                            <th>Notes</th>
                                            <th>
                                                <button type="button" id="add_row_btn" class="btn btn-sm btn-secondary bg-gradient-light mt-1">
                                                    <div class="c-icon">
                                                        <i class="cil-library-add"></i>
                                                    </div>
                                                </button>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="rows">
                                        <!--row-->
                                        <tr>
                                            <td class="text-center pt-3"><input id="acl_status_1" type="checkbox" name="acl_status_1"></td>
                                            <td>
                                                <select class="form-control form-control-sm acl_category" name="acl_category_1" id="acl_category_1" data-count="1">
                                                    <?php
                                                    foreach($categories as $category){
                                                        echo '<option value="'.$category->ACL_Category.'">'.$category->ACL_Category.'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select class="form-control form-control-sm" name="acl_parameter_1" id="acl_parameter_1">
                                                </select>
                                            </td>
                                            <td><textarea class="form-control form-control-sm" id="acl_notes_1" name="acl_notes_1" data-sanitize="escape" rows="1"></textarea></td>
                                            <td class="text-center">
                                                <button type="button" data-id="1" name="acl_remove_1" id="row_remove_1" class="row-remove btn btn-sm p-0">
                                                    <div class="c-icon">
                                                        <i class="cil-trash text-danger"></i>
                                                    </div>
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button name="addAssigneeACL" id="addAssigneeACL" class="btn btn-primary w-100" type="submit">ADD ASSIGNEE ACL(s)</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--add ACL PopUp modal CLOSE-->

    <div id="editACLPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10" id="editInfo">
                                <h5>Edit Information</h5>
                                <p>Please Enter the new Information Below.</p>
                            </div>
                            <div class="col-12">
                                <div class="content-form mx-auto">
                                    <input type="number" name="editId" id="editId" required hidden>
                                    <div class="form-group">
                                        <label for="editACLStatus">Active</label>       
                                        <input type="checkbox" name="editACLStatus" id="editACLStatus">
                                    </div>
                                    <div class="form-group">
                                        <label for="editACLCategory">Category</label>
                                        <select class="form-control form-control-sm" name="editACLCategory" id="editACLCategory">
                                            <?php
                                            foreach($categories as $category){
                                                echo '<option value="'.$category->ACL_Category.'">'.$category->ACL_Category.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="editACLParameter">Parameter</label>
                                        <select name="editACLParameter" id="editACLParameter" class="form-control form-control-sm">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="editACLNotes">Notes</label>
                                        <textarea class="form-control form-control-sm" id="editACLNotes" name="editACLNotes" data-sanitize="escape" rows="1"></textarea>
                                    </div>
                                    <button name="editACLAssignee" id="editACLAssignee" class="btn btn-primary w-100">EDIT ASSIGNEE ACL</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--edit ACL PopUp modal CLOSE-->

    <div id="deleteACLPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10">
                                <h5>Are You Sure You Want To Delete?</h5>
                                <br>
                                <button type="button" class="btn btn-danger text-white" id="delACLTrue">Yes</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--delete ACL PopUp modal CLOSE-->
    
    <script>
        $(document).ready(function(){
            var dirPath = "<?php dirPath() ?>";
            $(".acl_status:checkbox").click(function() { return false; });

            //GET MAIN ASSIGNEE DETAILS FOR EDIT
            $('.edit').on('click', function(){
                var dirPath = "<?php dirPath() ?>";
                var id = $(this).data('id');                
                var request = $.ajax({
                    url: dirPath + "/dashboard/req/edit.php",
                    type: "POST",
                    data: {
                        id : id,
                        table : 'Assignee'
                        },
                    dataType: "html"
                });

                request.done(function(msg) {
                    var obj = JSON.parse(msg);
                    document.getElementById('editId').value = obj.Assignee_ID;
                    document.getElementById('editAssigneeName').value = obj.Assignee_Name;
                    document.getElementById('editBeneficiary').value = obj.Beneficiary_ID;
                });

                request.fail(function(jqXHR, textStatus) {
                    $("#editPopUp div div div div").html("Error - Request failed: " + textStatus);
                });
            });
            //EDIT MAIN ASSIGNEE
            $('#editAssignee').on('click', function(){
                var dirPath = "<?php dirPath() ?>";
                var id = document.querySelector('#editId').value;
                var assigneeName = document.querySelector('#editAssigneeName').value;
                var beneficiaryID = document.querySelector('#editBeneficiary').value;
                
                if(assigneeName != ""){
                    var request = $.ajax({
                        url: dirPath + "/dashboard/req/update.php",
                        type: "POST",
                        data: {
                            id : id,
                            assigneeName : assigneeName, 
                            beneficiaryID : beneficiaryID, 
                            table : 'Assignee'
                            },
                        dataType: "html"    
                    });

                    request.done(function(msg) {
                        $("#editPopUp div div div div").html("Assignee Updated.<br><small>page will reload in 3 seconds</small>");
                        /// wait 3 seconds
                        setTimeout(function() {
                            window.location.reload(false)
                        }, 3000);
                    });

                    request.fail(function(jqXHR, textStatus) {
                        $("#editPopUp div div div div").html("Error - Request failed: " + textStatus);
                    });
                }
                else{
                    $("#editPopUp div div div div").html("Error - Textfield Empty");
                    /// wait 3 seconds
                    setTimeout(function() {
                        window.location.reload(false)
                    }, 2000);
                }
            });

            //DELETE MAIN ASSIGNEE
            $('.delete').on('click', function() {
                var dirPath = "<?php dirPath() ?>";
                var id = $(this).data('id');
                $('#delTrue').click(function(){
                    var request = $.ajax({
                        url: dirPath + "/dashboard/req/delete.php",
                        type: "POST",
                        data: {
                            id : id,
                            key : 'Assignee_ID',
                            table : 'Assignee'
                            },
                        dataType: "html"
                    });

                    request.done(function(msg) {
                        console.log(msg);
                        $("#deletePopUp div div div div").html("Assignee Deleted.<br><small>page will redirect in 3 seconds</small>");
                        /// wait 3 seconds
                        setTimeout(function() {
                            window.location.href = dirPath + "/dashboard/assignee/list"
                        }, 3000);
                    });

                    request.fail(function(jqXHR, textStatus) {
                        $("#deletePopUp div div div div").html("Error - Request failed: " + textStatus);
                    });
                });
            });  
        }); 
    
        //GET ASSIGNEE ACL DETAILS FOR EDIT
        $(document).on('click', '.editACL', function(){
            var dirPath = "<?php dirPath() ?>";
            var id = $(this).data('id');
            var request = $.ajax({
                url: dirPath + "/dashboard/req/edit.php",
                type: "POST",
                data: {
                    id : id,
                    table : 'Assignee_ACL'
                    },
                dataType: "html"
            });

            request.done(function(msg) {
                var obj = JSON.parse(msg);
                document.getElementById('editACLNotes').value = obj.Notes;
                if(obj.Active == 1){
                    var att = document.createAttribute("checked");
                    document.getElementById('editACLStatus').setAttributeNode(att);
                }
                document.getElementById('editId').value = obj.Assignee_ACL_ID;
                var acl_id = obj.ACL_ID;
                var request = $.ajax({
                    url: dirPath + "/dashboard/req/edit.php",
                    type: "POST",
                    data: {
                        id : acl_id,
                        table : 'ACL'
                    },
                    dataType: "html"    
                });

                request.done(function(msg) {
                    var acl_obj = JSON.parse(msg);
                    var category = acl_obj.ACL_Category;
                    var parameter = acl_obj.ACL_Parameter;
                    document.getElementById('editACLCategory').value = category;
                    var request = $.ajax({
                        url: dirPath + "/dashboard/req/parameters.php",
                        type: "POST",
                        data: {
                            category : category,
                            },
                        dataType: "html"    
                    });

                    request.done(function(msg) {
                        var obj = JSON.parse(msg);
                        var len = obj.length;
                        var data = "";
                        for(var i = 0; i < len; i++){
                            data += '<option value="'+obj[i].ACL_Parameter+'">'+obj[i].ACL_Parameter+'</option>';
                        }
                        $('#editACLParameter').html(data);
                        $('#editACLParameter').val(parameter);
                    });

                    request.fail(function(jqXHR, textStatus) {
                        $('#editACLParameter').html("Error");
                    });
                });

                request.fail(function(jqXHR, textStatus) {
                    $('#editACLParameter').html("Error");
                });
            });

            request.fail(function(jqXHR, textStatus) {
                $("#editPopUp div div div div").html("Error - Request failed: " + textStatus);
            });
        });
        //SET PARAMETERS ACCORDING TO CATEGORY DURING EDIT ASSIGNEE ACL
        $(document).on('change', '#editACLCategory', function(){
            var dirPath = "<?php dirPath() ?>";
            var category = $(this).val();
            var request = $.ajax({
                url: dirPath + "/dashboard/req/parameters.php",
                type: "POST",
                data: {
                    category : category,
                    },
                dataType: "html"    
            });

            request.done(function(msg) {
                var obj = JSON.parse(msg);
                var len = obj.length;
                var data = "";
                for(var i = 0; i < len; i++){
                    data += '<option value="'+obj[i].ACL_Parameter+'">'+obj[i].ACL_Parameter+'</option>';
                }
                $('#editACLParameter').html(data);
            });

            request.fail(function(jqXHR, textStatus) {
                $('#editACLParameter').html("Error");
            });
        });
        //EDIT ASSIGNEE ACL
        $('#editACLAssignee').on('click', function(){
            var dirPath = "<?php dirPath() ?>";
            var id = document.querySelector('#editId').value;
            if($('#editACLStatus').is(":checked")){
                active = 1;
            }
            else{
                active = 0;
            }
            var notes = document.querySelector('#editACLNotes').value;
            var parameter = document.querySelector('#editACLParameter').value;
            var category = document.querySelector('#editACLCategory').value;
            var request = $.ajax({
                url: dirPath + "/dashboard/req/edit.php",
                type: "POST",
                data: {
                    category : category,
                    parameter : parameter,
                    table : 'ACLByCategoryAndParameter'
                    },
                dataType: "html"    
            });

            request.done(function(msg) {
                var acl_id = msg;
                if(acl_id != ""){
                    var request = $.ajax({
                        url: dirPath + "/dashboard/req/update.php",
                        type: "POST",
                        data: {
                            id : id,
                            acl : acl_id, 
                            active : active, 
                            notes : notes, 
                            table : 'Assignee_ACL'
                            },
                        dataType: "html"    
                    });

                    request.done(function(msg) {
                        $("#editACLPopUp div div div div").html("Assignee ACL Updated.<br><small>page will reload in 3 seconds</small>");
                        /// wait 3 seconds
                        setTimeout(function() {
                            window.location.reload(false)
                        }, 3000);
                    });

                    request.fail(function(jqXHR, textStatus) {
                        $("#editACLPopUp div div div div").html("Error - Request failed: " + textStatus);
                    });
                }
                else{
                    $("#editACLPopUp div div div div").html("Error - Textfield Empty");
                    /// wait 3 seconds
                    setTimeout(function() {
                        window.location.reload(false)
                    }, 2000);
                }
            });

            request.fail(function(jqXHR, textStatus) {
                $("#editACLPopUp div div div div").html("Error - Request failed: " + textStatus);
            });

        });
        //DELETE ASSIGNEE ACL
        $('.deleteACL').on('click', function() {
            var dirPath = "<?php dirPath() ?>";
            var id = $(this).data('id');
            var assignee_id = '<?php echo $assignee->Assignee_ID ?>';
            var assignee_acls = '<?php echo $assignee->Assignee_ACLs ?>';
            assignee_acls = assignee_acls.replace(id,'');
            assignee_acls = assignee_acls.replace(',,',',');
            $('#delACLTrue').click(function(){
                var request = $.ajax({
                    url: dirPath + "/dashboard/req/delete.php",
                    type: "POST",
                    data: {
                        id : id,
                        key : 'Assignee_ACL_ID',
                        table : 'Assignee_ACL'
                        },
                    dataType: "html"
                });

                request.done(function(msg) {
                    var request = $.ajax({
                        url: dirPath + "/dashboard/req/update.php",
                        type: "POST",
                        data: {
                            id : assignee_id,
                            acl : assignee_acls, 
                            table : 'Assignee_ACLs'
                            },
                        dataType: "html"
                    });

                    request.done(function(msg) {
                        console.log(msg);
                        $("#deleteACLPopUp div div div div").html("Assignee ACL Deleted.<br><small>page will redirect in 3 seconds</small>");
                        /// wait 3 seconds
                        setTimeout(function() {
                            window.location.reload(false)
                        }, 3000);
                    });

                    request.fail(function(jqXHR, textStatus) {
                        $("#deleteACLPopUp div div div div").html("Error - Request failed: " + textStatus);
                    });
                });

                request.fail(function(jqXHR, textStatus) {
                    $("#deleteACLPopUp div div div div").html("Error - Request failed: " + textStatus);
                });
            });
        });

        $('.add').on('click', function(){
            var dirPath = "<?php dirPath() ?>";
            var category = $('#acl_category_1').val();
            var request = $.ajax({
                url: dirPath + "/dashboard/req/parameters.php",
                type: "POST",
                data: {
                    category : category,
                    },
                dataType: "html"    
            });

            request.done(function(msg) {
                var obj = JSON.parse(msg);
                var len = obj.length;
                var data = "";
                for(var i = 0; i < len; i++){
                    data += '<option value="'+obj[i].ACL_Parameter+'">'+obj[i].ACL_Parameter+'</option>';
                }
                $('#acl_parameter_1').html(data);
            });

            request.fail(function(jqXHR, textStatus) {
                $('#acl_parameter_1').html("Error");
            }); 
        });

        $(document).on('click', '.row-remove', function(){
            var dirPath = "<?php dirPath() ?>";
            var acl_row_num = $(this).data('id');
            var current_acls = $("#rows").html().split('<!--row-->');
            var num_of_acls = current_acls.length - 1;
            var acl_parameter = [""];
            var acl_category = [""];
            var acl_notes = [""];
            var acl_status = [""];

            for(var i = 1; i <= num_of_acls; i++){
                acl_parameter.push($('#acl_parameter_'+i).val());
                acl_category.push($('#acl_category_'+i).val());
                acl_notes.push($('#acl_notes_'+i).val());
                if($('#acl_status_'+i).is(":checked")){
                    acl_status.push('TRUE');
                }
                else{
                    acl_status.push('FALSE');
                }
            }

            current_acls.splice(acl_row_num, 1);
            acl_parameter.splice(acl_row_num, 1);
            acl_category.splice(acl_row_num, 1);
            acl_notes.splice(acl_row_num, 1);
            acl_status.splice(acl_row_num, 1);
            var final_acls = '';
            for(var i = 1; i < current_acls.length; i++){
                var acl_current_status = "";
                if(acl_status[i] == 'TRUE'){
                    acl_current_status = 'checked';
                }
                final_acls += '<!--row-->'
                    +'<tr>'
                        +'<td class="text-center pt-3"><input id="acl_status_'+i+'" type="checkbox" name="acl_status_'+i+'" '+acl_current_status+'></td>'
                        +'<td>'
                        +'<select class="form-control form-control-sm acl_category" name="acl_category_'+i+'" id="acl_category_'+i+'" value="'+acl_category[i]+'" data-count="'+i+'">'
                            +'<option value="'+acl_category[i]+'">'+acl_category[i]+'</option>'
                        +'</select></td>'
                        +'<td>'
                        +'<select class="form-control form-control-sm" name="acl_parameter_'+i+'" id="acl_parameter_'+i+'">'
                            +'<option value="'+acl_parameter[i]+'">'+acl_parameter[i]+'</option>'
                        +'</select></td>'
                        +'<td><textarea class="form-control form-control-sm" id="acl_notes_'+i+'" name="acl_notes_'+i+'" data-sanitize="escape" rows="1">'+acl_notes[i]+'</textarea></td>'
                        +'<td class="text-center">'
                            +'<button type="button" data-id="'+i+'" name="row_remove_'+i+'" id="row_remove_'+i+'" class="row-remove btn btn-sm p-0">'
                                +'<div class="c-icon">'
                                    +'<i class="cil-trash text-danger"></i>'
                                +'</div>'
                            +'</button>'
                        +'</td>'
                    +'</tr>';
            }
            $('#rows').html(final_acls);
            $('#num_of_assignee_acls').val(current_acls.length);
        });

        $(document).on('change', '.acl_category', function(){
            var dirPath = "<?php dirPath() ?>";
            var count = $(this).data('count');
            var category = $(this).val();
            var request = $.ajax({
                url: dirPath + "/dashboard/req/parameters.php",
                type: "POST",
                data: {
                    category : category,
                    },
                dataType: "html"    
            });

            request.done(function(msg) {
                var obj = JSON.parse(msg);
                var len = obj.length;
                var data = "";
                for(var i = 0; i < len; i++){
                    data += '<option value="'+obj[i].ACL_Parameter+'">'+obj[i].ACL_Parameter+'</option>';
                }
                $('#acl_parameter_'+count).html(data);
            });

            request.fail(function(jqXHR, textStatus) {
                $('#acl_parameter_'+count).html("Error");
            });
        });

        $(document).ready(function(){
            $('#add_row_btn').click(function(){
                var dirPath = "<?php dirPath() ?>";
                var current_assignee_acls = $("#rows").html();
                var num_of_assignee_acls = current_assignee_acls.split('<!--row-->').length;

                var assignee_acl_rows = '<!--row-->'
                    +'<tr>'
                        +'<td class="text-center pt-3"><input id="acl_status_'+num_of_assignee_acls+'" type="checkbox" name="acl_status_'+num_of_assignee_acls+'"></td>'
                        +'<td>'
                        +'<select class="form-control form-control-sm acl_category" name="acl_category_'+num_of_assignee_acls+'" id="acl_category_'+num_of_assignee_acls+'" data-count="'+num_of_assignee_acls+'">'
                            +'<?php
                            foreach($categories as $category){
                                echo '<option value="'.$category->ACL_Category.'">'.$category->ACL_Category.'</option>';
                            }
                            ?>'
                        +'</select></td>'
                        +'<td>'
                        +'<select class="form-control form-control-sm" name="acl_parameter_'+num_of_assignee_acls+'" id="acl_parameter_'+num_of_assignee_acls+'">'
                        +'</select></td>'
                        +'<td><textarea class="form-control form-control-sm" id="acl_notes_'+num_of_assignee_acls+'" name="acl_notes_'+num_of_assignee_acls+'" data-sanitize="escape" rows="1"></textarea></td>'
                        +'<td class="text-center">'
                            +'<button type="button" data-id="'+num_of_assignee_acls+'" name="row_remove_'+num_of_assignee_acls+'" id="row_remove_'+num_of_assignee_acls+'" class="row-remove btn btn-sm p-0">'
                                +'<div class="c-icon">'
                                    +'<i class="cil-trash text-danger"></i>'
                                +'</div>'
                            +'</button>'
                        +'</td>'
                    +'</tr>';
                $('#rows').append(assignee_acl_rows);
                $('#num_of_assignee_acls').val(num_of_assignee_acls);

                var category = $('#acl_category_'+num_of_assignee_acls).val();
                var request = $.ajax({
                    url: dirPath + "/dashboard/req/parameters.php",
                    type: "POST",
                    data: {
                        category : category,
                        },
                    dataType: "html"    
                });

                request.done(function(msg) {
                    var obj = JSON.parse(msg);
                    var len = obj.length;
                    var data = "";
                    for(var i = 0; i < len; i++){
                        data += '<option value="'+obj[i].ACL_Parameter+'">'+obj[i].ACL_Parameter+'</option>';
                    }
                    $('#acl_parameter_'+num_of_assignee_acls).html(data);
                });

                request.fail(function(jqXHR, textStatus) {
                    $('#acl_parameter_'+num_of_assignee_acls).html("Error");
                });
            });
        });    
    </script>


</body>
</html>
