<?php 
    require_once(__DIR__."/../inc/header.php"); 
    $model = new \Libraries\Model();
    $beneficiaries = $model->getBeneficiary();
    $categories = $model->getACLCategories();

    if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['create'])){
        $create = $model->createAssignee([
            'assigneeName' => $_POST['assignee-name'],
            'beneficiaryID' => $_POST['beneficiary'],
            'userID' => $_SESSION['user']['id'],
        ]);
        if($create){
            for($i = 1; $i <= $_POST['num_of_assignee_acls']; $i++){
                $acl_status = $_POST['acl_status_'.$i];
                if($acl_status == ""){
                    $acl_status = 0;
                }
                elseif($acl_status == "on"){
                    $acl_status = 1;
                }
                $getACLCategory = $_POST['acl_category_'.$i];
                $getACLParameter = $_POST['acl_parameter_'.$i];
                $acl = $model->getACLByCategoryAndParameter($getACLCategory, $getACLParameter);
                $createAssigneeACL[] = $model->createAssigneeACL([
                    'assignee_id' => $create, 
                    'active' => $acl_status, 
                    'acl' => $acl->ACL_ID, 
                    'acl_notes' => $_POST['acl_notes_'.$i], 
                    'user_id' => $_SESSION['user']['id'],
                ]);
            }
            $assigneeACLs = implode (",", $createAssigneeACL);
            $addACLToAssignee = $model->updateAssigneeACLs([
                'id' => $create, 
                'acl' => $assigneeACLs, 
            ]);
            if($addACLToAssignee){
                $alert = [
                    'type' => 'success',
                    'message' => 'Assignee Created <b>'.$_POST['assignee-name'].'</b><br>Refirecting to Assignee in 3 seconds.'
                ];
                header("refresh:1;".$GLOBALS['configurations']['dirpath']."/dashboard/assignee/view/".$create);
            }
            else{
                $alert = [
                    'type' => 'danger',
                    'message' => 'Error Creating Assignee ACLs for <b>'.$_POST['assignee-name'].'</b>. Server Error, Contact Adminstrator/Developer.'
                ];
            }
        }
        else{
            $alert = [
                'type' => 'danger',
                'message' => 'Error Creating Assignee <b>'.$_POST['assignee-name'].'</b>. Server Error, Contact Adminstrator/Developer.'
            ];
        }
    }
?>
    <title>Create a New Assignee - <?php echo $title ?></title>
</head>
<body class="c-app">
    
    <?php require_once(__DIR__."/../inc/sidebar.php"); ?>

    <div class="c-wrapper c-fixed-components">

        <?php require_once(__DIR__."/../inc/navbar.php"); ?>

        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                    <?php require(__DIR__.'/../inc/alert.php'); ?>
                        <div class="row justify-content-center">
                            <div class="col-sm-6 col-md-8 col-lg-10">
                                <form action="<?php $_PHP_SELF ?>" method="POST">
                                    <div class="card">
                                        <div class="card-header bg-light text-center"><strong>Add a New Assignee</strong></div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="assignee-name">Name</label>
                                                        <input class="form-control" id="assignee-name" type="text" name="assignee-name" data-sanitize="escape" required>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="beneficiary">Beneficiary</label>
                                                        <select name="beneficiary" id="beneficiary" class="form-control">
                                                            <?php
                                                            foreach($beneficiaries as $beneficiary){
                                                                echo '<option value="'.$beneficiary->Beneficiary_ID.'">'.$beneficiary->Beneficiary_Name.'</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>    
                                                <div class="col-12">
                                                    <div class="alert alert-dark text-center m-0 mt-3 p-1">Assignee ACL(s)</div>
                                                    <input type="text" name="num_of_assignee_acls" id="num_of_assignee_acls" value="1" hidden>
                                                    <table class="table table-responsive-sm">
                                                        <thead class="table-borderless text-center">
                                                            <tr>
                                                                <th>Active</th>
                                                                <th>Category</th>
                                                                <th>Parameter</th>
                                                                <th>Notes</th>
                                                                <th>
                                                                    <button type="button" id="add_row_btn" class="btn btn-sm btn-secondary bg-gradient-light mt-1">
                                                                        <div class="c-icon">
                                                                            <i class="cil-library-add"></i>
                                                                        </div>
                                                                    </button>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="rows">
                                                            <!--row-->
                                                            <tr>
                                                                <td class="text-center pt-3"><input id="acl_status_1" type="checkbox" name="acl_status_1"></td>
                                                                <td>
                                                                    <select class="form-control form-control-sm acl_category" name="acl_category_1" id="acl_category_1" data-count="1">
                                                                        <?php
                                                                        foreach($categories as $category){
                                                                            echo '<option value="'.$category->ACL_Category.'">'.$category->ACL_Category.'</option>';
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select class="form-control form-control-sm" name="acl_parameter_1" id="acl_parameter_1">
                                                                    </select>
                                                                </td>
                                                                <td><textarea class="form-control form-control-sm" id="acl_notes_1" name="acl_notes_1" data-sanitize="escape" rows="1"></textarea></td>
                                                                <td class="text-center">
                                                                    <button type="button" data-id="1" name="acl_remove_1" id="row_remove_1" class="row-remove btn btn-sm p-0">
                                                                        <div class="c-icon">
                                                                            <i class="cil-trash text-danger"></i>
                                                                        </div>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <button class="btn btn-sm btn-dark bg-gradient-dark w-100" name="create" type="submit">Add</button>
                                        </div>
                                    </div>
                                </form>
                                <!-- /.col-->
                            </div>
                            <!-- /.row-->
                        </div>
                    </div>
                </div>    
            </main>
        </div>
        
        <?php require_once(__DIR__."/../inc/footer.php"); ?>
        <script>
            $.validate({
                modules : 'sanitize'
            });
            $(document).on('click', '.row-remove', function(){
                var dirPath = "<?php dirPath() ?>";
                var acl_row_num = $(this).data('id');
                var current_acls = $("#rows").html().split('<!--row-->');
                var num_of_acls = current_acls.length - 1;
                var acl_parameter = [""];
                var acl_category = [""];
                var acl_notes = [""];
                var acl_status = [""];

                for(var i = 1; i <= num_of_acls; i++){
                    acl_parameter.push($('#acl_parameter_'+i).val());
                    acl_category.push($('#acl_category_'+i).val());
                    acl_notes.push($('#acl_notes_'+i).val());
                    if($('#acl_status_'+i).is(":checked")){
                        acl_status.push('TRUE');
                    }
                    else{
                        acl_status.push('FALSE');
                    }
                }

                current_acls.splice(acl_row_num, 1);
                acl_parameter.splice(acl_row_num, 1);
                acl_category.splice(acl_row_num, 1);
                acl_notes.splice(acl_row_num, 1);
                acl_status.splice(acl_row_num, 1);
                var final_acls = '';
                for(var i = 1; i < current_acls.length; i++){
                    var acl_current_status = "";
                    if(acl_status[i] == 'TRUE'){
                        acl_current_status = 'checked';
                    }
                    final_acls += '<!--row-->'
                        +'<tr>'
                            +'<td class="text-center pt-3"><input id="acl_status_'+i+'" type="checkbox" name="acl_status_'+i+'" '+acl_current_status+'></td>'
                            +'<td>'
                            +'<select class="form-control form-control-sm acl_category" name="acl_category_'+i+'" id="acl_category_'+i+'" value="'+acl_category[i]+'" data-count="'+i+'">'
                                +'<option value="'+acl_category[i]+'">'+acl_category[i]+'</option>'
                            +'</select></td>'
                            +'<td>'
                            +'<select class="form-control form-control-sm" name="acl_parameter_'+i+'" id="acl_parameter_'+i+'">'
                                +'<option value="'+acl_parameter[i]+'">'+acl_parameter[i]+'</option>'
                            +'</select></td>'
                            +'<td><textarea class="form-control form-control-sm" id="acl_notes_'+i+'" name="acl_notes_'+i+'" data-sanitize="escape" rows="1">'+acl_notes[i]+'</textarea></td>'
                            +'<td class="text-center">'
                                +'<button type="button" data-id="'+i+'" name="row_remove_'+i+'" id="row_remove_'+i+'" class="row-remove btn btn-sm p-0">'
                                    +'<div class="c-icon">'
                                        +'<i class="cil-trash text-danger"></i>'
                                    +'</div>'
                                +'</button>'
                            +'</td>'
                        +'</tr>';
                }
                $('#rows').html(final_acls);
                $('#num_of_assignee_acls').val(current_acls.length);
            });
            $(document).on('change', '.acl_category', function(){
                var dirPath = "<?php dirPath() ?>";
                var count = $(this).data('count');
                var category = $(this).val();
                var request = $.ajax({
                    url: dirPath + "/dashboard/req/parameters.php",
                    type: "POST",
                    data: {
                        category : category,
                        },
                    dataType: "html"    
                });

                request.done(function(msg) {
                    var obj = JSON.parse(msg);
                    var len = obj.length;
                    var data = "";
                    for(var i = 0; i < len; i++){
                        data += '<option value="'+obj[i].ACL_Parameter+'">'+obj[i].ACL_Parameter+'</option>';
                    }
                    $('#acl_parameter_'+count).html(data);
                });

                request.fail(function(jqXHR, textStatus) {
                    $('#acl_parameter_'+count).html("Error");
                });
            });
            $(document).ready(function(){
                var dirPath = "<?php dirPath() ?>";
                var category = $('#acl_category_1').val();
                var request = $.ajax({
                    url: dirPath + "/dashboard/req/parameters.php",
                    type: "POST",
                    data: {
                        category : category,
                        },
                    dataType: "html"    
                });

                request.done(function(msg) {
                    var obj = JSON.parse(msg);
                    var len = obj.length;
                    var data = "";
                    for(var i = 0; i < len; i++){
                        data += '<option value="'+obj[i].ACL_Parameter+'">'+obj[i].ACL_Parameter+'</option>';
                    }
                    $('#acl_parameter_1').html(data);
                });

                request.fail(function(jqXHR, textStatus) {
                    $('#acl_parameter_'+count).html("Error");
                });

                $('#add_row_btn').click(function(){
                    var current_assignee_acls = $("#rows").html();
                    var num_of_assignee_acls = current_assignee_acls.split('<!--row-->').length;
                    var assignee_acl_rows = '<!--row-->'
                        +'<tr>'
                            +'<td class="text-center pt-3"><input id="acl_status_'+num_of_assignee_acls+'" type="checkbox" name="acl_status_'+num_of_assignee_acls+'"></td>'
                            +'<td>'
                            +'<select class="form-control form-control-sm acl_category" name="acl_category_'+num_of_assignee_acls+'" id="acl_category_'+num_of_assignee_acls+'" data-count="'+num_of_assignee_acls+'">'
                                +'<?php
                                foreach($categories as $category){
                                    echo '<option value="'.$category->ACL_Category.'">'.$category->ACL_Category.'</option>';
                                }
                                ?>'
                            +'</select></td>'
                            +'<td>'
                            +'<select class="form-control form-control-sm" name="acl_parameter_'+num_of_assignee_acls+'" id="acl_parameter_'+num_of_assignee_acls+'">'
                                +'<?php
                                foreach($parameters as $parameter){
                                    echo '<option value="'.$parameter->ACL_Parameter.'">'.$parameter->ACL_Parameter.'</option>';
                                }
                                ?>'
                            +'</select></td>'
                            +'<td><textarea class="form-control form-control-sm" id="acl_notes_'+num_of_assignee_acls+'" name="acl_notes_'+num_of_assignee_acls+'" data-sanitize="escape" rows="1"></textarea></td>'
                            +'<td class="text-center">'
                                +'<button type="button" data-id="'+num_of_assignee_acls+'" name="row_remove_'+num_of_assignee_acls+'" id="row_remove_'+num_of_assignee_acls+'" class="row-remove btn btn-sm p-0">'
                                    +'<div class="c-icon">'
                                        +'<i class="cil-trash text-danger"></i>'
                                    +'</div>'
                                +'</button>'
                            +'</td>'
                        +'</tr>';
                    $('#rows').append(assignee_acl_rows);
                    $('#num_of_assignee_acls').val(num_of_assignee_acls);

                    var category = $('#acl_category_'+num_of_assignee_acls).val();
                    var request = $.ajax({
                        url: dirPath + "/dashboard/req/parameters.php",
                        type: "POST",
                        data: {
                            category : category,
                            },
                        dataType: "html"    
                    });

                    request.done(function(msg) {
                        var obj = JSON.parse(msg);
                        var len = obj.length;
                        var data = "";
                        for(var i = 0; i < len; i++){
                            data += '<option value="'+obj[i].ACL_Parameter+'">'+obj[i].ACL_Parameter+'</option>';
                        }
                        $('#acl_parameter_'+num_of_assignee_acls).html(data);
                    });

                    request.fail(function(jqXHR, textStatus) {
                        $('#acl_parameter_'+num_of_assignee_acls).html("Error");
                    });
                });
            });    
        </script>
    </div>
</body>
</html>